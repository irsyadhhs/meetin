package irsyadhhs.cs.upi.edu.meetin;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

/**
 * Created by HARVI on 2/26/2018.
 */

public class NotificationReceiver extends BroadcastReceiver {

    int notificationId;
    Context mContext;
    NotificationManager mNotificationManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        notificationId = extras.getInt(AppConfig.ALARM_ID);
        this.mContext = context;

        Intent notifIntent = new Intent(context, DetailMeeting.class);
        notifIntent.putExtra(AppConfig.EXTRA_MEETING_ID, extras.getString(AppConfig.EXTRA_MEETING_ID));
        notifIntent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, extras.getString(AppConfig.EXTRA_MEETING_HOSTID));
        notifIntent.putExtra(AppConfig.EXTRA_MEETING_STATUS, extras.getString(AppConfig.EXTRA_MEETING_STATUS));
        notifIntent.putExtra(AppConfig.EXTRA_ADMIN, extras.getString(AppConfig.EXTRA_ADMIN));
        notifIntent.putExtra(AppConfig.EXTRA_DETAIL_MODE, "notification");

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, notificationId, notifIntent, 0);

        String channelId = "";
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            channelId = createNotificationChannel();
        }else{
            channelId = String.valueOf(notificationId);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, String.valueOf(notificationId))
                .setSmallIcon(R.drawable.ic_alarm_black_24dp)
                .setContentTitle("Pertemuan Segera Diadakan")
                .setContentText(extras.getString(AppConfig.EXTRA_NOTIF_TEXT))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setChannelId(channelId)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.InboxStyle()
                    .addLine(extras.getString(AppConfig.EXTRA_NOTIF_TEXT))
                    .addLine("Lokasi : "+extras.getString(AppConfig.EXTRA_MEETING_LOCATION))
                    .addLine("Waktu : "+extras.getString(AppConfig.EXTRA_MEETING_TIMESTART)));


        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(notificationId, mBuilder.build());
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(){
        String channelId = "incoming_service";
        String channelName = "Incoming Schedule Info";
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_LOW);

        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        /*chan.enableLights(true);
        chan.setLightColor(Color.RED);
        chan.enableVibration(true);
        chan.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});*/
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.createNotificationChannel(chan);
        return channelId;
    }
}
