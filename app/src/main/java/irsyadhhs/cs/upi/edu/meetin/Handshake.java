package irsyadhhs.cs.upi.edu.meetin;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.DISCOVERABLE_BT_REQUEST_CODE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.ENABLE_BT_REQUEST_CODE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.KEHADIRAN;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;

public class Handshake extends AppCompatActivity implements AdapterView.OnItemClickListener{
    private static final String TAG = "MainActivity";

    BluetoothAdapter mBluetoothAdapter;

    BluetoothConnectionService mBluetoothConnection;

    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("d22432b5-2421-4bb4-8ede-7d5833f63bd4");

    BluetoothDevice mBTDevice;

    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();

    public DeviceListAdapter mDeviceListAdapter;

    ListView lvNewDevices;

    SharedPreferences sp;

    String myIdS, myNameS, meetingIdS, detailIdS, mDefaultName, mTimeEnd;

    Button btnJabat;
    TextView offConn;

    
    /**
     * Broadcast Receiver for listing devices that are not yet paired
     * -Executed by btnDiscover() method.
     */
    private BroadcastReceiver mBroadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "onReceive: ACTION FOUND.");

            if (BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra (BluetoothDevice.EXTRA_DEVICE);
                if(!mBTDevices.contains(device)){
                    mBTDevices.add(device);
                }
                Log.d(TAG, "onReceive: " + device.getName() + ": " + device.getAddress());
                mDeviceListAdapter = new DeviceListAdapter(context, R.layout.device_adapter_view, mBTDevices);
                lvNewDevices.setAdapter(mDeviceListAdapter);
            }else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                unregisterReceiver(this); //add this line
            }
        }
    };

    /**
     * Broadcast Receiver that detects bond state changes (Pairing status changes)
     */
    private final BroadcastReceiver mBroadcastReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)){
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //3 cases:
                //case1: bonded already
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED){
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDED.");
                    //inside BroadcastReceiver4
                    mBTDevice = mDevice;
                }
                //case2: creating a bone
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDING.");
                }
                //case3: breaking a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d(TAG, "BroadcastReceiver: BOND_NONE.");
                }
            }
        }
    };



    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: called.");
        super.onDestroy();
        if(mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            //unregisterReceiver(mBroadcastReceiver1);
            //unregisterReceiver(mBroadcastReceiver2);
            unregisterReceiver(mBroadcastReceiver3);
            unregisterReceiver(mBroadcastReceiver4);
        }
        //mBluetoothAdapter.cancelDiscovery();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handshake);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);

        sp = getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");

        Bundle extras = getIntent().getExtras();
        meetingIdS = extras.getString(AppConfig.EXTRA_MEETING_ID);
        detailIdS = extras.getString(AppConfig.EXTRA_DETAIL_ID);
        mTimeEnd = extras.getString(AppConfig.EXTRA_MEETING_TIMEEND);

        lvNewDevices = (ListView) findViewById(R.id.listDevice);
        btnJabat = findViewById(R.id.btnJabat);
        offConn = findViewById(R.id.offConnection);
        mBTDevices = new ArrayList<>();

        //Broadcasts when bond state changes (ie:pairing)
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver4, filter);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        btnJabat.setClickable(false);
        btnJabat.setEnabled(false);
        if(mBluetoothAdapter == null){
            Log.d(TAG, "enableDisableBT: Does not have BT capabilities.");
            Toast.makeText(this, "Tidak ada bluettoth", Toast.LENGTH_SHORT).show();
            btnJabat.setBackgroundResource(R.drawable.bgbuttondisable);
            btnJabat.setClickable(false);
            btnJabat.setEnabled(false);
        }else{
            enableDisableBT();
            offConn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    offConnection();
                }
            });
        }

        lvNewDevices.setOnItemClickListener(Handshake.this);

    }

    //create method for starting connection
    //***remember the conncction will fail and app will crash if you haven't paired first
    public void startConnection(){
        startBTConnection(mBTDevice,MY_UUID_INSECURE);
    }

    /**
     * starting chat service method
     */
    public void startBTConnection(BluetoothDevice device, UUID uuid){
        Log.d(TAG, "startBTConnection: Initializing RFCOM Bluetooth Connection.");
        mBluetoothConnection.startClient(device,uuid);
        btnJabat.setClickable(true);
        btnJabat.setEnabled(true);
        btnJabat.setBackgroundResource(R.drawable.bgbutton);
    }



    public void enableDisableBT(){
        if(!mBluetoothAdapter.isEnabled()){
            Log.d(TAG, "enableDisableBT: enabling BT.");
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBTIntent, ENABLE_BT_REQUEST_CODE);

        }else{
            offConn.setVisibility(View.VISIBLE);
            mDefaultName = mBluetoothAdapter.getName();
            mBluetoothAdapter.setName(myNameS);
            Log.d(TAG, "defName newName "+mDefaultName+" "+mBluetoothAdapter.getName());
            enableDisableDiscoverable();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ENABLE_BT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                offConn.setVisibility(View.VISIBLE);
                mDefaultName = mBluetoothAdapter.getName();
                mBluetoothAdapter.setName(myNameS);
                Log.d(TAG, "defName newName "+mDefaultName+" "+mBluetoothAdapter.getName());
                enableDisableDiscoverable();
            }
            if(resultCode == RESULT_CANCELED){
                btnJabat.setClickable(false);
                btnJabat.setEnabled(false);
                offConn.setVisibility(View.INVISIBLE);
                btnJabat.setBackgroundResource(R.drawable.bgbuttondisable);
            }
        }
    }


    public void enableDisableDiscoverable() {
        Log.d(TAG, "btnEnableDisable_Discoverable: Making device discoverable for 300 seconds.");

        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);
        startDiscover();
        /*String kehadiran = sp.getString(KEHADIRAN, "");
        Toast.makeText(this, kehadiran, Toast.LENGTH_SHORT).show();*/
    }

    public void startDiscover() {
        Log.d(TAG, "btnDiscover: Looking for unpaired devices.");

        if(mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
            Log.d(TAG, "btnDiscover: Canceling discovery.");

            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
        }
        if(!mBluetoothAdapter.isDiscovering()){

            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
        }
    }

    /**
     * This method is required for all devices running API23+
     * Android must programmatically check the permissions for bluetooth. Putting the proper permissions
     * in the manifest is not enough.
     *
     * NOTE: This will only execute on versions > LOLLIPOP because it is not needed otherwise.
     */


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //first cancel discovery because its very memory intensive.
        mBluetoothAdapter.cancelDiscovery();

        Log.d(TAG, "onItemClick: You Clicked on a device.");
        String deviceName = mBTDevices.get(i).getName();
        String deviceAddress = mBTDevices.get(i).getAddress();

        Log.d(TAG, "onItemClick: deviceName = " + deviceName);
        Log.d(TAG, "onItemClick: deviceAddress = " + deviceAddress);

        //create the bond.
        Log.d(TAG, "Trying to pair with " + deviceName);
        mBTDevices.get(i).createBond();

        mBTDevice = mBTDevices.get(i);
        mBluetoothConnection = new BluetoothConnectionService(Handshake.this, meetingIdS, detailIdS);
        startConnection();

    }

    public void btnJabat(View view) {
        if(btnJabat.isEnabled()) {
            String kehadiran = sp.getString(KEHADIRAN, "");
            String msgToSend = meetingIdS + ";" + myIdS + ";" + detailIdS + ";" + mTimeEnd + ";" + kehadiran;
            byte[] bytes = msgToSend.getBytes(Charset.defaultCharset());
            mBluetoothConnection.write(bytes);
        }
    }

    public void offConnection() {
        if(mBluetoothAdapter.isEnabled()){
            Log.d(TAG, "enableDisableBT: disabling BT.");
            mBluetoothAdapter.setName(mDefaultName);
            Log.d(TAG, "defName newName "+mDefaultName+" "+mBluetoothAdapter.getName());
            mBluetoothAdapter.disable();
            /*IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);*/
        }

        if(mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
            Log.d(TAG, "btnDiscover: Canceling discovery.");
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        if((mBluetoothAdapter != null)&&(mBluetoothAdapter.isEnabled())) {
            offConnection();
        }
        super.onBackPressed();
    }
}
