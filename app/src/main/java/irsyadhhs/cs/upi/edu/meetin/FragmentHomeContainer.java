package irsyadhhs.cs.upi.edu.meetin;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentHomeContainer extends Fragment {

    private static final String TAG = "FragmentHomeContainer";
    private SectionPageAdapter mSectionPageAdapter;
    private ViewPager mViewPager;

    public FragmentHomeContainer() {
        // Required empty public constructor
    }

    public static FragmentHomeContainer newInstance() {
        FragmentHomeContainer fragment = new FragmentHomeContainer();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_container, container, false);

        mSectionPageAdapter = new SectionPageAdapter(getFragmentManager());
        mViewPager = (ViewPager) rootView.findViewById(R.id.containerhome);
        setUpViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabshome);
        tabLayout.setupWithViewPager(mViewPager);

        return rootView;
    }

    public void setUpViewPager(ViewPager viewPager){
        FragmentMeeting upcoming = new FragmentMeeting();
        FragmentHistory history  = new FragmentHistory();

        mSectionPageAdapter.addFragment(upcoming, "Mendatang");
        mSectionPageAdapter.addFragment(history, "Riwayat");
        viewPager.setAdapter(mSectionPageAdapter);
    }
}
