package irsyadhhs.cs.upi.edu.meetin;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;

/**
 * Created by HARVI on 2/13/2018.
 */

public class AdapterNotif extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static ArrayList<ModelNotif> searchArrayList;

    private LayoutInflater mInflater;
    private Context context;

    SharedPreferences sp;
    String myIdS, myNameS;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private static RelationCallback mRelationCallback;
    private static EventCallback mEventCallback;

    public AdapterNotif(Context context){
        try {
            this.mRelationCallback = ((RelationCallback) context);
            this.mEventCallback = ((EventCallback) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    public interface RelationCallback {
        void onRelationCallback();
    }

    public interface EventCallback {
        void onEventCallback();
    }

    public AdapterNotif(Context context, ArrayList<ModelNotif> results) {
        searchArrayList = results;
        this.context = context;
        sp = context.getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();
    }

    public static class InvitationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        MyInvitationClickListener listener;
        ImageView invitationAcc;
        ImageView invitationNoAcc;
        TextView invitationMeeting;

        public InvitationViewHolder(View itemView, MyInvitationClickListener listener) {
            super(itemView);
            /*this.txtType = (TextView) itemView.findViewById(R.id.type);*/
            this.invitationMeeting = (TextView) itemView.findViewById(R.id.invitationMeeting);
            this.invitationAcc = (ImageView) itemView.findViewById(R.id.invitationAcc);
            this.invitationNoAcc = (ImageView) itemView.findViewById(R.id.invitationNoAcc);
            this.listener = listener;

            invitationAcc.setOnClickListener(this);
            invitationNoAcc.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.invitationAcc:
                    listener.onInvitationAcc(this.getAdapterPosition());
                    try {
                        mEventCallback.onEventCallback();
                    } catch (ClassCastException exception) {
                        // do something
                    }
                    break;
                case R.id.invitationNoAcc:
                    listener.onInvitationNoAcc(this.getAdapterPosition());
                    break;
                default:
                    break;
            }
        }

        public interface MyInvitationClickListener {
            void onInvitationAcc(int p);
            void onInvitationNoAcc(int p);
        }

    }

    public static class RelationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        MyRelationClickListener listener;
        ImageView relationAcc;
        ImageView relationNoAcc;
        TextView memberNameRelation;

        public RelationViewHolder(View itemView, MyRelationClickListener listener) {
            super(itemView);
            this.memberNameRelation = (TextView) itemView.findViewById(R.id.memberNameRelation);
            this.relationAcc = (ImageView) itemView.findViewById(R.id.relationAcc);
            this.relationNoAcc = (ImageView) itemView.findViewById(R.id.relationNoAcc);
            this.listener = listener;

            relationAcc.setOnClickListener(this);
            relationNoAcc.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.relationAcc:
                    listener.onRelationAcc(this.getAdapterPosition());
                    try {
                        mRelationCallback.onRelationCallback();
                    } catch (ClassCastException exception) {
                        // do something
                    }
                    break;
                case R.id.relationNoAcc:
                    listener.onRelationNoAcc(this.getAdapterPosition());
                    break;
                default:
                    break;
            }
        }

        public interface MyRelationClickListener {
            void onRelationAcc(int p);
            void onRelationNoAcc(int p);
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        MyClickListener listener;

        ImageView acc;
        ImageView delete;
        TextView memberName;
        TextView meetingTitle;

        public ViewHolder(View itemView, MyClickListener listener) {
            super(itemView);
            this.memberName = (TextView) itemView.findViewById(R.id.memberName);
            this.meetingTitle = (TextView) itemView.findViewById(R.id.meetingTitle);
            this.acc = (ImageView) itemView.findViewById(R.id.memberAcc);
            this.delete = (ImageView) itemView.findViewById(R.id.memberNoAcc);

            this.listener = listener;

            acc.setOnClickListener(this);
            delete.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.memberAcc:
                    listener.onAcc(this.getAdapterPosition());
                    break;
                case R.id.memberNoAcc:
                    listener.onDelete(this.getAdapterPosition());
                    break;
                default:
                    break;
            }
        }

        public interface MyClickListener {
            void onAcc(int p);
            void onDelete(int p);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case ModelNotif.EVENT_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notif_list, parent, false);
                AdapterNotif.ViewHolder myViewHolder = new AdapterNotif.ViewHolder(view, new ViewHolder.MyClickListener() {
                    @Override
                    public void onAcc(int p) {
                        mFirebaseDatabase.child("MeetingDetail").child(searchArrayList.get(p).getDetailId()).child("approve").setValue("1");
                        mFirebaseDatabase.child("MeetingDetail").child(searchArrayList.get(p).getDetailId()).child("sync").setValue(2);
                        searchArrayList.remove(p);
                        notifyItemRemoved(p);
                    }

                    @Override
                    public void onDelete(int p) {
                        mFirebaseDatabase.child("MeetingDetail").child(searchArrayList.get(p).getDetailId()).removeValue();
                        searchArrayList.remove(p);
                        notifyItemRemoved(p);
                    }
                });
                return myViewHolder;
            case ModelNotif.RELATION_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notif_relation, parent, false);
                AdapterNotif.RelationViewHolder myRelationViewHolder = new AdapterNotif.RelationViewHolder(view, new RelationViewHolder.MyRelationClickListener() {
                    @Override
                    public void onRelationAcc(int p) {
                        mFirebaseDatabase.child("Relation").child(searchArrayList.get(p).getUserId1())
                                .child(searchArrayList.get(p).getRelationId()).child("approve").setValue("1");

                        searchArrayList.remove(p);
                        notifyItemRemoved(p);
                    }

                    @Override
                    public void onRelationNoAcc(int p) {
                        mFirebaseDatabase.child("Relation").child(searchArrayList.get(p).getUserId1())
                                .child(searchArrayList.get(p).getRelationId()).removeValue();

                        searchArrayList.remove(p);
                        notifyItemRemoved(p);
                    }
                });
                return  myRelationViewHolder;

            case ModelNotif.INVITATION_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notif_invitation, parent, false);
                AdapterNotif.InvitationViewHolder myInvitationViewHolder = new AdapterNotif.InvitationViewHolder(view, new InvitationViewHolder.MyInvitationClickListener() {
                    @Override
                    public void onInvitationAcc(int p) {
                        mFirebaseDatabase.child("MeetingDetail").child(searchArrayList.get(p).getDetailId()).child("approve").setValue("1");
                        mFirebaseDatabase.child("MeetingDetail").child(searchArrayList.get(p).getDetailId()).child("sync").setValue(2);

                        searchArrayList.remove(p);
                        notifyItemRemoved(p);
                    }

                    @Override
                    public void onInvitationNoAcc(int p) {
                        mFirebaseDatabase.child("MeetingDetail").child(searchArrayList.get(p).getDetailId()).removeValue();
                        searchArrayList.remove(p);
                        notifyItemRemoved(p);
                    }
                });
                return  myInvitationViewHolder;
        }


        return null;
    }

    @Override
    public int getItemViewType(int position) {

        switch (searchArrayList.get(position).type) {
            case 0:
                return ModelNotif.EVENT_TYPE;
            case 1:
                return ModelNotif.RELATION_TYPE;
            case 2:
                return ModelNotif.INVITATION_TYPE;
            default:
                return -1;
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        ModelNotif object = searchArrayList.get(position);
        if (object != null) {
            switch (object.type) {
                case ModelNotif.EVENT_TYPE:
                    ((ViewHolder) holder).meetingTitle.setText(object.getMeetingTitle());
                    ((ViewHolder) holder).memberName.setText(object.getMemberName() + " ingin mengikuti pertemuan");

                    break;
                case ModelNotif.RELATION_TYPE:
                    ((RelationViewHolder) holder).memberNameRelation.setText(object.getName2());
                    break;
                case ModelNotif.INVITATION_TYPE:
                    ((InvitationViewHolder) holder).invitationMeeting.setText("Undangan baru di " + object.getMeetingTitle());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }
}
