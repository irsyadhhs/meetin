package irsyadhhs.cs.upi.edu.meetin;

public class ModelDraft {

    public String meetingTitle="";
    public String about="";
    public String description="";
    public String location="";
    public String latitude="";
    public String longitude="";
    public String date="";
    public String timeStart="";
    public String duration="";
    public String meetingId="";

    public ModelDraft(){

    }

    public ModelDraft(String meetingTitle, String about, String description,
                          String location, String latitude, String longitude, String date, String timeStart,
                          String duration){
        this.meetingTitle = meetingTitle;
        this.about = about;
        this.description = description;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.timeStart = timeStart;
        this.duration = duration;
    }

    public void setMeetingId(String meetingId){
        this.meetingId = meetingId;
    }

    public String getMeetingId(){
        return this.meetingId;
    }

    public void setMeetingTitle(String title) {
        this.meetingTitle = title;
    }

    public String getTitle() { return meetingTitle; }


    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude ) {
        this.longitude = longitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
