package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/24/2018.
 */

public class ModelUser {
    public String name;
    public String email;
    public String UID;
    public String company;
    public String divisi;
    public String job;
    public String education;
    public String picture;
    public String instanceId;

    // Default constructor required for calls to
    // DataSnapshot.getValue(ModelUser.class)
    public ModelUser() {
    }

    public ModelUser(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }

    public void setEmail(String email){
        this.email = email;
    }
    public String getEmail(){
        return this.email;
    }

    public void setCompany(String company){
        this.company = company;
    }
    public String getCompany(){
        return this.company;
    }

    public void setDivisi(String divisi){
        this.divisi = divisi;
    }
    public String getDivisi(){
        return this.divisi;
    }

    public void setJob(String job){
        this.job = job;
    }
    public String getJob(){
        return this.job;
    }

    public void setEducation(String education){
        this.education = education;
    }
    public String getEducation(){
        return this.education;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }


}
