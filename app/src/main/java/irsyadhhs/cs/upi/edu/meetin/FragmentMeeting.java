package irsyadhhs.cs.upi.edu.meetin;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.KEHADIRAN;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MEETING_FINISHED;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MEETING_FINISHED_EARLY;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SYNC_FINISHED_EARLY;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;


/**
 * A simple {@link Fragment} subclass.
 */

public class FragmentMeeting extends Fragment {
    private static RecyclerView recyclerView;

    RelativeLayout groupFindRL;

    ArrayList<ModelMeeting> isiUpcoming = new ArrayList<>();

    SharedPreferences sp;

    String myIdS, myNameS, meetingIdS;

    double latitude;
    double longitude;

    TextView noUpcomingTV;
    ImageView noUpcomingImV;

    Context mContext;

    private ProgressBar progressBar;

    private DatabaseReference mFirebaseDatabase;

    private int CREATE_MEETING_REQUEST = 2;

    public FragmentMeeting() {
        // Required empty public constructor
    }

    public static FragmentMeeting newInstance() {
        FragmentMeeting fragment = new FragmentMeeting();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabase.keepSynced(true);

        mContext = getActivity().getApplicationContext();

        sp = this.getActivity().getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fragment_feed, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listView1);
        noUpcomingTV = rootView.findViewById(R.id.noUpcomingTV);
        noUpcomingImV = rootView.findViewById(R.id.noUpcomingImV);
        groupFindRL = rootView.findViewById(R.id.groupFind);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        groupFindRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, FindEvent.class);
                startActivity(intent);
            }
        });

        noUpcomingImV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CreateMeeting.class);
                intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, myIdS);
                intent.putExtra(AppConfig.MODE, "add");
                startActivityForResult(intent, CREATE_MEETING_REQUEST);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!runtime_permissions()) {
            getMeetingDetail();
        }
    }

    public void sortList(ArrayList<ModelMeeting> list) {
        Collections.sort(list, new Comparator<ModelMeeting>() {
            @Override
            public int compare(ModelMeeting m1, ModelMeeting m2) {
                String st1 = m1.getStatus();
                String st2 = m2.getStatus();
                int stComp = st1.compareTo(st2);

                Integer dd1 = m1.getDateDiff();
                Integer dd2 = m2.getDateDiff();
                int ddComp;

                ddComp = dd1.compareTo(dd2);


                if (stComp != 0) {
                    return stComp;
                } else if (ddComp != 0) {
                    return ddComp;
                } else {
                    Long hd1 = m1.getHourDiff();
                    Long hd2 = m2.getHourDiff();

                    return hd1.compareTo(hd2);
                }
            }
        });
    }

    private void getMeeting(final ModelDetail model, final String detailId) {
        mFirebaseDatabase.child("MeetingList").child(model.meetingId).addListenerForSingleValueEvent(new ValueEventListener() {
            int status = 0;

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    ModelMeeting modelMeeting = dataSnapshot.getValue(ModelMeeting.class);
                    if (modelMeeting == null) {
                        return;
                    }
                    modelMeeting.setMeetingId(model.meetingId);
                    modelMeeting.setAdmin(model.getAdmin());
                    modelMeeting.setAttendance(model.attendance);
                    modelMeeting.setDetailId(detailId);

                    int dateDiff = getDate(modelMeeting.date);
                    long hourDiff = getTime(modelMeeting.timeEnd);
                    modelMeeting.setDateDiff(dateDiff);
                    modelMeeting.setHourDiff(hourDiff);

                    Log.d(TAG, "meeting id "+modelMeeting.meetingTitle);
                    Log.d(TAG, "date diff "+dateDiff);
                    Log.d(TAG, "hour diff "+hourDiff);

                    latitude = Double.valueOf(modelMeeting.latitude);
                    longitude = Double.valueOf(modelMeeting.longitude);

                    String dateMeeting = modelMeeting.date + " " + modelMeeting.timeStart;
                    String dateMeetingStop = modelMeeting.date + " " + modelMeeting.timeEnd;
                    long dateSch = getDateParse(dateMeeting);
                    long dateSchStop = getDateParse(dateMeetingStop);

                    Log.d(TAG, "blutut stat "+sp.getString(KEHADIRAN,""));

                    if(SYNC_FINISHED_EARLY == model.sync){
                        int alarmId = Integer.parseInt(modelMeeting.code.substring(4, 9));
                        stopAlarm(alarmId, dateSchStop, myIdS, detailId);
                        mFirebaseDatabase.child("MeetingDetail").child(detailId).child("sync").setValue(3);
                    }

                    if ((!MEETING_FINISHED.equals(modelMeeting.status))&&(dateDiff != -99)&&(dateDiff > 0 || (dateDiff == 0 && hourDiff != -1))) {
                        if (isAdded()) {
                            recyclerView.setVisibility(View.VISIBLE);
                            noUpcomingTV.setVisibility(View.INVISIBLE);
                            noUpcomingImV.setVisibility(View.INVISIBLE);
                            isiUpcoming.add(modelMeeting);
                            int alarmId = Integer.parseInt(modelMeeting.code.substring(4, 9));

                            setAlarm(model.meetingId, alarmId, dateSch, latitude, longitude, myIdS, detailId, hourDiff);
                            stopAlarm(alarmId, dateSchStop, myIdS, detailId);
                            if(model.sync == 2) {
                                long delayToTimeStart = getTime(modelMeeting.timeStart);
                                mFirebaseDatabase.child("MeetingDetail").child(detailId).child("sync").setValue(3);
                                if (delayToTimeStart != -1) {
                                    AlarmManager alarmManager = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                    Intent meetingIntent = new Intent(getActivity().getApplicationContext(), NotificationReceiver.class);
                                    meetingIntent.putExtra(AppConfig.EXTRA_NOTIF_TEXT, modelMeeting.meetingTitle);
                                    meetingIntent.putExtra(AppConfig.EXTRA_MEETING_ID, modelMeeting.meetingId);
                                    meetingIntent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, modelMeeting.hostId);
                                    meetingIntent.putExtra(AppConfig.EXTRA_MEETING_STATUS, modelMeeting.status);
                                    meetingIntent.putExtra(AppConfig.EXTRA_MEETING_LOCATION, modelMeeting.location);
                                    meetingIntent.putExtra(AppConfig.EXTRA_MEETING_TIMESTART, modelMeeting.timeStart);
                                    meetingIntent.putExtra(AppConfig.EXTRA_ADMIN, modelMeeting.admin);
                                    meetingIntent.putExtra(AppConfig.ALARM_ID, alarmId);

                                    meetingIntent.setAction(AppConfig.ACTION_START_SERVICE);
                                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), alarmId * -1, meetingIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, dateSch - 900000, pendingIntent);
                                }
                            }
                        }
                    }

                    sortList(isiUpcoming);
                    if (isAdded()) {
                        recyclerView.setAdapter(new AdapterUpcoming(getActivity().getApplicationContext(), isiUpcoming));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void getMeetingDetail() {
        progressBar.setVisibility(View.VISIBLE);
        isiUpcoming.clear();
        mFirebaseDatabase.child("MeetingDetail").orderByChild("memberId").equalTo(myIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    recyclerView.setVisibility(View.INVISIBLE);
                    noUpcomingTV.setVisibility(View.VISIBLE);
                    noUpcomingImV.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    int status = 0;
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                        if (modelDetail == null) {
                            return;
                        }
                        if ("1".equals(modelDetail.approve)) {
                            meetingIdS = modelDetail.meetingId;
                            String detailId = childSnapshot.getKey();
                            getMeeting(modelDetail, detailId);
                            status = 1;
                        }
                    }
                    if (status != 1) {
                        recyclerView.setVisibility(View.INVISIBLE);
                        noUpcomingTV.setVisibility(View.VISIBLE);
                        noUpcomingImV.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // tampilkan dialog minta ijin
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //loadData();
            } else {
                //permssion tidak diberikan, tampilkan pesan
                AlertDialog.Builder ad = new AlertDialog.Builder(getActivity().getApplicationContext());
                ad.setMessage("Izinkan Akses Lokasi")
                        .setCancelable(false)
                        .setPositiveButton("Izinkan", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                runtime_permissions();
                            }
                        });
                AlertDialog alert = ad.create();
                alert.show();
            }
        }
    }

    public long getDateParse(String dateMeeting) {
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        try {
            return mdformat.parse(dateMeeting).getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public int getDate(String dateS) {
        int diff;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String strDate = mdformat.format(calendar.getTime());

        try {
            Date date1 = mdformat.parse(dateS);
            Date date2 = mdformat.parse(strDate);

            diff = ((int) ((date1.getTime() / (24 * 60 * 60 * 1000)) - (int) (date2.getTime() / (24 * 60 * 60 * 1000))));

            return diff;
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public long getTime(String timeEndS) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);

            Date systemDate = Calendar.getInstance().getTime();
            String myDate = format.format(systemDate);
            Date date1 = format.parse(timeEndS);
            Date date2 = format.parse(myDate);


            long millse = date1.getTime() - date2.getTime();

            if (millse > 0) {
                return millse;
            } else {
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public void setAlarm(String meetingId, int pendingIntentCode, long timeSchedule, double latitude, double longitude, String userId, String detailId, long geoDuration) {
        AlarmManager alarmManager = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Intent meetingIntent = new Intent(getActivity().getApplicationContext(), AttendanceReceiver.class);
        meetingIntent.putExtra(AppConfig.EXTRA_MEETING_LATITUDE, latitude);
        meetingIntent.putExtra(AppConfig.EXTRA_MEETING_LONGITUDE, longitude);
        meetingIntent.putExtra(AppConfig.EXTRA_MEETING_ID, meetingId);
        meetingIntent.putExtra(AppConfig.EXTRA_GEO_ID, pendingIntentCode);
        meetingIntent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, userId);
        meetingIntent.putExtra(AppConfig.EXTRA_DETAIL_ID, detailId);
        meetingIntent.putExtra(AppConfig.EXTRA_GEO_DURATION, geoDuration);
        meetingIntent.setAction(AppConfig.ACTION_START_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), pendingIntentCode, meetingIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeSchedule, pendingIntent);
        }else{
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeSchedule, pendingIntent);
        }


        Log.d("setalarm", "schedule start");
    }

    public void stopAlarm(int pendingIntentCode, long timeSchedule, String myIdS, String detailId) {
        int pendingStopCode = pendingIntentCode * -1;
        AlarmManager alarmManager = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Intent meetingIntent = new Intent(getActivity().getApplicationContext(), StopServiceReceiver.class);
        meetingIntent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, myIdS);
        meetingIntent.putExtra(AppConfig.EXTRA_DETAIL_ID, detailId);
        meetingIntent.putExtra(AppConfig.ALARM_ID, pendingStopCode);
        meetingIntent.setAction(String.valueOf(pendingStopCode));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), pendingStopCode, meetingIntent, 0);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeSchedule, pendingIntent);
        }else{
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeSchedule, pendingIntent);
        }


        Log.d("stopalarm", "schedule stop");
        Log.d(TAG, "stop schedule detailId" + detailId);
    }
}
