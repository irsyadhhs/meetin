package irsyadhhs.cs.upi.edu.meetin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MEETING_ONGOING;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MEETING_UNDISCOVERABLE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class AddAnggota extends AppCompatActivity implements AdapterAddAnggota.AdapterCallback {
    private static RecyclerView recyclerView;

    ProgressBar progressBar;
    private DatabaseReference mFirebaseDatabase;

    ArrayList<ModelAddAnggota> mRelation = new ArrayList<>();

    SharedPreferences sp;
    String myIdS, myNameS, meetingIdS, hostIdS, titleS, codeS, name1S;
    TextView titleMeetingTV, codeInfoTV;
    RelativeLayout groupInfo;
    SwitchCompat switchCode;

    private AdapterAddAnggota mMyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mMyAdapter = new AdapterAddAnggota(this);
        setContentView(R.layout.activity_add_anggota);

        sp = this.getSharedPreferences(SP, MODE_PRIVATE);
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddAnggota.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        titleMeetingTV = findViewById(R.id.titleMeetingAddAnggota);
        codeInfoTV = findViewById(R.id.informasiKode);
        progressBar = findViewById(R.id.progressBar);
        groupInfo = findViewById(R.id.groupInfo);
        switchCode = findViewById(R.id.switchCode);

        Bundle extras = getIntent().getExtras();
        meetingIdS = extras.getString(AppConfig.EXTRA_MEETING_ID);
        hostIdS = extras.getString(AppConfig.EXTRA_MEETING_HOSTID);
        titleS = extras.getString(AppConfig.EXTRA_MEETING_TITLE);
        codeS = extras.getString(AppConfig.EXTRA_MEETING_CODE);
        name1S = extras.getString(AppConfig.EXTRA_MEMBER_USER1);

        titleMeetingTV.setText(titleS);
        codeInfoTV.setText("Tambahkan anggota melalui kode" + "\n" + "Bagikan kode " + codeS);

        recyclerView = findViewById(R.id.listViewAddAnggota);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        LocalBroadcastManager.getInstance(this).registerReceiver(mRelationReceiver,
                new IntentFilter("find-relation"));

        //loadMember();
        switchShare();
        getRelation();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRelationReceiver);
        super.onDestroy();
    }

    @Override
    public void onMethodCallback() {
        getRelation();
    }

    private void switchShare() {
        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("status").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null) {
                    String status = dataSnapshot.getValue().toString();
                    Log.d(TAG, "status " + status);
                    if (MEETING_ONGOING.equals(status)) {
                        switchCode.setChecked(true);
                        groupInfo.setVisibility(View.VISIBLE);
                    } else if (MEETING_UNDISCOVERABLE.equals(status)) {
                        switchCode.setChecked(false);
                        groupInfo.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        switchCode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //groupInfo.setVisibility(View.VISIBLE);
                    mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("status").setValue(MEETING_ONGOING);
                }else{
                    //groupInfo.setVisibility(View.GONE);
                    mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("status").setValue(MEETING_UNDISCOVERABLE);
                }

            }
        });
    }

    private void getRelation() {
        progressBar.setVisibility(View.VISIBLE);
        mRelation.clear();
        mFirebaseDatabase.child("Relation").child(myIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0){
                    Toast.makeText(AddAnggota.this, "Tidak ada relasi", Toast.LENGTH_SHORT).show();
                    recyclerView.setVisibility(View.INVISIBLE);
                }
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    final ModelRelation modelRelation = childSnapshot.getValue(ModelRelation.class);
                    Log.d(TAG, "relasi masuk "+modelRelation.relationUserId);
                    if (modelRelation == null) {
                        Log.d(TAG, "relasi null");
                        return;
                    }
                    recyclerView.setVisibility(View.VISIBLE);
                    mFirebaseDatabase.child("Users").child(modelRelation.relationUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                            if (modelUser == null) {
                                return;
                            }
                            getMeetingDeatil(modelRelation, modelUser.picture);
                        }
                        @Override
                        public void onCancelled(DatabaseError error) {
                            Log.e(TAG, "Failed to read user", error.toException());
                        }
                    });
                }
                progressBar.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void getMeetingDeatil(final ModelRelation modelRelation, final String picture) {
        mFirebaseDatabase.child("MeetingDetail").orderByChild("memberId").equalTo(modelRelation.relationUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            int status = 0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0){
                    ModelAddAnggota modelAddAnggota = new ModelAddAnggota("",meetingIdS, hostIdS, modelRelation.relationUserId, modelRelation.relationName, "-1", "0", "0", "0"
                    );
                    modelAddAnggota.setPicture(picture);
                    mRelation.add(modelAddAnggota);
                    AdapterAddAnggota datakamus = new AdapterAddAnggota(AddAnggota.this, mRelation, titleS);
                    recyclerView.setAdapter(datakamus);
                    return;
                }
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                    if (modelDetail == null) {
                        return;
                    }

                    if(modelDetail.meetingId.equals(meetingIdS)){
                        status = 1;
                        String approve = modelDetail.approve;
                        ModelAddAnggota modelAddAnggota = new ModelAddAnggota(childSnapshot.getKey(),meetingIdS, hostIdS, modelRelation.relationUserId, modelRelation.relationName, approve, "0", "0", "0"
                        );
                        modelAddAnggota.setPicture(picture);
                        mRelation.add(modelAddAnggota);
                    }
                }
                if(status != 1){
                    ModelAddAnggota modelAddAnggota = new ModelAddAnggota("",meetingIdS, hostIdS, modelRelation.relationUserId, modelRelation.relationName, "-1", "0", "0", "0"
                    );
                    modelAddAnggota.setPicture(picture);
                    mRelation.add(modelAddAnggota);
                }
                AdapterAddAnggota datakamus = new AdapterAddAnggota(AddAnggota.this, mRelation, titleS);
                recyclerView.setAdapter(datakamus);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void toRelation(View view) {
        Intent intent = new Intent(this, FindRelation.class);
        startActivity(intent);
    }

    private BroadcastReceiver mRelationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switchShare();
            getRelation();
        }
    };
}
