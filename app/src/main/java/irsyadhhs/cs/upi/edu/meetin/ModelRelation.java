package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/24/2018.
 */

public class ModelRelation {
    public String relationUserId="";
    public String relationName="";
    public String relationPicture="";
    public String approve="";
    public String meetingId="";
    public String hostId="";
    public int sync;

    public ModelRelation(){

    }

    public ModelRelation(String relationUserId, String relationName, String approve){
        this.relationUserId = relationUserId;
        this.relationName = relationName;
        this.approve = approve;
    }

    public void setMeetingId(String meetingId){
        this.meetingId = meetingId;
    }
    public String getMeetingId(){
        return this.meetingId;
    }
    public void setHostId(String hostId){
        this.hostId = hostId;
    }
    public String getHostId(){
        return this.hostId;
    }
    public String getRelationPicture() {
        return relationPicture;
    }
    public void setRelationPicture(String relationPicture) {
        this.relationPicture = relationPicture;
    }
    public String getRelationUserId() {
        return relationUserId;
    }

    public void setRelationUserId(String relationUserId) {
        this.relationUserId = relationUserId;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

}
