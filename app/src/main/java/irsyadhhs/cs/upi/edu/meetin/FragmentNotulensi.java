package irsyadhhs.cs.upi.edu.meetin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MY_ID;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MY_NAME;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

/**
 * Created by HARVI on 3/18/2018.
 */

public class FragmentNotulensi extends Fragment {

    String titleS, meetingIdS, timeEndS, dateS, myIdS, myNameS, hostIdS;
    TextView titleRekapTV, statusNotulensiTV;
    EditText isiNotulensiET;
    private DatabaseReference mFirebaseDatabase;
    SharedPreferences sp;
    ProgressBar progressBar;
    SwitchCompat switchShare;
    Button btnNotulensi;
    private RecyclerView recyclerView;
    private FirebaseRecyclerAdapter<ModelNotulensi, FragmentNotulensi.NotulensiHolder> mNotulensiAdapter;
    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        Bundle extras = this.getArguments();

        Log.d(TAG, "getArgs meeting id " + extras.getString(AppConfig.EXTRA_MEETING_ID));
        Log.d(TAG, "getArgs meeting title " + extras.getString(AppConfig.EXTRA_MEETING_TITLE));
        Log.d(TAG, "getArgs meeting timeend " + extras.getString(AppConfig.EXTRA_MEETING_TIMEEND));
        Log.d(TAG, "getArgs meeting date " + extras.getString(AppConfig.EXTRA_MEETING_DATE));

        titleS = extras.getString(AppConfig.EXTRA_MEETING_TITLE);
        meetingIdS = extras.getString(AppConfig.EXTRA_MEETING_ID);
        timeEndS = extras.getString(AppConfig.EXTRA_MEETING_TIMEEND);
        dateS = extras.getString(AppConfig.EXTRA_MEETING_DATE);
        hostIdS = extras.getString(AppConfig.EXTRA_MEETING_HOSTID);
        myIdS = context.getSharedPreferences(SP, MODE_PRIVATE).getString(MY_ID,"");
        myNameS = context.getSharedPreferences(SP, MODE_PRIVATE).getString(MY_NAME,"");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_notulensi,container,false);

        bindingView(view);
        //recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        titleRekapTV.setText(titleS);

        if(myIdS.equals(hostIdS)) {
            populateNotulensiHost();
        }else{
            populateNotulensiMember();
        }
        getNotulensi();
        switchShare();
        mNotulensiAdapter.startListening();

        btnNotulensi.setEnabled(false);
        isiNotulensiET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                btnNotulensi.setEnabled(true);
                return false;
            }
        });
        btnNotulensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btnNotulensi.isEnabled()) {
                    InputMethodManager inputManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    prosesNotulensi();
                }
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mNotulensiAdapter.stopListening();
    }

    public void bindingView(View view){
        statusNotulensiTV = view.findViewById(R.id.statusNotulensiTV);
        btnNotulensi = view.findViewById(R.id.btnNotulensi);
        progressBar = view.findViewById(R.id.progressBar);
        titleRekapTV = view.findViewById(R.id.titleRekapTV);
        isiNotulensiET = view.findViewById(R.id.notulensiET);
        recyclerView = view.findViewById(R.id.listViewNotulensi);
        switchShare = view.findViewById(R.id.switchShare);
    }

    private void switchShare() {
        if(myIdS.equals(hostIdS)){
            switchShare.setText("Bagikan dengan anggota");
        }else{
            switchShare.setText("Bagikan dengan ketua");
        }

        switchShare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(myIdS.equals(hostIdS)){
                        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("notulensi").child(myIdS).child("share").setValue("1");
                    }else{
                        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("notulensi").child(myIdS).child("share").setValue("2");
                    }
                    //notulensi dibagikan
                    statusNotulensiTV.setText("Notulensi Dibagikan");
                }else{
                    mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("notulensi").child(myIdS).child("share").setValue("0");
                    //notulensi tidak dibagikan
                    statusNotulensiTV.setText("Notulensi Tidak Dibagikan");
                }

            }
        });
    }


    public void getNotulensi(){
        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("notulensi").child(myIdS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0){
                    isiNotulensiET.setText("-");
                }else {
                    ModelNotulensi modelNotulensi = dataSnapshot.getValue(ModelNotulensi.class);
                    if (modelNotulensi.notulensi == null) {
                        return;
                    }
                    isiNotulensiET.setText(modelNotulensi.notulensi);
                    if(modelNotulensi.getShare().equals("0")){
                        switchShare.setChecked(false);
                    }else{
                        switchShare.setChecked(true);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void prosesNotulensi() {
        final String notulensi = isiNotulensiET.getText().toString();
        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("notulensi").child(myIdS).setValue(new ModelNotulensi(myNameS, notulensi));
        Toast.makeText(context, "Catatan disimpan", Toast.LENGTH_SHORT).show();
        btnNotulensi.setEnabled(false);
        switchShare.setChecked(false);
        isiNotulensiET.clearFocus();
    }

    public void populateNotulensiHost(){
        DatabaseReference notulensiRef = mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("notulensi");
        Query query = notulensiRef.orderByChild("share").equalTo("2");
        FirebaseRecyclerOptions personsOptions = new FirebaseRecyclerOptions.Builder<ModelNotulensi>().setQuery(query, ModelNotulensi.class).build();

        mNotulensiAdapter = new FirebaseRecyclerAdapter<ModelNotulensi, FragmentNotulensi.NotulensiHolder>(personsOptions) {
            @Override
            protected void onBindViewHolder(FragmentNotulensi.NotulensiHolder holder, int position, ModelNotulensi model) {
                holder.setNotulensi(model.author, model.notulensi);
            }
            @Override
            public FragmentNotulensi.NotulensiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_notulensi, parent, false);

                return new FragmentNotulensi.NotulensiHolder(view);
            }
        };
        recyclerView.setAdapter(mNotulensiAdapter);
    }

    public void populateNotulensiMember(){
        DatabaseReference notulensiRef = mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("notulensi");
        Query query = notulensiRef.orderByChild("share").equalTo("1");
        FirebaseRecyclerOptions personsOptions = new FirebaseRecyclerOptions.Builder<ModelNotulensi>().setQuery(query, ModelNotulensi.class).build();

        mNotulensiAdapter = new FirebaseRecyclerAdapter<ModelNotulensi, FragmentNotulensi.NotulensiHolder>(personsOptions) {
            @Override
            protected void onBindViewHolder(FragmentNotulensi.NotulensiHolder holder, int position, ModelNotulensi model) {
                holder.setNotulensi(model.author, model.notulensi);
            }
            @Override
            public FragmentNotulensi.NotulensiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_notulensi, parent, false);

                return new FragmentNotulensi.NotulensiHolder(view);
            }
        };
        recyclerView.setAdapter(mNotulensiAdapter);
    }

    public class NotulensiHolder extends RecyclerView.ViewHolder{
        TextView author;
        TextView notulensi;
        public NotulensiHolder(View itemView) {
            super(itemView);
            this.author = itemView.findViewById(R.id.authorNotulensi);
            this.notulensi = itemView.findViewById(R.id.isiNotulensi);
        }

        public void setNotulensi(String author, String notulensi){
            this.author.setText(author);
            this.notulensi.setText(notulensi);
        }
    }
}
