package irsyadhhs.cs.upi.edu.meetin;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EditProfile extends AppCompatActivity {

    EditText editname_et, pekerjaan_et1, pendidikan_et, editperusahaan_et, editbagian_et;
    ImageView editProfilePicture;
    String myIdS, myNameS, myEmailS, perusahaanS, bagianS, pekerjaanS, pendidikanS;

    StringBuilder job;
    StringBuilder education;

    private Uri filePath;

    private final int PICK_IMAGE_REQUEST = 71;

    RelativeLayout parentLayoutRL;
    RelativeLayout parentLayoutPenRL;
    int aPet_id=100;
    int aPen_id=1000;

    List<EditText> allEdPekerjaan = new ArrayList<EditText>();
    List<EditText> allEdPendidikan = new ArrayList<EditText>();
    ProgressBar progressBar;
    Button btn_clock;

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    FirebaseStorage storage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProfile.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        Bundle extras = getIntent().getExtras();
        myIdS = extras.getString(AppConfig.PROFILE_USER_ID);
        myNameS = extras.getString(AppConfig.PROFILE_NAME);
        myEmailS = extras.getString(AppConfig.PROFILE_EMAIL);
        perusahaanS = extras.getString(AppConfig.PROFILE_PERUSAHAAN);
        bagianS = extras.getString(AppConfig.PROFILE_BAGIAN);
        pekerjaanS = extras.getString(AppConfig.PROFILE_PEKERJAAN);
        pendidikanS = extras.getString(AppConfig.PROFILE_PENDIDIKAN);

        editname_et = findViewById(R.id.editname_et);
        pekerjaan_et1 = findViewById(R.id.pekerjaan_et1);
        pendidikan_et = findViewById(R.id.pendidikan_et);
        editbagian_et = findViewById(R.id.editbagian_et);
        editperusahaan_et = findViewById(R.id.editperusahaan_et);
        editProfilePicture = findViewById(R.id.editPictureProfile);
        progressBar = findViewById(R.id.progressBar);

        if("null".equals(perusahaanS)){
            perusahaanS="-";
        }else{
            editperusahaan_et.setText(perusahaanS);
        }
        if("null".equals(bagianS)){
            bagianS="-";
        }else{
            editbagian_et.setText(bagianS);
        }
        if("null".equals(pekerjaanS)){
            pekerjaanS="-";
        }
        if("null".equals(pendidikanS)){
            pendidikanS="-";
        }

        editname_et.setText(myNameS);

        parentLayoutRL = findViewById(R.id.pekerjaanGroup);
        parentLayoutPenRL = findViewById(R.id.pendidikanGroup);
    }

    public void addPekerjaan_et(View view) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams (
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        if(aPet_id==100){
            params.addRule(RelativeLayout.BELOW, R.id.pekerjaan_et1);
        }else{
            params.addRule(RelativeLayout.BELOW, aPet_id);
        }
        params.setMargins(10,10,10,5);
        EditText edittTxt = new EditText(EditProfile.this);

        aPet_id++;
        edittTxt.setHint("Pekerjaan");
        edittTxt.setPadding(5,0,0,0);
        edittTxt.setLayoutParams(params);
        edittTxt.setBackgroundResource(R.drawable.textinputshape);
        edittTxt.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        edittTxt.setId(aPet_id);
        /*InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        edittTxt.setFilters(fArray);*/
        allEdPekerjaan.add(edittTxt);
        parentLayoutRL.addView(edittTxt);
    }

    public void addPendidikan_et(View view) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams (
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        if(aPen_id==1000){
            params.addRule(RelativeLayout.BELOW, R.id.pendidikan_et);
        }else{
            params.addRule(RelativeLayout.BELOW, aPen_id);
        }
        params.setMargins(10,10,10,5);
        EditText edittTxt = new EditText(this);

        aPen_id++;
        edittTxt.setHint("Pendidikan");
        edittTxt.setPadding(5,0,0,0);
        edittTxt.setLayoutParams(params);
        edittTxt.setBackgroundResource(R.drawable.textinputshape);
        edittTxt.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        edittTxt.setId(aPen_id);
        /*InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        edittTxt.setFilters(fArray);*/
        allEdPendidikan.add(edittTxt);
        parentLayoutPenRL.addView(edittTxt);
    }

    public void prosesCreate(View view){
        progressBar.setVisibility(View.VISIBLE);
        pekerjaanS = pekerjaan_et1.getText().toString();
        String[] strings = new String[allEdPekerjaan.size()];

        job = new StringBuilder(pekerjaanS);
        for(int i=0; i < allEdPekerjaan.size(); i++){
            strings[i] = allEdPekerjaan.get(i).getText().toString();
            //pekerjaanS += "\n" + strings[i];
            pekerjaanS += ", " + strings[i];
        }

        pendidikanS = pendidikan_et.getText().toString();

        String[] strings2 = new String[allEdPendidikan.size()];
        for(int i=0; i < allEdPendidikan.size(); i++){
            strings2[i] = allEdPendidikan.get(i).getText().toString();
            pendidikanS += ", " + strings2[i];

        }
        bagianS = editbagian_et.getText().toString();
        perusahaanS = editperusahaan_et.getText().toString();
        myNameS = editname_et.getText().toString().toLowerCase();

        if(!perusahaanS.isEmpty()){
            mFirebaseDatabase.child("Users").child(myIdS).child("company").setValue(perusahaanS);
        }
        if(!bagianS.isEmpty()){
            mFirebaseDatabase.child("Users").child(myIdS).child("divisi").setValue(bagianS);
        }
        if(!myNameS.isEmpty()){
            mFirebaseDatabase.child("Users").child(myIdS).child("name").setValue(myNameS);
        }
        if(!pendidikanS.isEmpty()){
            mFirebaseDatabase.child("Users").child(myIdS).child("education").setValue(pendidikanS);
        }
        if(!pekerjaanS.isEmpty()){
            mFirebaseDatabase.child("Users").child(myIdS).child("job").setValue(pekerjaanS);
        }
        if(filePath!=null){
            uploadPic();
        }else {
            setResult(RESULT_OK);
            finish();
        }
    }

    private void uploadPic() {
        StorageReference ref = storageReference.child("profpic/"+myIdS);
        ref.putFile(filePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        progressBar.setVisibility(View.GONE);
                        String url = taskSnapshot.getDownloadUrl().toString();

                        // use Firebase Realtime Database to store [name + url]
                        mFirebaseDatabase.child("Users").child(myIdS).child("picture").setValue(url);
                        setResult(RESULT_OK);
                        finish();
                        Toast.makeText(EditProfile.this, "Sukses", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressBar.setVisibility(View.GONE);
                        setResult(RESULT_OK);
                        finish();
                        Toast.makeText(EditProfile.this, "Kesalahan: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                    }
                });
    }


    public void photoPicker(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            /*final Bitmap[] bitmap = {null};
            try {
                bitmap[0] = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                btn_clock = (Button) findViewById(R.id.btnRotate);
                btn_clock.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Matrix mMatrix = new Matrix();
                        Matrix mat = editProfilePicture.getImageMatrix();
                        mMatrix.set(mat);
                        mMatrix.setRotate(90);
                        bitmap[0] = Bitmap.createBitmap(bitmap[0], 0, 0, bitmap[0].getWidth(),
                                bitmap[0].getHeight(), mMatrix, false);
                        editProfilePicture.setImageBitmap(bitmap[0]);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            Picasso.with(EditProfile.this)
                    .load(filePath)
                    .resize(96,96)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(editProfilePicture);

        }
    }
}
