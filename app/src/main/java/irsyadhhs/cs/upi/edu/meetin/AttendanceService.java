package irsyadhhs.cs.upi.edu.meetin;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;


import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;
/**
 * Created by HARVI on 2/19/2018.
 */

public class AttendanceService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {
    private LocationListener locationListener;
    private LocationManager locationManager;
    private volatile HandlerThread mHandlerThread;
    private PendingIntent geoFencePendingIntent;
    private final String ATTENDANCE_GEOFENCE_REQ = "attendance_geofence_req";
    private ServiceHandler mServiceHandler;
    double latl = 0;
    double longl = 0;
    double meetingLatl;
    double meetingLongl;
    String memberId;
    String detailId;
    int serviceCode;
    long geoDuration;
    Context ctx;
    public static boolean shouldContinue = true;
    private GoogleApiClient googleApiClient;

    private GeofencingClient mGeofencingClient;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Geofence geofence = createGeofence( meetingLatl, meetingLongl, 50 , geoDuration);
        GeofencingRequest geofenceRequest = createGeofenceRequest( geofence );
        addGeofence( geofenceRequest );
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Status status) {

    }

    // Define how the handler will process messages
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        // Define how to handle any incoming messages here
        @Override
        public void handleMessage(Message message) {
            // ...
            // When needed, stop the service with
            // stopSelf();
        }
    }

    public void onCreate() {
        super.onCreate();
        startServiceWithNotification();
        createGoogleApi();
        googleApiClient.connect();
        // An Android handler thread internally operates on a looper.
        mHandlerThread = new HandlerThread("AttendanceService.HandlerThread");
        mHandlerThread.start();
        // An Android service handler is a handler running on a specific background thread.
        mServiceHandler = new ServiceHandler(mHandlerThread.getLooper());

        mGeofencingClient = LocationServices.getGeofencingClient(this);

    }

    private void createGoogleApi() {
        Log.d(TAG, "createGoogleApi()");
        if ( googleApiClient == null ) {
            googleApiClient = new GoogleApiClient.Builder( this )
                    .addConnectionCallbacks( this )
                    .addOnConnectionFailedListener( this )
                    .addApi( LocationServices.API )
                    .build();
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startServiceWithNotification();

        Bundle extras = intent.getExtras();
        meetingLatl = extras.getDouble(AppConfig.EXTRA_MEETING_LATITUDE);
        meetingLongl = extras.getDouble(AppConfig.EXTRA_MEETING_LONGITUDE);
        serviceCode = extras.getInt(AppConfig.EXTRA_MEETING_ID);
        memberId = extras.getString(AppConfig.EXTRA_MEETING_HOSTID);
        detailId = extras.getString(AppConfig.EXTRA_DETAIL_ID);
        geoDuration = extras.getLong(AppConfig.EXTRA_GEO_DURATION);
        ctx = getApplication().getApplicationContext();

        Log.d(TAG, "service id "+serviceCode);
        Log.d(TAG, "member id "+memberId);
        Log.d(TAG, "meeting lat "+meetingLatl);
        Log.d(TAG, "meeting long "+meetingLongl);

        Log.d("service", "startservice");
        mServiceHandler.post(new Runnable() {
            @Override
            public void run() {
                locationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        longl = location.getLongitude();
                        latl = location.getLatitude();
                        Log.d("mylocation", "lat: "+latl+" | long "+longl);
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctx.startActivity(i);
                    }
                };

                locationManager = (LocationManager) getApplicationContext().getSystemService(getApplicationContext().LOCATION_SERVICE);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
            }
        });

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        shouldContinue = true;
        super.onDestroy();
        googleApiClient.disconnect();
        if(locationManager != null){
            locationManager.removeUpdates(locationListener);
        }
    }

    // Create a Geofence
    private Geofence createGeofence( double latl, double longl, float radius, long geoDuration) {
        Log.d(TAG, "createGeofence");
        return new Geofence.Builder()
                .setRequestId(String.valueOf(serviceCode))
                .setCircularRegion(latl, longl, radius)
                .setExpirationDuration(geoDuration)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
                        | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }

    private GeofencingRequest createGeofenceRequest(Geofence geofence ) {
        Log.d(TAG, "createGeofenceRequest");
        return new GeofencingRequest.Builder()
                .setInitialTrigger( GeofencingRequest.INITIAL_TRIGGER_ENTER )
                .addGeofence( geofence )
                .build();
    }

    private PendingIntent createGeofencePendingIntent() {
        Log.d(TAG, "createGeofencePendingIntent");
        if ( geoFencePendingIntent != null )
            return geoFencePendingIntent;

        Intent intent = new Intent( this, GeofenceIntentService.class);
        intent.setAction(String.valueOf(serviceCode));
        intent.putExtra(AppConfig.EXTRA_MEETING_ID, serviceCode);
        intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, memberId);
        intent.putExtra(AppConfig.EXTRA_DETAIL_ID, detailId);
        return PendingIntent.getService(
                this, serviceCode, intent, PendingIntent.FLAG_UPDATE_CURRENT );
    }

    // Add the created GeofenceRequest to the device's monitoring list
    @SuppressLint("MissingPermission")
    private void addGeofence(GeofencingRequest request) {
        Log.d(TAG, "addGeofence");

        LocationServices.GeofencingApi.addGeofences(
                googleApiClient,
                request,
                createGeofencePendingIntent()
        ).setResultCallback(this);
    }

    void startServiceWithNotification() {
        if (!shouldContinue) return;
        shouldContinue = false;

        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        notificationIntent.setAction(String.valueOf(serviceCode));  // A string containing the action name
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this, serviceCode, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, String.valueOf(serviceCode))
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name))
                .setContentText("running in background")
                .setContentIntent(contentPendingIntent)
                .setOngoing(true)
                .build();
        notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;
        startForeground(serviceCode, notification);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
