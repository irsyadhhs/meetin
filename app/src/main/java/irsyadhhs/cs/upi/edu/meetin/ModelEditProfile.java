package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/24/2018.
 */

public class ModelEditProfile {

    private String name;
    private String job;
    private String education;
    private String divisi;
    private String company;

    public ModelEditProfile(){

    }

    public ModelEditProfile(String name, String job, String education, String divisi, String company){
        this.name = name;
        this.job = job;
        this.education = education;
        this.divisi = divisi;
        this.company = company;
    }
}
