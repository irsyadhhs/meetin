package irsyadhhs.cs.upi.edu.meetin;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

/**
 * Created by HARVI on 2/19/2018.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Intent notifIntent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notifIntent, 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, String.valueOf(0))
                    .setSmallIcon(R.drawable.ic_alarm_black_24dp)
                    .setContentTitle("Sinkronisasi Jadwal")
                    .setContentText("Ketuk disini untuk sinkron jadwal")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);


            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

            notificationManager.notify(0, mBuilder.build());
        }
    }
}