package irsyadhhs.cs.upi.edu.meetin;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMyMeeting extends Fragment {

    @SuppressLint("StaticFieldLeak")
    private static RecyclerView recyclerView;
    ArrayList<ModelMyMeeting> myMeetingList = new ArrayList<>();

    String myIdS, myNameS, timeEnds, meetingIdS;

    SharedPreferences sp;

    TextView main, submain;

    CardView draft;

    private ProgressBar progressBar;

    private int CREATE_MEETING_REQUEST = 2;

    private DatabaseReference mFirebaseDatabase;

    Context mContext;

    private static final String TAG = "FragmentMyMeeting";

    public FragmentMyMeeting() {
        // Required empty public constructor
    }

    public static FragmentMyMeeting newInstance() {
        FragmentMyMeeting fragment = new FragmentMyMeeting();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = this.getActivity().getSharedPreferences(SP, MODE_PRIVATE);
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fragment_neigh, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listViewMyML);
        main = rootView.findViewById(R.id.main);
        submain = rootView.findViewById(R.id.submain);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        draft = rootView.findViewById(R.id.cardDraft);

        mContext = getActivity().getApplicationContext();

        LocalBroadcastManager.getInstance(mContext).registerReceiver(mMessageReceiver,
                new IntentFilter("delete-meeting"));

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getMeeting();
        getDraft();

        FloatingActionButton fab = rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), CreateMeeting.class);
                intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, myIdS);
                intent.putExtra(AppConfig.MODE, "add");
                startActivityForResult(intent, CREATE_MEETING_REQUEST);
            }
        });

        draft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Draft.class);
                startActivity(intent);
            }
        });
        return rootView;
    }

    private void getDraft() {
        mFirebaseDatabase.child("Draft").child(myIdS).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "draft count "+dataSnapshot.getChildrenCount());
                if(dataSnapshot.getChildrenCount() > 0){
                    draft.setVisibility(View.VISIBLE);
                }else{
                    draft.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == CREATE_MEETING_REQUEST) {
            if (resultCode == RESULT_OK) {
                getMeeting();
            }
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getMeeting();
        }
    };

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private void getMeeting() {
        myMeetingList.clear();
        progressBar.setVisibility(View.VISIBLE);
        mFirebaseDatabase.child("MeetingList").orderByChild("hostId").equalTo(myIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0){
                    recyclerView.setVisibility(View.INVISIBLE);
                    main.setVisibility(View.VISIBLE);
                    submain.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                }else {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        String meetingId = childSnapshot.getKey();
                        final ModelMyMeeting modelMyMeeting = childSnapshot.getValue(ModelMyMeeting.class);
                        if (modelMyMeeting == null) {
                            return;
                        }
                        mFirebaseDatabase.child("MeetingDetail").orderByChild("meetingId").equalTo(meetingId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                                    ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                                    if (modelDetail == null) {
                                        return;
                                    }
                                    if(modelDetail.memberId.equals(myIdS)) {
                                        String detailId = childSnapshot.getKey();
                                        modelMyMeeting.setDetailId(detailId);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                Log.e(TAG, "Failed to read user", error.toException());
                            }
                        });

                        timeEnds = modelMyMeeting.getTimeEnd();

                        int dateDiff = getDate(modelMyMeeting.date);
                        long hourDiff = getTime(timeEnds);

                        modelMyMeeting.setMeetingId(meetingId);
                        modelMyMeeting.setDateDiff(dateDiff);
                        modelMyMeeting.setHourDiff(hourDiff);

                        myMeetingList.add(modelMyMeeting);

                        Log.d("MyMeeting", modelMyMeeting.getTitle() + " date + hour + status " +dateDiff+" "+hourDiff+" "+modelMyMeeting.status);

                        /*if(((dateDiff < 0) && (modelMyMeeting.getStatus().equals("0"))) || ((dateDiff == 0) && (hourDiff == -1) && (modelMyMeeting.getStatus().equals("0")))){
                            mFirebaseDatabase.child("MeetingList").child(meetingId).child("status").setValue("1");
                        }*/
                    }
                    Collections.sort(myMeetingList, new Comparator<ModelMyMeeting>() {
                        @Override
                        public int compare(ModelMyMeeting m1, ModelMyMeeting m2) {
                            String st1 = m1.getStatus();
                            String st2 = m2.getStatus();
                            if(st1.equals("2")){
                                st1 = "0";
                            }else if(st2.equals("2")){
                                st2 = "0";
                            }
                            int stComp = st1.compareTo(st2);

                            Integer dd1 = m1.getDateDiff();
                            Integer dd2 = m2.getDateDiff();
                            int ddComp = dd2.compareTo(dd1);

                            if (stComp != 0) {
                                return stComp;
                            } else if(ddComp != 0){
                                return ddComp;
                            } else {
                                Long hd1 = m1.getHourDiff();
                                Long hd2 = m2.getHourDiff();
                                return hd2.compareTo(hd1);
                            }
                        }
                    });
                    if(isAdded()) {
                        AdapterMyMeeting datamember = new AdapterMyMeeting(getActivity(), myMeetingList);
                        recyclerView.setAdapter(datamember);
                        recyclerView.setVisibility(View.VISIBLE);
                        main.setVisibility(View.INVISIBLE);
                        submain.setVisibility(View.INVISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public int getDate(String meetDate) {
        int diff;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String strDate = mdformat.format(calendar.getTime());

        try {
            Date date1 = mdformat.parse(meetDate);
            Date date2 = mdformat.parse(strDate);

            diff = ((int) ((date1.getTime() / (24 * 60 * 60 * 1000)) - (int) (date2.getTime() / (24 * 60 * 60 * 1000))));

            return diff;
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public int getTime(String time) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);

            Date systemDate = Calendar.getInstance().getTime();
            String myDate = format.format(systemDate);
            Date date1 = format.parse(time);
            Date date2 = format.parse(myDate);

            long millse = date1.getTime() - date2.getTime();
            if(millse > 0) {
                return 1;
            }else{
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }
}
