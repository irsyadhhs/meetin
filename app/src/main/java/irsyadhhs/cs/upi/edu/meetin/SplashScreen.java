package irsyadhhs.cs.upi.edu.meetin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences sp;
    int REQUEST_CODE_INTRO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getSharedPreferences(SP, MODE_PRIVATE);
        REQUEST_CODE_INTRO = 99;
        if(sp.getString("tutorial", "yes").equals("yes")){
            startActivityForResult(new Intent(SplashScreen.this, MainIntroActivity.class), REQUEST_CODE_INTRO);
        }else{
            startActivity(new Intent(SplashScreen.this, Login.class));
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_INTRO) {
            if (resultCode == RESULT_OK) {
                sp.edit().putString("tutorial", "no").apply();
                startActivity(new Intent(SplashScreen.this, Login.class));
                finish();
            } else {
                finish();
            }
        }
    }
}
