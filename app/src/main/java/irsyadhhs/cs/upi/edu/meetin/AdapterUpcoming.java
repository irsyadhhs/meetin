package irsyadhhs.cs.upi.edu.meetin;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by HARVI on 11/17/2017.
 */

public class AdapterUpcoming extends RecyclerView.Adapter<AdapterUpcoming.ViewHolder> {
    private static ArrayList<ModelMeeting> searchArrayList;

    private LayoutInflater mInflater;

    public AdapterUpcoming(Context context, ArrayList<ModelMeeting> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView title, location, date, time;

        public ViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.outtitle);
            this.location = (TextView) itemView.findViewById(R.id.outlocation);
            this.date = (TextView) itemView.findViewById(R.id.outdate);
            this.time = (TextView) itemView.findViewById(R.id.outtime);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(v.getContext(), DetailMeeting.class);
                    intent.putExtra(AppConfig.EXTRA_MEETING_ID, searchArrayList.get(position).getMeetingId());
                    intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, searchArrayList.get(position).hostId);
                    intent.putExtra(AppConfig.EXTRA_MEETING_STATUS, searchArrayList.get(position).status);
                    intent.putExtra(AppConfig.EXTRA_MEETING_DATE, searchArrayList.get(position).date);
                    intent.putExtra(AppConfig.EXTRA_ADMIN, searchArrayList.get(position).getAdmin());
                    intent.putExtra(AppConfig.EXTRA_DETAIL_ID, searchArrayList.get(position).getDetailId());

                    intent.putExtra(AppConfig.EXTRA_MEETING_HOSTNAME, searchArrayList.get(position).hostName);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, searchArrayList.get(position).meetingTitle);
                    intent.putExtra(AppConfig.EXTRA_MEETING_ABOUT, searchArrayList.get(position).about);
                    intent.putExtra(AppConfig.EXTRA_MEETING_DESCRIPTION, searchArrayList.get(position).description);
                    intent.putExtra(AppConfig.EXTRA_MEETING_LOCATION, searchArrayList.get(position).location);

                    intent.putExtra(AppConfig.EXTRA_MEETING_TIMESTART, searchArrayList.get(position).timeStart);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TIMEEND, searchArrayList.get(position).timeEnd);
                    intent.putExtra(AppConfig.EXTRA_MEETING_LATITUDE, String.valueOf(searchArrayList.get(position).latitude));
                    intent.putExtra(AppConfig.EXTRA_MEETING_LONGITUDE, String.valueOf(searchArrayList.get(position).longitude));

                    intent.putExtra(AppConfig.EXTRA_MEETING_CODE, searchArrayList.get(position).code);

                    intent.putExtra(AppConfig.EXTRA_DETAIL_MODE, "home");

                    v.getContext().startActivity(intent);

                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.title.setText(searchArrayList.get(position).meetingTitle);
        holder.location.setText(searchArrayList.get(position).location);
        holder.date.setText(searchArrayList.get(position).date);
        holder.time.setText(searchArrayList.get(position).timeStart);
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }
}
