package irsyadhhs.cs.upi.edu.meetin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class FragmentHistory extends Fragment {

    Context mContext;
    private static RecyclerView recyclerViewH;

    ArrayList<ModelMeeting> isiHistory = new ArrayList<>();


    SharedPreferences sp;

    String myIdS, myNameS, meetingIdS;

    TextView noUpcomingTV, noHistoryTV;

    private ProgressBar progressBar;

    private DatabaseReference mFirebaseDatabase;

    public FragmentHistory() {
        // Required empty public constructor
    }

    public static FragmentHistory newInstance() {
        FragmentHistory fragment = new FragmentHistory();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity().getApplicationContext();

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        sp = this.getActivity().getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fragment_history, container, false);

        recyclerViewH = (RecyclerView) rootView.findViewById(R.id.listViewMeetingHistory);
        noUpcomingTV = rootView.findViewById(R.id.noUpcomingTV);
        noHistoryTV = rootView.findViewById(R.id.noHistoryTV);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        LocalBroadcastManager.getInstance(mContext).registerReceiver(mMessageReceiver,
                new IntentFilter("delete-meeting-home"));

        recyclerViewH.setHasFixedSize(true);
        recyclerViewH.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewH.setItemAnimator(new DefaultItemAnimator());

        getMeetingDetail();
        return rootView;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getMeetingDetail();
        }
    };

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    public void sortList(ArrayList<ModelMeeting> list) {
        Collections.sort(list, new Comparator<ModelMeeting>() {
            @Override
            public int compare(ModelMeeting m1, ModelMeeting m2) {
                Integer dd1 = m1.getDateDiff();
                Integer dd2 = m2.getDateDiff();
                int ddComp;
                ddComp = dd2.compareTo(dd1);

                if (ddComp != 0) {
                    return ddComp;
                } else {
                    Long hd1 = m1.getHourDiff();
                    Long hd2 = m2.getHourDiff();

                    return hd2.compareTo(hd1);
                }
            }
        });
    }

    private void getMeeting(final ModelDetail model, final String detailId) {
        mFirebaseDatabase.child("MeetingList").child(model.meetingId).addListenerForSingleValueEvent(new ValueEventListener() {
            int status = 0;

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    ModelMeeting modelMeeting = dataSnapshot.getValue(ModelMeeting.class);
                    if (modelMeeting == null) {
                        return;
                    }
                    modelMeeting.setMeetingId(model.meetingId);
                    modelMeeting.setAdmin(model.getAdmin());
                    modelMeeting.setAttendance(model.attendance);
                    modelMeeting.setDetailId(detailId);

                    int dateDiff = getDate(modelMeeting.date);
                    long hourDiff = getTime(modelMeeting.timeEnd);
                    modelMeeting.setDateDiff(dateDiff);
                    modelMeeting.setHourDiff(hourDiff);

                    if (dateDiff < 0 || ((dateDiff == 0) && (hourDiff == -1)) || ("1".equals(modelMeeting.status))) {
                        status = 1;
                        isiHistory.add(modelMeeting);
                    }
                    if (status == 1) {
                        recyclerViewH.setVisibility(View.VISIBLE);
                        noHistoryTV.setVisibility(View.INVISIBLE);
                    }
                    sortList(isiHistory);
                    if (isAdded()) {
                        recyclerViewH.setAdapter(new AdapterHistory(getActivity().getApplicationContext(), isiHistory));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void getMeetingDetail() {
        progressBar.setVisibility(View.VISIBLE);
        isiHistory.clear();
        mFirebaseDatabase.child("MeetingDetail").orderByChild("memberId").equalTo(myIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    recyclerViewH.setVisibility(View.INVISIBLE);
                    noHistoryTV.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    int status = 0;
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                        if (modelDetail == null) {
                            return;
                        }
                        if ("1".equals(modelDetail.approve)) {
                            meetingIdS = modelDetail.meetingId;
                            String detailId = childSnapshot.getKey();
                            getMeeting(modelDetail, detailId);
                            status = 1;
                        }
                    }
                    if (status != 1) {
                        recyclerViewH.setVisibility(View.INVISIBLE);
                        noHistoryTV.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public int getDate(String dateS) {
        int diff;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String strDate = mdformat.format(calendar.getTime());

        try {
            Date date1 = mdformat.parse(dateS);
            Date date2 = mdformat.parse(strDate);

            diff = ((int) ((date1.getTime() / (24 * 60 * 60 * 1000)) - (int) (date2.getTime() / (24 * 60 * 60 * 1000))));

            return diff;
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public long getTime(String timeEndS) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);

            Date systemDate = Calendar.getInstance().getTime();
            String myDate = format.format(systemDate);
            Date date1 = format.parse(timeEndS);
            Date date2 = format.parse(myDate);


            long millse = date1.getTime() - date2.getTime();
            if (millse > 0) {
                return millse;
            } else {
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }
}
