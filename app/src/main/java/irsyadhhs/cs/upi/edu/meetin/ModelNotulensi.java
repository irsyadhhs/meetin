package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 3/13/2018.
 */

public class ModelNotulensi {
    public String author;
    public String notulensi;
    public String share="0";

    public ModelNotulensi(){
        //required empty constructor
    }

    public ModelNotulensi(String author, String notulensi) {
        this.author = author;
        this.notulensi = notulensi;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getNotulensi() {
        return notulensi;
    }

    public void setNotulensi(String notulensi) {
        this.notulensi = notulensi;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

}
