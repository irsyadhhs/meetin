package irsyadhhs.cs.upi.edu.meetin;

import android.*;
import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;
import java.util.concurrent.Executor;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

/**
 * Created by HARVI on 4/21/2018.
 */

public class ServiceAttendance extends Service{

    private GeofencingClient mGeofencingClient;
    private PendingIntent geoFencePendingIntent;
    private ValueEventListener mConnectionListener;
    DatabaseReference connectedRef;
    int serviceCode;
    double meetingLatl, meetingLongl;
    long geoDuration;
    String memberId, detailId, meetingId;
    NotificationManager mNotificationManager;
    Context mContext;

    private DatabaseReference mFirebaseDatabase;

    FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback mLocationCallback;

    public static boolean shouldContinue = true;
    private final int UPDATE_INTERVAL =  10000;
    private final int FASTEST_INTERVAL = 3000;

    private static final String TAG = "serviceattendance";
    private static final int mRadius = 25;
;
    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mGeofencingClient = LocationServices.getGeofencingClient(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void checkConnection(){
        mConnectionListener = connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (!connected) {
                    Intent onNetwork = new Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS);
                    onNetwork.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 1997, onNetwork, 0);

                    String channelId = "";
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                        channelId = createNotificationChannel();
                    }else{
                        channelId = "1997";
                    }

                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, channelId)
                            .setSmallIcon(R.drawable.ic_warning_black_24dp)
                            .setContentTitle("Koneksi data internet tidak tersedia")
                            .setContentText("Gunakan metode kehadiran yang lain")
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            // Set the intent that will fire when the user taps the notification
                            .setContentIntent(pendingIntent)
                            .setChannelId(channelId)
                            .setAutoCancel(true);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);

                    notificationManager.notify(1997, mBuilder.build());
                }else{
                    NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    if(notificationManager!=null) {
                        notificationManager.cancel(1997);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

    private Geofence createGeofence(double latl, double longl, float radius, long geoDuration) {
        Log.d(TAG, "createGeofence");
        return new Geofence.Builder()
                .setRequestId(String.valueOf(serviceCode))
                .setCircularRegion(latl, longl, radius)
                .setExpirationDuration(geoDuration)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
                        | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }

    private GeofencingRequest createGeofenceRequest(Geofence geofence) {
        Log.d(TAG, "createGeofenceRequest");
        return new GeofencingRequest.Builder()
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                .addGeofence(geofence)
                .build();
    }

    private PendingIntent createGeofencePendingIntent() {
        Log.d(TAG, "createGeofencePendingIntent");
        if (geoFencePendingIntent != null)
            return geoFencePendingIntent;

        Intent intent = new Intent(this, GeofenceIntentService.class);
        intent.setAction(String.valueOf(serviceCode));
        intent.putExtra(AppConfig.EXTRA_MEETING_ID, serviceCode);
        intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, memberId);
        intent.putExtra(AppConfig.EXTRA_DETAIL_ID, detailId);
        return PendingIntent.getService(
                this, serviceCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void addGeofence(GeofencingRequest request) {
        Log.d(TAG, "addGeofence");


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGeofencingClient.addGeofences(request, createGeofencePendingIntent())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Geofence is successfully added");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Geofence is failed to add");
                    }
                });
    }

    void startServiceWithNotification() {
        if (!shouldContinue){
            return;
        }
        shouldContinue = false;

        //if(!sync.equals("1")){
            //mFirebaseDatabase.child("MeetingDetail").child(detailId).child("sync").setValue(1);
            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
            notificationIntent.setAction(String.valueOf(serviceCode));  // A string containing the action name
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent contentPendingIntent = PendingIntent.getActivity(getApplicationContext(), serviceCode, notificationIntent, 0);

            String channelId = "";
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                channelId = createNotificationChannel();
            }

            Notification notification = new NotificationCompat.Builder(mContext, String.valueOf(serviceCode))
                    .setSmallIcon(R.drawable.ic_alarm_black_24dp)
                    .setContentTitle("Meetin berjalan")
                    .setContentText("Pertemuan sedang berlangsung")
                    .setContentIntent(contentPendingIntent)
                    .setChannelId(channelId)
                    .build();
            notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;
            startForeground(serviceCode, notification);
        //}
    }

    @Override
    public void onDestroy() {
        shouldContinue = true;
        super.onDestroy();
        //mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(AppConfig.ACTION_START_SERVICE.equals(intent.getAction())) {
            Log.d("service", "startservice");
            shouldContinue = true;

            checkConnection();

            Bundle extras = intent.getExtras();
            meetingLatl = extras.getDouble(AppConfig.EXTRA_MEETING_LATITUDE);
            meetingLongl = extras.getDouble(AppConfig.EXTRA_MEETING_LONGITUDE);
            serviceCode = extras.getInt(AppConfig.EXTRA_GEO_ID);
            meetingId = extras.getString(AppConfig.EXTRA_MEETING_ID);
            memberId = extras.getString(AppConfig.EXTRA_MEETING_HOSTID);
            detailId = extras.getString(AppConfig.EXTRA_DETAIL_ID);
            geoDuration = extras.getLong(AppConfig.EXTRA_GEO_DURATION);

            Log.d(TAG, "service attendance detaildId "+detailId);
            startServiceWithNotification();

            Geofence geofence = createGeofence(meetingLatl, meetingLongl, mRadius, geoDuration);
            GeofencingRequest geofenceRequest = createGeofenceRequest(geofence);
            addGeofence(geofenceRequest);
            getLastKnownLocation();

        }else{
            Log.d("service", "stopservice");
            shouldContinue = false;
            if(mLocationCallback != null) {
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
            if(mConnectionListener != null) {
                connectedRef.removeEventListener(mConnectionListener);
            }
            //mFirebaseDatabase.child("MeetingList").child(meetingId).child("status").setValue("1");
            stopSelf();
        }

        return START_REDELIVER_INTENT;
    }


    // Get last known location
    private void getLastKnownLocation() {
        Log.d(TAG, "getLastKnownLocation()");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "last location not allowed");
            return;
        }
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    Log.d(TAG, "location request is null");
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Log.d(TAG, "updated latitude "+location.getLatitude());
                    Log.d(TAG, "updated longitude "+location.getLongitude());
                }
            };
        };
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations, this can be null.
                        if (location != null) {
                            Log.d(TAG, "last latitude "+location.getLatitude());
                            Log.d(TAG, "last longitude "+location.getLongitude());
                            startLocationUpdates();
                        }
                    }
                });
    }

    private void startLocationUpdates(){
        Log.i(TAG, "startLocationUpdates()");
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "request update not allowed");
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(){
        String channelId = "ongoing_service";
        String channelName = "Schedule Ongoing Info";
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_MIN);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.createNotificationChannel(chan);
        return channelId;
    }
}
