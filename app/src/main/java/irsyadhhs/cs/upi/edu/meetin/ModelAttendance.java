package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/25/2018.
 */

public class ModelAttendance {
    private String detailId;
    private String meetingId;
    private String memberId;
    private String memberName;
    private String attendance;
    private String timeArrive;
    private String timeLeave;
    private String witnessName;
    private String witnessState = "0";
    public String picture;
    public String approve;
    public String handshake="0";

    public ModelAttendance(){

    }

    public ModelAttendance(String detailId, String meetingId, String memberId, String memberName, String attendance, String timeArrive, String timeLeave, String approve){
        this.detailId = detailId;
        this.meetingId = meetingId;
        this.memberId = memberId;
        this.memberName = memberName;
        this.timeArrive = timeArrive;
        this.timeLeave = timeLeave;
        this.attendance = attendance;
        this.approve = approve;
    }

    public String getDetailId(){
        return  this.detailId;
    }
    public void setDetailId(String detailId){
        this.detailId = detailId;
    }

    public String getMeetingId(){
        return  this.meetingId;
    }
    public void setMeetingId(String meetingId){
        this.meetingId = meetingId;
    }

    public String getMemberId(){
        return  this.memberId;
    }
    public void setMemberId(String memberId){
        this.memberId = memberId;
    }

    public String getMemberName(){
        return  this.memberName;
    }
    public void setMemberName(String memberName){
        this.memberName = memberName;
    }

    public String getAttendance(){
        return  this.attendance;
    }
    public void setAttendance(String attendance){
        this.attendance = attendance;
    }

    public void setTimeArrive(String timeArrive){
        this.timeArrive = timeArrive;
    }
    public String getTimeArrive(){
        return this.timeArrive;
    }

    public void setTimeLeave(String timeLeave){
        this.timeLeave = timeLeave;
    }
    public String getTimeLeave(){return this.timeLeave;}

    public void setWitnessName(String witnessName){
        this.witnessName = witnessName;
    }
    public String getWitnessName(){return this.witnessName;}

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getHandshake() {
        return handshake;
    }

    public void setHandshake(String handshake) {
        this.handshake = handshake;
    }

    public String getWitnessState() {
        return witnessState;
    }

    public void setWitnessState(String witnessState) {
        this.witnessState = witnessState;
    }


}
