package irsyadhhs.cs.upi.edu.meetin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentRelation extends Fragment{

    RelativeLayout rl;

    private static RecyclerView recyclerView;

    ArrayList<ModelRelation> isiRelation = new ArrayList<>();

    TextView noRelationTV;

    SharedPreferences sp;
    String myIdS, myNameS;

    private ProgressBar progressBar;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseRecyclerAdapter<ModelRelation, RelationHolder> mRelationAdapter;

    Context context;

    RelativeLayout.LayoutParams params;

    public FragmentRelation() {
        // Required empty public constructor
    }

    public static FragmentRelation newInstance() {
        FragmentRelation fragment = new FragmentRelation();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();

        sp = context.getSharedPreferences(SP, MODE_PRIVATE);
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fragment_around, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listViewRelation);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        noRelationTV = rootView.findViewById(R.id.noRelationTV);
        rl = rootView.findViewById(R.id.groupFindRelation);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        rl.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), FindRelation.class);
                startActivity(intent);
            }
        });

        params = new
                RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        populateRelation();
        mRelationAdapter.startListening();
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRelationAdapter.stopListening();
    }

    public void populateRelation(){
        Query relationRef = mFirebaseDatabase.child("Relation").child(myIdS);

        FirebaseRecyclerOptions personsOptions = new FirebaseRecyclerOptions.Builder<ModelRelation>().setQuery(relationRef, ModelRelation.class).build();

        mRelationAdapter = new FirebaseRecyclerAdapter<ModelRelation, RelationHolder>(personsOptions) {
            @Override
            protected void onBindViewHolder(final RelationHolder holder, int position, final ModelRelation model) {
                if("1".equals(model.approve)) {
                    mFirebaseDatabase.child("Users").child(model.relationUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                            if (modelUser == null) {
                                mFirebaseDatabase.child("Relation").child(myIdS).child(model.relationUserId).removeValue();
                                return;
                            }
                            holder.setRelation(modelUser.name, modelUser.picture);
                            mFirebaseDatabase.child("Relation").child(myIdS).child(model.relationUserId).child("relationName").setValue(modelUser.name);
                            model.setRelationPicture(modelUser.picture);
                        }

                        @Override
                        public void onCancelled(DatabaseError error) {
                            Log.e(TAG, "Failed to read user", error.toException());
                        }
                    });
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(v.getContext(), Profile.class);
                            intent.putExtra(AppConfig.PROFILE_USER_ID, String.valueOf(model.relationUserId));
                            v.getContext().startActivity(intent);

                        }
                    });
                }else{
                    // Set the height by params
                    params.height=0;
                    holder.itemView.setLayoutParams(params);

                }
            }
            @Override
            public RelationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.relation_list, parent, false);

                return new RelationHolder(view);
            }
        };
        recyclerView.setAdapter(mRelationAdapter);
    }

    public class RelationHolder extends RecyclerView.ViewHolder{
        TextView name;
        ImageView img;
        ProgressBar progressBarPic;
        public RelationHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.relationName);
            this.img = itemView.findViewById(R.id.memberPhotoRelation);
            this.progressBarPic = itemView.findViewById(R.id.progressBarPic);
        }

        public void setRelation(String relationName, String picture){
            this.progressBarPic.setVisibility(View.VISIBLE);
            this.name.setText(relationName);

            Picasso.with(context)
                    .load(picture)
                    .resize(72,72)
                    .centerCrop()
                    .error(R.drawable.ic_account_circle_black_48dp)
                    .placeholder(R.drawable.ic_account_circle_black_48dp)
                    .transform(new CircleTransform())
                    .into(this.img, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBarPic.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBarPic.setVisibility(View.GONE);
                        }
                    });
        }
    }

}
