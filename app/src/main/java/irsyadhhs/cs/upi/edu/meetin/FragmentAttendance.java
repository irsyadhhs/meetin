package irsyadhhs.cs.upi.edu.meetin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

/**
 * Created by HARVI on 3/18/2018.
 */

public class FragmentAttendance extends Fragment {
    public boolean MY_ATTENDANCE_STATUS = false;
    public boolean MEETING_RUNNING_STATUS = false;
    public boolean MEMBER_TAGGED_STATUS = false;

    private static RecyclerView recyclerView;

    TextView titleTV;
    String titleS, meetingIdS, timeEndS, timeStartS, dateS, detailIdS;
    RelativeLayout groupInfo;
    ImageView infoIcon;

    private DatabaseReference mFirebaseDatabase;

    public static final String ACTION = "irsyadhhs.cs.upi.edu.meetin.GeofenceIntentService";

    private FirebaseRecyclerAdapter<ModelAttendance, FragmentAttendance.AttendanceHolder> attendanceAdapter;

    SharedPreferences sp;
    String myIdS, myNameS;

    Context context;

    RelativeLayout.LayoutParams params;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_attendance,container,false);
        bindingView(view);
        sp = context.getSharedPreferences(SP, MODE_PRIVATE);
        if(sp.getString(AppConfig.INFO_TAG_PREF, "").equals("yes")){
            groupInfo.setVisibility(View.VISIBLE);
        }else{
            groupInfo.setVisibility(View.GONE);
        }

        Bundle extras = this.getArguments();
        titleS = extras.getString(AppConfig.EXTRA_MEETING_TITLE);
        meetingIdS = extras.getString(AppConfig.EXTRA_MEETING_ID);
        timeEndS = extras.getString(AppConfig.EXTRA_MEETING_TIMEEND);
        timeStartS = extras.getString(AppConfig.EXTRA_MEETING_TIMESTART);
        dateS = extras.getString(AppConfig.EXTRA_MEETING_DATE);
        detailIdS = extras.getString(AppConfig.EXTRA_DETAIL_ID);

        this.myIdS = sp.getString(AppConfig.MY_ID, "0");
        this.myNameS = sp.getString(AppConfig.MY_NAME, "0");

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        params = new
                RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        LocalBroadcastManager.getInstance(context).registerReceiver(mAttendanceReceiver,
                new IntentFilter(ACTION));

        populateAttendance();
        attendanceAdapter.startListening();
        titleTV.setText(titleS);
        infoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeInfoTag();
            }
        });
        return view;
    }

    private BroadcastReceiver mAttendanceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*Bundle extras = intent.getExtras();
            String status = extras.getString("attendance");
            if("enter".equals(status)){
                getMyAttendance();
                populateAttendance();
            }*/

        }
    };

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mAttendanceReceiver);
        super.onDestroy();
    }

    private void bindingView(View view) {
        titleTV = view.findViewById(R.id.titleMeetingAttendance);
        groupInfo = view.findViewById(R.id.groupTagInfo);
        recyclerView = view.findViewById(R.id.listViewAnggotaHadir);
        infoIcon = view.findViewById(R.id.infoIcon);
    }

    public void closeInfoTag() {
        sp.edit().putString(AppConfig.INFO_TAG_PREF, "no").apply();
        groupInfo.setVisibility(View.GONE);
    }

    public long checkTime(String time) {
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        Date systemDate = Calendar.getInstance().getTime();
        String myDate = mdformat.format(systemDate);

        try {
            long date1 = mdformat.parse(myDate).getTime();
            long date2 = mdformat.parse(time).getTime();
            return date2 - date1;
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    private void getWitness(final TextView tagByMember, final ModelAttendance modelAttendance, final String text) {
        mFirebaseDatabase.child("MeetingDetail")
                .child(modelAttendance.getDetailId())
                .child("witness")
                .orderByChild("witnessName").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String witnessName = text + " ";
                long count = dataSnapshot.getChildrenCount();
                int i = 0;
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    i++;
                    ModelAttendance model = childSnapshot.getValue(ModelAttendance.class);
                    witnessName += model.getWitnessName();
                    if(i != count){
                        witnessName += ", ";
                    }
                }
                Log.d("attendanceAdapter", "in getWitness "+modelAttendance.getMemberName());
                tagByMember.setText(witnessName);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void getHandshake(final TextView seenWith, final ModelAttendance modelAttendance, final String text) {
        mFirebaseDatabase.child("MeetingList")
                .child(modelAttendance.getMeetingId())
                .child("handshake")
                .child(modelAttendance.getMemberId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String witnessName = text + " ";
                long count = dataSnapshot.getChildrenCount();
                int i = 0;
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    i++;
                    witnessName +=  childSnapshot.getValue().toString();
                    if(i != count){
                        witnessName += ", ";
                    }
                }
                seenWith.setText(witnessName);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void getMyAttendance(){
        mFirebaseDatabase.child("MeetingDetail").child(detailIdS).child("attendance").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    String attendance = dataSnapshot.getValue().toString();
                    Log.d(TAG, "my attendance "+attendance);
                    if ("0".equals(attendance)) {
                        MY_ATTENDANCE_STATUS = false;
                    }else{
                        MY_ATTENDANCE_STATUS = true;
                    }
                    Log.d(TAG, "my attendance status "+MY_ATTENDANCE_STATUS);
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void populateAttendance(){
        if(detailIdS != null) {
            getMyAttendance();
        }
        final long millseEnd = checkTime(dateS + " " + timeEndS);
        final long millseStart = checkTime(dateS + " " + timeStartS);

        if((millseEnd < 0)||(millseStart > 0)){
            MEETING_RUNNING_STATUS = false;
        }else{
            MEETING_RUNNING_STATUS = true;
        }

        DatabaseReference notulensiRef = mFirebaseDatabase.child("MeetingDetail");
        Query query = notulensiRef.orderByChild("meetingId").equalTo(meetingIdS);
        FirebaseRecyclerOptions personsOptions = new FirebaseRecyclerOptions.Builder<ModelAttendance>().setQuery(query, ModelAttendance.class).build();

        attendanceAdapter = new FirebaseRecyclerAdapter<ModelAttendance, FragmentAttendance.AttendanceHolder>(personsOptions) {
            @Override
            protected void onBindViewHolder(final FragmentAttendance.AttendanceHolder holder, final int position, final ModelAttendance model) {
                model.setDetailId(getRef(position).getKey());
                mFirebaseDatabase.child("Users").child(model.getMemberId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                        if (modelUser == null) {
                            return;
                        }
                        holder.setPicture(modelUser.picture);
                    }
                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.e(TAG, "Failed to read user", error.toException());
                    }
                });

                if("0".equals(model.getAttendance())) {
                    holder.setNoAttendance(model);
                    tagMember(holder,model);
                }else{
                    if("1".equals(model.getWitnessState())){
                        holder.setAttendance(model);
                        mFirebaseDatabase.child("MeetingDetail").child(getRef(position).getKey()).child("witness")
                                .child(myIdS)
                                .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Log.d(TAG, "tag attendance member "+dataSnapshot.getChildrenCount());
                                if(dataSnapshot.getChildrenCount()!=0){
                                    MEMBER_TAGGED_STATUS = true;
                                }else{
                                    MEMBER_TAGGED_STATUS = false;
                                }
                                tagMember(holder,model);
                            }
                            @Override
                            public void onCancelled(DatabaseError error) {
                                Log.e(TAG, "Failed to read user", error.toException());
                            }
                        });
                    }else {
                        holder.setAttendance(model);
                    }
                    Log.d(TAG, "myId and memberId "+myIdS+" "+model.getMemberId());
                }
            }
            @Override
            public FragmentAttendance.AttendanceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.attendance_item_list, parent, false);

                return new FragmentAttendance.AttendanceHolder(view);
            }
        };
        recyclerView.setAdapter(attendanceAdapter);
    }

    public void tagMember(final FragmentAttendance.AttendanceHolder holder, final ModelAttendance model){
        if(myIdS.equals(model.getMemberId()) || !MEETING_RUNNING_STATUS) {
            holder.addTag.setVisibility(View.GONE);
        }else if(!MY_ATTENDANCE_STATUS || MEMBER_TAGGED_STATUS){
            holder.addTag.setVisibility(View.VISIBLE);
            holder.addTag.setTextColor(Color.parseColor("#9e9e9e"));
            holder.addTag.setClickable(false);
            holder.addTag.setEnabled(false);
        }else{
            holder.addTag.setVisibility(View.VISIBLE);
            holder.addTag.setTextColor(Color.parseColor("#000000"));
            holder.addTag.setClickable(true);
            holder.addTag.setEnabled(true);
        }

        Log.d(TAG, "name attendance meeting member "+model.getMemberName()+" "
                +MY_ATTENDANCE_STATUS+" "+MEETING_RUNNING_STATUS+" "+MEMBER_TAGGED_STATUS);

        holder.addTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
                ad.setMessage("Dengan menandai anggota lain hadir, Anda bersaksi dan bertanggung jawab atas pencatatan kehadirannya. "+
                        "Tindakan ini tidak dapat diurungkan")
                        .setTitle("Menandai "+ model.getMemberName())
                        .setCancelable(false)
                        .setPositiveButton("Tandai", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
                                Date systemDate = Calendar.getInstance().getTime();
                                String timeArrive = format.format(systemDate);

                                String detailId = model.getDetailId();
                                HashMap<String, String> witness = new HashMap<>();
                                witness.put("witnessName", myNameS);
                                mFirebaseDatabase.child("MeetingDetail").child(detailId).child("witnessState").setValue("1");
                                mFirebaseDatabase.child("MeetingDetail").child(detailId).child("attendance").setValue("1");
                                mFirebaseDatabase.child("MeetingDetail").child(detailId).child("witness").child(myIdS).setValue(witness);
                                if("0".equals(model.getTimeArrive())) {
                                    mFirebaseDatabase.child("MeetingDetail").child(detailId).child("timeArrive").setValue(timeArrive);
                                    mFirebaseDatabase.child("MeetingDetail").child(detailId).child("timeLeave").setValue(timeEndS);
                                }
                            }
                        })
                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ad.create().dismiss();
                            }
                        });

                AlertDialog alert = ad.create();
                alert.show();
            }
        });
    }

    public class AttendanceHolder extends RecyclerView.ViewHolder{
        TextView member;
        TextView tagByMember;
        TextView seenWith;
        TextView addTag;
        TextView timeArriveTV;
        TextView timeLeaveTV;
        ImageView picture;

        public AttendanceHolder(View itemView) {
            super(itemView);
            this.member = (TextView) itemView.findViewById(R.id.member);
            this.addTag = itemView.findViewById(R.id.addTag);
            this.tagByMember = itemView.findViewById(R.id.tagByMember);
            this.seenWith = itemView.findViewById(R.id.seenWith);
            this.timeArriveTV = itemView.findViewById(R.id.timeArriveTV);
            this.timeLeaveTV = itemView.findViewById(R.id.timeLeaveTV);
            this.picture = itemView.findViewById(R.id.memberPicture);
        }

        public void setAttendance(ModelAttendance modelAttendance){
            Log.d("attendanceAdapter", "in setAttendance "+modelAttendance.getMemberName());
            this.addTag.setVisibility(View.GONE);
            this.member.setText(modelAttendance.getMemberName());
            this.member.setTextColor(Color.parseColor("#000000"));

            if("1".equals(modelAttendance.getWitnessState())){
                tagByMember.setVisibility(View.VISIBLE);
                getWitness(tagByMember, modelAttendance, "Ditandai oleh");
            }
            if("1".equals(modelAttendance.getHandshake())){
                seenWith.setVisibility(View.VISIBLE);
                getHandshake(seenWith, modelAttendance, "Terlihat bersama dengan");
            }
            if(!"0".equals(modelAttendance.getTimeArrive())){
                timeArriveTV.setVisibility(View.VISIBLE);
                timeArriveTV.setText(modelAttendance.getTimeArrive()+" - ");
            }
            if(!"0".equals(modelAttendance.getTimeLeave())){
                timeLeaveTV.setVisibility(View.VISIBLE);
                timeLeaveTV.setText(modelAttendance.getTimeLeave());
            }
        }

        public void setNoAttendance(ModelAttendance modelAttendance){
            this.addTag.setVisibility(View.VISIBLE);
            this.member.setText(modelAttendance.getMemberName());
            this.member.setTextColor(Color.parseColor("#9e9e9e"));

            if(!"1".equals(modelAttendance.approve)){
                params.height=0;
                this.itemView.setLayoutParams(params);
            }
            if((modelAttendance.getMemberId().equals(myIdS))&&("0".equals(modelAttendance.getAttendance()))){
                this.addTag.setVisibility(View.GONE);
            }
            if(!MY_ATTENDANCE_STATUS){
                this.addTag.setTextColor(Color.parseColor("#9e9e9e"));
            }
        }

        public void setPicture(String picture){
            Picasso.with(context)
                    .load(picture)
                    .resize(96,96)
                    .centerCrop()
                    .error(R.drawable.ic_account_circle_black_48dp)
                    .placeholder(R.drawable.ic_account_circle_black_48dp)
                    .transform(new CircleTransform())
                    .into(this.picture);
        }
    }
}
