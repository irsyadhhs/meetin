package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 3/12/2018.
 */

public class ModelMessage {
    private String body;
    private String from;
    private String to;

    public ModelMessage(String body, String from, String to) {
        this.body = body;
        this.from = from;
        this.to = to;
    }

    public ModelMessage() {
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    public String getBody() {
        return body;
    }
}
