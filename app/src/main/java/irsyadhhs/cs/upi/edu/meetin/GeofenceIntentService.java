package irsyadhhs.cs.upi.edu.meetin;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.google.android.gms.common.GooglePlayServicesUtil.getErrorString;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.HADIR;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.KEHADIRAN;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TIDAK_HADIR;

/**
 * Created by HARVI on 2/22/2018.
 */

public class GeofenceIntentService extends IntentService {

    String memberId;
    String detailId;
    int meetingId;
    public boolean UPDATE_TIME;
    SharedPreferences sp;
    public static volatile boolean shouldContinue = true;

    public static final String ACTION = "irsyadhhs.cs.upi.edu.meetin.GeofenceIntentService";

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    public GeofenceIntentService() {
        super("GeofenceIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();
        UPDATE_TIME = true;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Bundle extras = intent.getExtras();
        meetingId = extras.getInt(AppConfig.EXTRA_MEETING_ID);
        memberId = extras.getString(AppConfig.EXTRA_MEETING_HOSTID);
        detailId = extras.getString(AppConfig.EXTRA_DETAIL_ID);
        sp = getSharedPreferences(SP, MODE_PRIVATE);

        if (!shouldContinue) {
            stopSelf();
            return;
        }

        // Retrieve the Geofencing intent
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        // Handling errors
        if ( geofencingEvent.hasError() ) {
            String errorMsg = GeofenceErrorMessages.getErrorString(this,
                    geofencingEvent.getErrorCode());
            Log.e( TAG, errorMsg );
            return;
        }

        // Retrieve GeofenceTrasition
        int geoFenceTransition = geofencingEvent.getGeofenceTransition();
        // Check if the transition type
        if ( geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT ) {
            // Get the geofence that were triggered
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();
            // Create a detail message with Geofences received
            getGeofenceTrasitionDetails(geoFenceTransition, triggeringGeofences );
        }

        if (!shouldContinue) {
            stopSelf();
            return;
        }
    }

    // Create a detail message with Geofences received
    private void getGeofenceTrasitionDetails(int geoFenceTransition, List<Geofence> triggeringGeofences) {
        // get the ID of each geofence triggered
        ArrayList<String> triggeringGeofencesList = new ArrayList<>();
        for ( Geofence geofence : triggeringGeofences ) {
            triggeringGeofencesList.add( geofence.getRequestId() );
        }

        if ( geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ) {
            Log.d("statusgeofence", "enter");
            Log.d("detailId", detailId);
            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
            Date systemDate = Calendar.getInstance().getTime();
            String timeArrive = format.format(systemDate);

            Log.d("timeArriveUpdateTime", timeArrive);
            getTimeArrive(timeArrive);

            Intent intent = new Intent(ACTION);
            intent.setAction(ACTION);
            intent.putExtra("attendance", "enter");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            sp.edit().putString(KEHADIRAN, HADIR).apply();
        }else if ( geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT ) {
            Log.d("statusgeofence", "exit");
            Log.d("detailId", detailId);

            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
            Date systemDate = Calendar.getInstance().getTime();
            String timeLeave = format.format(systemDate);

            mFirebaseDatabase.child("MeetingDetail").child(detailId).child("attendance").setValue("3");
            Log.d("timeLeaveUpdateTime", timeLeave);
            mFirebaseDatabase.child("MeetingDetail").child(detailId).child("timeLeave").setValue(timeLeave);

            Intent intent = new Intent(ACTION);
            intent.setAction(ACTION);
            intent.putExtra("attendance", "exit");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            sp.edit().putString(KEHADIRAN, TIDAK_HADIR).apply();
        }
    }

    public void getTimeArrive(final String timeArrive){
        mFirebaseDatabase.child("MeetingDetail").child(detailId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelDetail modelDetail = dataSnapshot.getValue(ModelDetail.class);
                if (modelDetail == null) {
                    Log.e(TAG, "ModelUser data is null!");
                    return;
                }

                if("0".equals(modelDetail.timeArrive) && "0".equals(modelDetail.attendance)){
                    mFirebaseDatabase.child("MeetingDetail").child(detailId).child("attendance").setValue("1");
                    mFirebaseDatabase.child("MeetingDetail").child(detailId).child("timeArrive").setValue(timeArrive);
                }else{
                    Log.d(TAG, "Attendance sudah dicatat");
                    if("3".equals(modelDetail.attendance)) {
                        Log.d(TAG, "re-enter");
                        mFirebaseDatabase.child("MeetingDetail").child(detailId).child("attendance").setValue("1");
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }
}
