package irsyadhhs.cs.upi.edu.meetin;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.EXTRA_DETAIL_MODE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MEETING_FINISHED;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MEETING_FINISHED_EARLY;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SYNC_FINISHED_EARLY;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

/**
 * Created by HARVI on 3/18/2018.
 */

public class FragmentMeetingDetail extends Fragment implements View.OnClickListener{

    TextView titleTV, hostIdTV, aboutTV, anggotaTV, locationTV, dateTV, timeStartTV, durationTV, timerTV, addTimeEndTV;
    TextView btn5, btn20, btn30, btn100, btnDelete;
    String myNameS, codeS, myIdS, meetingIdS, titleS, hostIdS, aboutS, anggotaS, locationS, dateS, adminS, newTimeEndS;
    String timeStartS, timeEndS, durationS, statusS, descriptionS, hostNameS, latitudeS, longitudeS, detailIdS;
    String mode = "";
    long millse, millsEnd;

    Button btnEnd;
    CardView cardLocation, cardAnggota, cardAbout, cardHost;
    LinearLayout addTimeEndLL;
    RelativeLayout groupControl;
    ProgressBar progressBar;
    ArrayList<HashMap<String, String>> mAnggota = new ArrayList<HashMap<String, String>>();

    private DatabaseReference mFirebaseDatabase;
    private ValueEventListener mConnectionListener;
    DatabaseReference connectedRef;
    private MenuItem handShakeItem;
    private MenuItem addItem;
    private MenuItem editItem;

    Bundle extras;
    Context context;
    OnDataPass dataPasser;

    private static final String TAG = "FragmentDetailMeeting";
    private static final int REQ_EDIT = 1234;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");

        extras = getActivity().getIntent().getExtras();
        meetingIdS = extras.getString(AppConfig.EXTRA_MEETING_ID);
        hostIdS = extras.getString(AppConfig.EXTRA_MEETING_HOSTID);
        statusS = extras.getString(AppConfig.EXTRA_MEETING_STATUS);
        dateS = extras.getString(AppConfig.EXTRA_MEETING_DATE);
        adminS = extras.getString(AppConfig.EXTRA_ADMIN);
        detailIdS = extras.getString(AppConfig.EXTRA_DETAIL_ID);
        timeEndS = extras.getString(AppConfig.EXTRA_MEETING_TIMEEND);
        mode = extras.getString(AppConfig.EXTRA_DETAIL_MODE);

        SharedPreferences sp = context.getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_meeting_detail,container,false);
        bindingView(view);
        setHasOptionsMenu(true);

        if((hostIdS.equals(myIdS)) || ("1".equals(adminS))) {
            groupControl.setVisibility(View.VISIBLE);
        }else{
            btnDelete.setText("Keluar dari pertemuan");
        }

        setButtonClick();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        connectedRef.removeEventListener(mConnectionListener);
    }

    private void setButtonClick() {
        cardAnggota.setOnClickListener(this);
        cardLocation.setOnClickListener(this);
        cardAbout.setOnClickListener(this);
        cardHost.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn20.setOnClickListener(this);
        btn30.setOnClickListener(this);
        btn100.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardAbout:
                aboutDetail();
                break;
            case R.id.cardHost:
                hostDetail();
                break;
            case R.id.cardAnggota:
                memberDetail();
                break;
            case R.id.cardLocation:
                mapDetail();
                break;
            case R.id.deleteMeeting:
                if((hostIdS.equals(myIdS)) || ("1".equals(adminS))) {
                    deleteMeeting();
                }else{
                    leaveMeeting();
                }
                break;
            case R.id.btn5:
                btnAdd5();
                break;
            case R.id.btn20:
                btnAdd20();
                break;
            case R.id.btn30:
                btnAdd30();
                break;
            case R.id.btn100:
                btnAdd100();
                break;
        }
    }

    public void bindingView(View view){
        btnEnd = view.findViewById(R.id.btnEndMyMeeting);
        btnDelete = view.findViewById(R.id.deleteMeeting);
        btn5 = view.findViewById(R.id.btn5);
        btn20 = view.findViewById(R.id.btn20);
        btn30 = view.findViewById(R.id.btn30);
        btn100 = view.findViewById(R.id.btn100);
        cardAbout = view.findViewById(R.id.cardAbout);
        cardLocation = view.findViewById(R.id.cardLocation);
        cardHost = view.findViewById(R.id.cardHost);
        cardAnggota = view.findViewById(R.id.cardAnggota);
        titleTV = (TextView) view.findViewById(R.id.title);
        hostIdTV = (TextView) view.findViewById(R.id.hostId);
        aboutTV = (TextView) view.findViewById(R.id.about);
        anggotaTV = (TextView) view.findViewById(R.id.anggota);
        locationTV = (TextView) view.findViewById(R.id.location);
        dateTV = (TextView) view.findViewById(R.id.date);
        timeStartTV = (TextView) view.findViewById(R.id.time);
        durationTV = (TextView) view.findViewById(R.id.duration);
        timerTV = (TextView) view.findViewById(R.id.timer);
        groupControl = view.findViewById(R.id.groupControl);
        progressBar = view.findViewById(R.id.progressBar);
        addTimeEndLL = view.findViewById(R.id.layoutAddTimeEnd);
        addTimeEndTV = view.findViewById(R.id.addTimeEnd);
    }

    public void setTextView() {
        titleTV.setText(titleS);
        hostIdTV.setText(hostNameS);
        aboutTV.setText(aboutS);
        locationTV.setText(locationS);
        dateTV.setText(dateS);
        timeStartTV.setText(timeStartS);
        progressBar.setVisibility(View.GONE);
    }

    public void setDuration(){
        try {

            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date date1 = format.parse(timeStartS);
            Date date2 = format.parse(timeEndS);

            long millse = date1.getTime() - date2.getTime();
            long mills = Math.abs(millse);

            int hours = (int) (mills / (1000 * 60 * 60));
            int mins = (int) (mills / (1000 * 60)) % 60;

            durationS = hours + " jam " + mins + " menit"; // updated value every1 second
            durationTV.setText(durationS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.meeting_detail_menu, menu);
        handShakeItem = menu.findItem(R.id.menu_handshake);
        if((hostIdS.equals(myIdS)) || ("1".equals(adminS))) {
            addItem = menu.findItem(R.id.menu_add);
            editItem = menu.findItem(R.id.menu_edit);
        }
        checkConnection();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_handshake:
                Intent intent = new Intent(context, Handshake.class);
                if((meetingIdS != null)&&(detailIdS != null)){
                    intent.putExtra(AppConfig.EXTRA_MEETING_ID, meetingIdS);
                    intent.putExtra(AppConfig.EXTRA_DETAIL_ID, detailIdS);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TIMEEND, timeEndS);
                }
                startActivity(intent);
                return true;
            case R.id.menu_add:
                addAnggota();
                return true;

            case R.id.menu_edit:
                editDetail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void checkConnection(){
        mConnectionListener = connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (!connected) {
                    Toast.makeText(context, "Tidak Ada Koneksi", Toast.LENGTH_SHORT).show();
                    hostNameS = extras.getString(AppConfig.EXTRA_MEETING_HOSTNAME);
                    titleS = extras.getString(AppConfig.EXTRA_MEETING_TITLE);
                    aboutS = extras.getString(AppConfig.EXTRA_MEETING_ABOUT);
                    descriptionS = extras.getString(AppConfig.EXTRA_MEETING_DESCRIPTION);
                    locationS = extras.getString(AppConfig.EXTRA_MEETING_LOCATION);
                    latitudeS = extras.getString(AppConfig.EXTRA_MEETING_LATITUDE);
                    longitudeS = extras.getString(AppConfig.EXTRA_MEETING_LONGITUDE);
                    timeStartS = extras.getString(AppConfig.EXTRA_MEETING_TIMESTART);
                    dateS = extras.getString(AppConfig.EXTRA_MEETING_DATE);
                    timeEndS = extras.getString(AppConfig.EXTRA_MEETING_TIMEEND);
                    codeS = extras.getString(AppConfig.EXTRA_MEETING_CODE);
                    int dateDiff = checkDate();
                    long millse = checkTime(timeStartS);
                    long millsEnd = checkTime(timeEndS);

                    if ((dateDiff == 0)&&(!MEETING_FINISHED.equals(statusS))&&(millse < 0)&&(millsEnd > 0)) {
                        handShakeItem.setVisible(true);
                        timerTV.setText("pertemuan sedang berlangsung");
                    }else if(MEETING_FINISHED.equals(statusS)){
                        btnEnd.setClickable(false);
                        btnEnd.setBackgroundResource(R.drawable.bgbuttondisable);
                        timerTV.setText("Pertemuan telah selesai");
                    }
                    sendBundle();
                    setDuration();
                    setTextView();
                    progressBar.setVisibility(View.INVISIBLE);
                }else{
                    getMeeting(meetingIdS);
                    getMember();
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

    private void sendBundle() {
        Bundle bundleNotulensi = new Bundle();
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_ID, meetingIdS);
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_TITLE, titleS);
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_TIMEEND, timeEndS);
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_TIMESTART, timeStartS);
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_DATE, dateS);
        bundleNotulensi.putString(AppConfig.EXTRA_DETAIL_ID, detailIdS);
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_HOSTID, hostIdS);
        Log.d(TAG, "DETAIL ID " +detailIdS);
        dataPasser.onDataPass(bundleNotulensi);
    }

    private void getMember() {
        anggotaS = "0";
        mFirebaseDatabase.child("MeetingDetail").orderByChild("meetingId").equalTo(meetingIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            int i = 0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    String detailId = childSnapshot.getKey();
                    ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                    if (modelDetail == null) {
                        return;
                    }
                    if("1".equals(modelDetail.approve)){
                        i++;
                    }
                }
                anggotaTV.setText(String.valueOf(i));
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void getMeeting(final String meetingId) {
        progressBar.setVisibility(View.VISIBLE);
        mFirebaseDatabase.child("MeetingList").child(meetingId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelMeeting modelMeeting = dataSnapshot.getValue(ModelMeeting.class);
                if (modelMeeting == null) {
                    return;
                }
                titleS = modelMeeting.meetingTitle;
                hostIdS = modelMeeting.hostId;
                hostNameS = modelMeeting.hostName;
                aboutS = modelMeeting.about;
                descriptionS = modelMeeting.description;
                locationS = modelMeeting.location;
                dateS = modelMeeting.date;
                timeStartS = modelMeeting.timeStart;
                timeEndS = modelMeeting.timeEnd;
                statusS = modelMeeting.status;
                latitudeS = modelMeeting.latitude;
                longitudeS = modelMeeting.longitude;
                codeS = modelMeeting.code;

                sendBundle();

                int dateDiff = checkDate();
                millse = checkTime(timeStartS);
                millsEnd = checkTime(timeEndS);

                if ((dateDiff != -99)&&(!MEETING_FINISHED.equals(statusS))) {
                    if (dateDiff >= 1) {
                        if((hostIdS.equals(myIdS))||("1".equals(adminS))) {
                            addItem.setVisible(true);
                            editItem.setVisible(true);
                        }
                        timerTV.setText("dimulai dalam " + dateDiff + " hari");
                    } else if(dateDiff < 0) {
                        meetHasEnded();
                        timerTV.setText("Pertemuan telah selesai");
                    }else if(dateDiff == 0){
                        if(millse > 0){
                            if((hostIdS.equals(myIdS))||("1".equals(adminS))) {
                                addItem.setVisible(true);
                                editItem.setVisible(true);
                            }
                            int hours = (int) (millse / (1000 * 60 * 60));
                            int mins = (int) (millse / (1000 * 60)) % 60;
                            String difference = "dimulai dalam " + hours + " jam " + mins + " menit"; // updated value every1 second
                            timerTV.setText(difference);
                        }else if(millsEnd<0){
                            meetHasEnded();
                            timerTV.setText("Pertemuan telah selesai");
                        }else{
                            if((hostIdS.equals(myIdS))||("1".equals(adminS))) {
                                btnEnd.setClickable(true);
                                btnEnd.setBackgroundResource(R.drawable.bgbutton);
                                btnEnd.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        meetingEnd();
                                    }
                                });
                                addTimeEndTV.setVisibility(View.VISIBLE);
                                addTimeEndLL.setVisibility(View.VISIBLE);
                            }
                            if((hostIdS.equals(myIdS))||("1".equals(adminS))) {
                                addItem.setVisible(true);
                            }
                            handShakeItem.setVisible(true);
                            timerTV.setText("pertemuan sedang berlangsung");
                        }
                    }else{
                        if(millsEnd == -1){
                            meetHasEnded();
                            timerTV.setText("Pertemuan telah selesai");
                        }
                    }
                }else{
                    meetHasEnded();
                    timerTV.setText("Pertemuan telah selesai");
                }
                setDuration();
                setTextView();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void meetHasEnded(){
        if((hostIdS.equals(myIdS))||("1".equals(adminS))) {
            addItem.setVisible(false);
            editItem.setVisible(false);
            btnEnd.setClickable(false);
            btnEnd.setBackgroundResource(R.drawable.bgbuttondisable);
        }
    }

    public long getDateParse() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
        Date systemDate = Calendar.getInstance().getTime();
        final String timeEndNow = format.format(systemDate);

        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);

        String dateMeetingStop = dateS + " " + timeEndNow;
        try {
            return mdformat.parse(dateMeetingStop).getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public int checkDate() {
        int diff;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String strDate = mdformat.format(calendar.getTime());

        try {
            Date date1 = mdformat.parse(dateS);
            Date date2 = mdformat.parse(strDate);

            diff = ((int) ((date1.getTime() / (24 * 60 * 60 * 1000)) - (int) (date2.getTime() / (24 * 60 * 60 * 1000))));
            if(diff < 0){
                diff = -1;
            }
            return diff;
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public long checkTime(String time) {
        try {

            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);

            Date systemDate = Calendar.getInstance().getTime();
            String myDate = format.format(systemDate);
            Date date1 = format.parse(time);
            Date date2 = format.parse(myDate);

            long millse = date1.getTime() - date2.getTime();
            if(millse > 0) {
                return millse;
            }else{
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return  -99;
        }
    }

    public void hostDetail() {
        Intent intent = new Intent(context, Profile.class);
        intent.putExtra(AppConfig.PROFILE_USER_ID, hostIdS);
        startActivity(intent);
    }

    public void memberDetail() {
        Intent intent = new Intent(context, Anggota.class);
        intent.putExtra(AppConfig.EXTRA_MEETING_ID, meetingIdS);
        intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, titleS);
        intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, hostIdS);
        //intent.putExtra(AppConfig.EXTRA_ARRAYLIST_ANGGOTA, mAnggota);
        startActivity(intent);
    }

    public void aboutDetail() {
        Intent intent = new Intent(context, Description.class);
        intent.putExtra(AppConfig.EXTRA_MEETING_ABOUT, aboutS);
        intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, titleS);
        intent.putExtra(AppConfig.EXTRA_MEETING_DESCRIPTION, descriptionS);
        startActivity(intent);
    }

    public void mapDetail() {
        Intent intent = new Intent(context, MapActivity.class);
        intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, titleS);
        intent.putExtra(AppConfig.EXTRA_MEETING_LATITUDE, latitudeS);
        intent.putExtra(AppConfig.EXTRA_MEETING_LONGITUDE, longitudeS);
        startActivity(intent);
    }

    public void addAnggota(){
        Intent intent = new Intent(context, AddAnggota.class);
        intent.putExtra(AppConfig.EXTRA_MEETING_ID, meetingIdS);
        intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, titleS);
        intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, hostIdS);
        intent.putExtra(AppConfig.EXTRA_MEETING_CODE, codeS);
        intent.putExtra(AppConfig.EXTRA_MEMBER_USER1, myNameS);
        startActivity(intent);
    }

    public void editDetail(){
        Intent intent = new Intent(context, CreateMeeting.class);
        intent.putExtra(AppConfig.MODE, "edit");
        intent.putExtra(AppConfig.EXTRA_MEETING_ID, meetingIdS);
        intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, hostIdS);
        intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, titleS);
        intent.putExtra(AppConfig.EXTRA_MEETING_ABOUT, aboutS);
        intent.putExtra(AppConfig.EXTRA_MEETING_DESCRIPTION, descriptionS);
        intent.putExtra(AppConfig.EXTRA_MEETING_LOCATION, locationS);
        intent.putExtra(AppConfig.EXTRA_MEETING_DATE, dateS);
        intent.putExtra(AppConfig.EXTRA_MEETING_TIMESTART, timeStartS);
        intent.putExtra(AppConfig.EXTRA_MEETING_TIMEEND, timeEndS);
        intent.putExtra(AppConfig.EXTRA_MEETING_LATITUDE, latitudeS);
        intent.putExtra(AppConfig.EXTRA_MEETING_LONGITUDE, longitudeS);
        startActivityForResult(intent, REQ_EDIT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_EDIT) {
            if (resultCode == RESULT_OK) {
                checkConnection();
            }
        }
    }

    public void deleteMeeting() {
        final AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
        ad.setMessage("Anda yakin akan menghapus jadwal "+titleS+" ?")
                .setTitle("Konfirmasi Hapus Jadwal")
                .setCancelable(false)
                .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        forceStop();
                        mFirebaseDatabase.child("MeetingList").child(meetingIdS).removeValue();
                        if("home".equals(mode)){
                            Intent intent = new Intent("delete-meeting-home");
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        }else{
                            Intent intent = new Intent("delete-meeting");
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        }

                        getActivity().finish();
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ad.create().dismiss();
                    }
                });

        AlertDialog alert = ad.create();
        alert.show();
    }

    public void leaveMeeting() {
        final AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
        ad.setMessage("Anda yakin akan keluar dari "+titleS+" ?")
                .setTitle("Konfirmasi Keluar")
                .setCancelable(false)
                .setPositiveButton("Keluar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        forceStop();
                        mFirebaseDatabase.child("MeetingDetail").child(detailIdS).removeValue();
                        if("home".equals(mode)){
                            Intent intent = new Intent("delete-meeting-home");
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        }
                        getActivity().finish();
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ad.create().dismiss();
                    }
                });

        AlertDialog alert = ad.create();
        alert.show();
    }

    public void meetingEnd(){
        final AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
        ad.setMessage("Akhiri sesi "+titleS+" ?")
                .setTitle("Konfirmasi Selesai")
                .setCancelable(false)
                .setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        forceStop();

                        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
                        Date systemDate = Calendar.getInstance().getTime();
                        newTimeEndS = format.format(systemDate);

                        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("timeEnd").setValue(newTimeEndS);
                        /*mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("status").setValue(MEETING_FINISHED_EARLY);*/

                        sendNotif("selesai");
                        getMeeting(meetingIdS);
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ad.create().dismiss();
                    }
                });

        AlertDialog alert = ad.create();
        alert.show();
    }

    public void forceStop(){
        int dateDiff = checkDate();
        long millse = checkTime(timeStartS);
        long millsEnd = checkTime(timeEndS);
        if((dateDiff==0)&&(millse < 0)&&(millsEnd > 0)) {
            int alarmId = Integer.parseInt(codeS.substring(4, 9));
            int actionCode = alarmId * -1;
            long dateSchStop = getDateParse();
            AlarmManager alarmManager = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            Intent meetingIntent = new Intent(getActivity().getApplicationContext(), StopServiceReceiver.class);
            meetingIntent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, myIdS);
            meetingIntent.putExtra(AppConfig.EXTRA_DETAIL_ID, detailIdS);
            meetingIntent.putExtra(AppConfig.ALARM_ID, actionCode);
            meetingIntent.setAction(String.valueOf(actionCode));
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), alarmId, meetingIntent, 0);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, dateSchStop, pendingIntent);
        }
    }

    public String updateTimeEnd(String timeEnd, int addMinute){
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.US);
            Date date = formatter.parse(timeEnd);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MINUTE, addMinute);
            Date d = cal.getTime();
            return formatter.format(d);
        }catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void btnAdd5(){
        String timeEnd = updateTimeEnd(timeEndS, 5);
        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("timeEnd").setValue(timeEnd);
        sendNotif("durasi");
        getMeeting(meetingIdS);
    }

    public void btnAdd20(){
        String timeEnd = updateTimeEnd(timeEndS, 20);
        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("timeEnd").setValue(timeEnd);
        sendNotif("durasi");
        getMeeting(meetingIdS);
    }

    public void btnAdd30(){
        String timeEnd = updateTimeEnd(timeEndS, 30);
        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("timeEnd").setValue(timeEnd);
        sendNotif("durasi");
        getMeeting(meetingIdS);
    }

    public void btnAdd100(){
        String timeEnd = updateTimeEnd(timeEndS, 100);
        mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("timeEnd").setValue(timeEnd);
        sendNotif("durasi");
        getMeeting(meetingIdS);
    }

    public void sendNotif(final String mode){
        mFirebaseDatabase.child("MeetingDetail").orderByChild("meetingId").equalTo(meetingIdS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                            ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                            String detailId = childSnapshot.getKey();
                            if (modelDetail == null) {
                                return;
                            }
                            if("1".equals(modelDetail.approve)){
                                mFirebaseDatabase.child("MeetingDetail").child(detailId).child("sync").setValue(SYNC_FINISHED_EARLY);

                                String body = "";
                                if("durasi".equals(mode)) {
                                    body = "Penambahan durasi di " + titleS;
                                }else{
                                    body = "Jadwal " + titleS + " telah diselesaikan";

                                    if((("1".equals(modelDetail.attendance))||("4".equals(modelDetail.attendance)))){
                                        mFirebaseDatabase.child("MeetingDetail").child(detailId).child("timeLeave").setValue(newTimeEndS);
                                    }else if("3".equals(modelDetail.attendance)){
                                        mFirebaseDatabase.child("MeetingDetail").child(detailId).child("attendance").setValue("1");
                                    }
                                }
                                if(!modelDetail.memberId.equals(myIdS)) {
                                    mFirebaseDatabase.child("messages").push().setValue(new ModelMessage(body, myIdS, modelDetail.memberId));
                                }
                            }
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.e(TAG, "Failed to read user", error.toException());
                    }
                });
    }

    public interface OnDataPass {
        void onDataPass(Bundle bundle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
    }
}
