package irsyadhhs.cs.upi.edu.meetin;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.HashMap;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class Seting extends AppCompatActivity {

    /*
    TODO stop service at logout
    TODO cancel alarm at logout

 */
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    String myIdS, myEmailS;
    HashMap<String, String> addparams = new HashMap<String, String>();
    FirebaseAuth auth;
    FirebaseUser user;
    private FirebaseAuth.AuthStateListener authListener;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seting);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Seting.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
        sp = getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "");
        myEmailS = sp.getString(AppConfig.MY_EMAIL, "");
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();
        auth = FirebaseAuth.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    onLogOut();
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }

    public void logout(View view) {
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setMessage("Anda yakin akan keluar dari aplikasi?")
                .setTitle("Konfirmasi Keluar")
                .setCancelable(false)
                .setPositiveButton("Keluar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(Seting.this, "Logged out", Toast.LENGTH_SHORT).show();
                        FirebaseDatabase.getInstance().getReference()
                                .child("Users")
                                .child(myIdS)
                                .child("instanceId")
                                .setValue("0");
                        auth.signOut();
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ad.create().dismiss();
                    }
                });
        AlertDialog alert = ad.create();
        alert.show();
    }

    public void delAcc(View view) {
        final EditText edittext = new EditText(getApplicationContext());
        edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edittext.setTextColor(Color.parseColor("#000000"));
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setMessage("Anda yakin akan menghapus akun? Tindakan ini akan menghapus " +
                "seluruh rekam data Anda, termasuk daftar jadwal dan relasi.\n"+
                "Masukkan kata sandi Anda untuk melanjutkan.")
                .setTitle("Konfirmasi Hapus Akun")
                .setCancelable(false)
                .setView(edittext)
                .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //updateSync(AppConfig.DELETE_USER_ACCOUNT, addparams);
                        progressBar.setVisibility(View.VISIBLE);
                        String password = edittext.getText().toString();
                        auth.signInWithEmailAndPassword(myEmailS, password)
                                .addOnCompleteListener(Seting.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(Seting.this, "Password Salah", Toast.LENGTH_LONG).show();
                                        } else {
                                            if (user != null) {
                                                mFirebaseDatabase.child("Users").child(myIdS).removeValue();
                                                user.delete()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    Toast.makeText(Seting.this, "Akun dihapus", Toast.LENGTH_SHORT).show();
                                                                    onLogOut();
                                                                } else {
                                                                    Toast.makeText(Seting.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                                                                    progressBar.setVisibility(View.GONE);
                                                                }
                                                            }
                                                        });
                                            }
                                        }
                                    }
                                });
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ad.create().dismiss();
                    }
                });

        AlertDialog alert = ad.create();
        alert.show();
    }

    public void onLogOut(){
        sp.edit().putString(AppConfig.APP_AUTH,"no").apply();
        detailChangeListener();

        Intent i = new Intent(getApplicationContext(), AttendanceService.class);
        getApplicationContext().stopService(i);

        Intent i2 = new Intent(getApplicationContext(), GeofenceIntentService.class);
        getApplicationContext().stopService(i2);

        Intent intent = new Intent(Seting.this, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        progressBar.setVisibility(View.GONE);
    }

    private void detailChangeListener() {
        mFirebaseDatabase.child("MeetingDetail").orderByChild("memberId").equalTo(myIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                    if (modelDetail == null) {
                        Log.e(TAG, "ModelUser data is null!");
                        return;
                    }
                    if("1".equals(modelDetail.approve)){
                        meetingChangeListener(modelDetail);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void meetingChangeListener(final ModelDetail model) {
        mFirebaseDatabase.child("MeetingList").child(model.meetingId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0){
                    progressBar.setVisibility(View.INVISIBLE);
                }else {
                    ModelMeeting modelMeeting = dataSnapshot.getValue(ModelMeeting.class);
                    if (modelMeeting == null) {
                        Log.e(TAG, "ModelUser data is null!");
                        return;
                    }
                    if("0".equals(modelMeeting.status)) {
                        int alarmId = Integer.parseInt(modelMeeting.code.substring(4,9));
                        cancelAlarm(alarmId);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void cancelAlarm(int pendingIntentCode){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent meetingIntent = new Intent(Seting.this, AttendanceReceiver.class);
        meetingIntent.setAction(AppConfig.ACTION_START_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(Seting.this, pendingIntentCode, meetingIntent, 0);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();

        Log.d(TAG,"cancel alarm " + pendingIntentCode);
    }
}
