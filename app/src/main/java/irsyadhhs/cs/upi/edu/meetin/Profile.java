package irsyadhhs.cs.upi.edu.meetin;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class Profile extends AppCompatActivity {
    String pekerjaanS, pendidikanS, perusahaanS, bagianS, nameS, emailS, userIdS, pictureURL, myIdS, myNameS;
    private int EDIT_PROFILE_REQUEST = 1;

    private DatabaseReference mFirebaseDatabase;

    MenuItem addItem, addedItem;

    ProgressBar progressBar, progressBarPic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        SharedPreferences sp = getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        Bundle extras = getIntent().getExtras();

        userIdS = extras.getString(AppConfig.PROFILE_USER_ID);

        progressBar = findViewById(R.id.progressBar);
        progressBarPic = findViewById(R.id.progressBarPic);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        addUserChangeListener(userIdS);
        //myProfileList();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        MenuItem editItem = menu.findItem(R.id.menu_edit);
        final MenuItem addedItem = menu.findItem(R.id.menu_added);
        final MenuItem addItem = menu.findItem(R.id.menu_add);
        this.addItem = addItem;
        this.addedItem = addedItem;
        if(myIdS.equals(userIdS)){
            editItem.setVisible(true);
        }else{
            mFirebaseDatabase.child("Relation").child(myIdS).child(userIdS)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String approve="";
                            final ModelRelation modelRelation = dataSnapshot.getValue(ModelRelation.class);
                            if (modelRelation == null) {
                                approve = "-1";
                            } else {
                                if (modelRelation.relationUserId.equals(userIdS)) {
                                    approve = modelRelation.approve;
                                }
                            }
                            Log.d(TAG, "approve = " + approve);
                            if ((approve.equals("-1")) || (approve.equals("0"))) {
                                addItem.setVisible(true);
                            }else{
                                addedItem.setVisible(true);
                            }

                        }
                        @Override
                        public void onCancelled(DatabaseError error) {
                            Log.e(TAG, "Failed to read user", error.toException());
                        }
                    });

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                editProfile();
                return true;
            case R.id.menu_add:
                addRelation();
                return true;
            case R.id.menu_added:
                delRelation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void delRelation() {
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setMessage("hapus "+nameS+" dari daftar relasi?")
                .setTitle("Hapus Relasi?")
                .setCancelable(false)
                .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(Profile.this, "relasi dihapus", Toast.LENGTH_SHORT).show();
                        mFirebaseDatabase.child("Relation").child(myIdS).child(userIdS).removeValue();
                        /*finish();
                        overridePendingTransition(0, 0);
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);*/
                        addedItem.setVisible(false);

                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ad.create().dismiss();
                    }
                });
        AlertDialog alert = ad.create();
        alert.show();
    }

    private void addRelation() {
        mFirebaseDatabase.child("Relation").child(userIdS).child(myIdS)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getChildrenCount() == 0){
                            ModelRelation modelRelation2 = new ModelRelation(myIdS, myNameS, "0");
                            modelRelation2.setSync(0);
                            mFirebaseDatabase.child("Relation").child(userIdS).child(myIdS).setValue(modelRelation2);
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.e(TAG, "Failed to read user", error.toException());
                    }
                });
        mFirebaseDatabase.child("Relation").child(myIdS).child(userIdS)
                .setValue(new ModelRelation(userIdS, nameS, "1"));
        addItem.setVisible(false);
    }

    /* ModelUser data change listener */
    private void addUserChangeListener(String UID) {
        // ModelUser data change listener
        progressBar.setVisibility(View.VISIBLE);
        mFirebaseDatabase.child("Users").child(UID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                // Check for null
                if (modelUser == null) {
                    Log.e(TAG, "ModelUser data is null!");
                    return;
                }
                nameS = modelUser.name;
                emailS = modelUser.email;
                perusahaanS = modelUser.getCompany();
                bagianS = modelUser.getDivisi();
                pendidikanS = modelUser.getEducation();
                pekerjaanS = modelUser.getJob();
                pictureURL = modelUser.getPicture();
                setProfileText();
                Log.e(TAG, "ModelUser data is changed!" + modelUser.name + ", " + modelUser.email);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void setProfileText(){
        progressBarPic.setVisibility(View.VISIBLE);
        TextView profileName, profileEmail, pekerjaan, pendidikan, perusahaan, bagian;
        ImageView picture;
        profileName = (TextView) findViewById(R.id.profileName);
        profileEmail  = (TextView) findViewById(R.id.profileEmail);
        pekerjaan = (TextView) findViewById(R.id.pekerjaan);
        pendidikan = (TextView) findViewById(R.id.pendidikan);
        perusahaan = (TextView) findViewById(R.id.perusahaan);
        bagian = (TextView) findViewById(R.id.bagian);
        picture = findViewById(R.id.PictureProfile);
        if("null".equals(perusahaanS)){
            perusahaanS="-";
        }
        if("null".equals(bagianS)){
            bagianS="-";
        }
        if("null".equals(pekerjaanS)){
            pekerjaanS="-";
        }
        if("null".equals(pekerjaanS)){
            pendidikanS="-";
        }
        profileName.setText(nameS);
        profileEmail.setText(emailS);
        perusahaan.setText(perusahaanS);
        bagian.setText(bagianS);
        pekerjaan.setText(pekerjaanS);
        pendidikan.setText(pendidikanS);
        Picasso.with(Profile.this)
                .load(pictureURL)
                .resize(96,96)
                .centerCrop()
                .error(R.drawable.ic_account_circle_black_48dp)
                .placeholder(R.drawable.ic_account_circle_black_48dp)
                .transform(new CircleTransform())
                .into(picture, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBarPic.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        Log.v("Picasso", "Could not fetch image");
                        progressBarPic.setVisibility(View.GONE);
                    }
                });
        progressBar.setVisibility(View.GONE);
    }

    public void editProfile(){
        Intent intent = new Intent(Profile.this, EditProfile.class);
        intent.putExtra(AppConfig.PROFILE_USER_ID, userIdS);
        intent.putExtra(AppConfig.PROFILE_EMAIL, emailS);
        intent.putExtra( AppConfig.PROFILE_NAME, nameS);
        intent.putExtra(AppConfig.PROFILE_PERUSAHAAN, perusahaanS);
        intent.putExtra(AppConfig.PROFILE_BAGIAN, bagianS);
        intent.putExtra(AppConfig.PROFILE_PEKERJAAN, pekerjaanS);
        intent.putExtra(AppConfig.PROFILE_PENDIDIKAN, pendidikanS);
        startActivityForResult(intent, EDIT_PROFILE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_PROFILE_REQUEST) {
            if (resultCode == RESULT_OK) {
                addUserChangeListener(userIdS);
            }
        }
    }
}
