package irsyadhhs.cs.upi.edu.meetin;

import android.content.Intent;
import android.content.SharedPreferences;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class Login extends AppCompatActivity {
    Button loginBtn;
    EditText emailET, passwordET;
    String emailS, passwordS, phoneS, nameS, userIdS;

    SharedPreferences sp;
    SharedPreferences.Editor ed;
    private FirebaseAuth auth;
    private ProgressBar progressBar;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);*/

        sp = getSharedPreferences(SP, MODE_PRIVATE);

        auth = FirebaseAuth.getInstance();
        mFirebaseInstance = FirebaseDatabase.getInstance();

        mFirebaseDatabase = mFirebaseInstance.getReference("Users");
        if (auth.getCurrentUser() != null) {
            onSignupSuccess();
        }

        emailET = (EditText) findViewById(R.id.email_et);
        passwordET = (EditText) findViewById(R.id.password_et);
        loginBtn = (Button) findViewById(R.id.btnLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    public void toRegister(View view) {
        Intent intent = new Intent(Login.this, Register.class);
        startActivity(intent);
        finish();
    }

    public boolean validate() {
        boolean valid = true;

        emailS = emailET.getText().toString().toLowerCase();
        passwordS = passwordET.getText().toString();

        if (emailS.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailS).matches()) {
            emailET.setError("enter a valid email address");
            valid = false;
        } else {
            emailET.setError(null);
        }

        if (passwordS.isEmpty() || passwordS.length() < 4 || passwordS.length() > 16) {
            passwordET.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            passwordET.setError(null);
        }

        return valid;
    }

    public void loginDone(View view) {
        if (validate()) {
            getCurrentUser(emailS, passwordS);
        }else{
            onSignupFailed();
        }
    }

    public void checkConnection(){
        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            boolean connected;
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (!connected) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(Login.this, "Tidak Ada Koneksi", Toast.LENGTH_SHORT).show();
                }else{
                    getCurrentUser(emailS, passwordS);
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                //System.err.println("Listener was cancelled");
            }
        });
    }

    public void getCurrentUser(final String email, String password){
        progressBar.setVisibility(View.VISIBLE);
        //authenticate user
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        progressBar.setVisibility(View.GONE);
                        if (!task.isSuccessful()) {
                            // there was an error
                                Toast.makeText(Login.this, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
                        } else {
                            FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
                            String UID = currentFirebaseUser.getUid();
                            ed = sp.edit();
                            ed.putString(AppConfig.APP_AUTH, "yes").apply();
                            ed.putString(AppConfig.MY_ID, UID).apply();
                            ed.putString(AppConfig.MY_EMAIL, email).apply();
                            sp.edit().putString(AppConfig.INFO_TAG_PREF, "yes").apply();
                            addUserChangeListener(UID);
                            onSignupSuccess();
                        }
                    }
                });
    }

    private void addUserChangeListener(final String UID) {
        // ModelUser data change listener
        mFirebaseDatabase.child(UID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                // Check for null
                if (modelUser == null) {
                    return;
                }
                ed = sp.edit();
                ed.putString(AppConfig.MY_NAME, modelUser.name).apply();
                setToken(UID);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void setToken(String UID) {
        String instanceId = FirebaseInstanceId.getInstance().getToken();
        if (instanceId != null) {
            FirebaseDatabase.getInstance().getReference()
                    .child("Users")
                    .child(UID)
                    .child("instanceId")
                    .setValue(instanceId);
        }
    }

    public void onSignupSuccess() {
        Intent intent = new Intent(Login.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(Login.this, "Login failed", Toast.LENGTH_LONG).show();
        loginBtn.setEnabled(true);
    }
}
