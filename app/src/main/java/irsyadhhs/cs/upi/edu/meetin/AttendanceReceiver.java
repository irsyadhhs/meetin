package irsyadhhs.cs.upi.edu.meetin;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.ACTION_START_SERVICE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.KEHADIRAN;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TIDAK_ADA_JADWAL;
import static java.lang.Math.cos;

/**
 * Created by HARVI on 2/18/2018.
 */

public class AttendanceReceiver extends BroadcastReceiver{
    private DatabaseReference mFirebaseDatabase;
    int notificationId;
    Context mContext;
    NotificationManager mNotificationManager;
    SharedPreferences sp;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final Bundle extras = intent.getExtras();

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        notificationId = extras.getInt(AppConfig.EXTRA_GEO_ID);
        mContext = context;

        sp = context.getSharedPreferences(SP, Context.MODE_PRIVATE);

        Log.d("alarmstart", "still running");

        mFirebaseDatabase.child("MeetingDetail").child(extras.getString(AppConfig.EXTRA_DETAIL_ID)).child("sync").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    return;
                }
                String sync = dataSnapshot.getValue().toString();
                if("3".equals(sync)){
                    mFirebaseDatabase.child("MeetingDetail").child(extras.getString(AppConfig.EXTRA_DETAIL_ID)).child("sync").setValue(4);
                    Intent notifIntent = new Intent(context, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, notificationId, notifIntent, 0);

                    String channelId = "";
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                        channelId = createNotificationChannel();
                    }

                    sp.edit().putString(KEHADIRAN, TIDAK_ADA_JADWAL).apply();

                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, String.valueOf(notificationId))
                            .setSmallIcon(R.drawable.ic_alarm_black_24dp)
                            .setContentTitle("Pertemuan Berlangsung")
                            .setChannelId(channelId)
                            .setContentText("ketuk disini jika kehadiran tidak tercatat")
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            // Set the intent that will fire when the user taps the notification
                            .setContentIntent(pendingIntent)
                            .setAutoCancel(true);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

                    notificationManager.notify(notificationId, mBuilder.build());

                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });

        checkLocation(context);

        Intent i = new Intent(context, ServiceAttendance.class);
        i.putExtra(AppConfig.EXTRA_MEETING_LATITUDE, extras.getDouble(AppConfig.EXTRA_MEETING_LATITUDE));
        i.putExtra(AppConfig.EXTRA_MEETING_LONGITUDE, extras.getDouble(AppConfig.EXTRA_MEETING_LONGITUDE));
        i.putExtra(AppConfig.EXTRA_MEETING_ID, extras.getString(AppConfig.EXTRA_MEETING_ID));
        i.putExtra(AppConfig.EXTRA_GEO_ID, extras.getInt(AppConfig.EXTRA_GEO_ID));
        i.putExtra(AppConfig.EXTRA_MEETING_HOSTID, extras.getString(AppConfig.EXTRA_MEETING_HOSTID));
        i.putExtra(AppConfig.EXTRA_DETAIL_ID, extras.getString(AppConfig.EXTRA_DETAIL_ID));
        i.putExtra(AppConfig.EXTRA_GEO_DURATION, extras.getLong(AppConfig.EXTRA_GEO_DURATION));
        i.setAction(ACTION_START_SERVICE);
        context.startService(i);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(){
        String channelId = "start_service";
        String channelName = "Schedule Start Info";
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_LOW);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.createNotificationChannel(chan);
        return channelId;
    }

    private void checkLocation(Context context) {
        int off = 0;
        try {
            off = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if(off==0){
            Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            onGPS.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 1996, onGPS, 0);

            String channelId = "";
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                channelId = createNotificationChannel();
            }else{
                channelId = "1996";
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.ic_warning_black_24dp)
                    .setContentTitle("Pencatatan kehadiran membutuhkan layanan lokasi aktif")
                    .setContentText("Ketuk disini untuk mengaktifkan")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setChannelId(channelId)
                    .setAutoCancel(true);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

            notificationManager.notify(1996, mBuilder.build());
        }
    }

}
