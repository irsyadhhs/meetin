package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/24/2018.
 */

public class ModelAddAnggota {
    public String meetingId;
    public String hostId;
    public String memberId;
    public String memberName;
    public String approve;
    public String attendance;
    public String timeArrive;
    public String timeLeave;
    public String detailId;
    public String picture;

    public ModelAddAnggota(){

    }

    public ModelAddAnggota(String detailId, String meetingId, String hostId, String memberId, String memberName, String approve,
                       String attendance, String timeArrive, String timeLeave){
        this.detailId = detailId;
        this.meetingId = meetingId;
        this.hostId = hostId;
        this.memberId = memberId;
        this.memberName = memberName;
        this.approve = approve;
        this.attendance = attendance;
        this.timeArrive = timeArrive;
        this.timeLeave = timeLeave;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
