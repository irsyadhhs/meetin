package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 11/18/2017.
 */

public class AppConfig {

    public static final String MY_ID = "my_id";
    public static final String MY_NAME = "my_name";
    public static final String MY_EMAIL = "my_email";
    public static final String INFO_TAG_PREF = "info_pref";
    public static final String ALARM_ID = "alarm_id";
    public static final String APP_AUTH = "app_auth";
    public static final String TAG = "meetin";
    public static final String SP = "meetin.irsyadhhs.cs.upi.edu.meetin";
    public static final String ACTION_START_SERVICE = "irsyadhhs.cs.upi.edu.meetin.startService";
    public static final String API_KEY = "AIzaSyATgZVwNL9GSsaqzhO0-UKsj8shVLF18Y0";
    public static final String KEHADIRAN = "kehadiran";
    public static final String HADIR = "yes";
    public static final String TIDAK_HADIR = "no";
    public static final String TIDAK_ADA_JADWAL = "idle";

    public static final String EXTRA_DETAIL_ID = "extra_detail_id";
    public static final String EXTRA_ADMIN = "extra_admin";
    public static final String EXTRA_NOTIF_TEXT = "extra_notif_text";
    public static final String EXTRA_MEETING_ID = "extra_meeting_id";
    public static final String EXTRA_MEETING_TITLE = "extra_meeting_title";
    public static final String EXTRA_MEETING_HOSTID = "extra_meeting_hostid";
    public static final String EXTRA_MEETING_HOSTNAME = "extra_meeting_hostname";
    public static final String EXTRA_MEETING_ABOUT = "extra_meeting_about";
    public static final String EXTRA_MEETING_DESCRIPTION = "extra_meeting_description";
    public static final String EXTRA_MEETING_LOCATION = "extra_meeting_location";
    public static final String EXTRA_MEETING_LATITUDE = "extra_meeting_latitude";
    public static final String EXTRA_MEETING_LONGITUDE = "extra_meeting_longitude";
    public static final String EXTRA_MEETING_DATE = "extra_meeting_date";
    public static final String EXTRA_MEETING_TIMESTART = "extra_meeting_time1";
    public static final String EXTRA_MEETING_TIMEEND = "extra_meeting_time2";
    public static final String EXTRA_MEETING_STATUS = "extra_meeting_status";
    public static final String EXTRA_MEETING_CODE = "extra_meeting_code";
    public static final String EXTRA_GEO_DURATION = "extra_geo_duration";
    public static final String EXTRA_GEO_ID = "extra_geo_id";
    public static final String EXTRA_DETAIL_MODE = "extra_detail_mode";

    public static final String EXTRA_MEMBER_USER1 = "extra_member_user1";

    public static final String EXTRA_ARRAYLIST_ANGGOTA = "arraylist_anggota";

    public static final String MODE = "create_meeting_mode";
    public static final int ENABLE_BT_REQUEST_CODE = 1000;
    public static final int DISCOVERABLE_BT_REQUEST_CODE = 1100;

    public static final String PROFILE_USER_ID = "profile_user_id";
    public static final String PROFILE_NAME = "profile_user_name";
    public static final String PROFILE_EMAIL = "profile_user_email";
    public static final String PROFILE_PERUSAHAAN = "profile_user_perusahaan";
    public static final String PROFILE_BAGIAN = "profile_user_bagian";
    public static final String PROFILE_PEKERJAAN = "profile_user_pekerjaan";
    public static final String PROFILE_PENDIDIKAN = "profile_user_pendidikan";

    public static final String MEETING_ONGOING = "0";
    public static final String MEETING_FINISHED = "1";
    public static final String MEETING_UNDISCOVERABLE = "2";
    public static final String MEETING_FINISHED_EARLY = "3";


    public static final int SYNC_FINISHED_EARLY = 5;
}
