package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/6/2018.
 */

public class ModelAnggota {
    private String detailId;
    private String meetingId;
    private String memberId;
    private  String memberName;
    private  String attendance;
    private  String picture;

    public ModelAnggota(){

    }

    public ModelAnggota(String  detailId, String  meetingId, String  memberId, String memberName){
        this.detailId = detailId;
        this.meetingId = meetingId;
        this.memberId = memberId;
        this.memberName = memberName;
    }

    public String getDetailId(){
        return  detailId;
    }
    public void setDetailId(String detailId){
        this.detailId = detailId;
    }

    public String getMeetingId(){
        return  meetingId;
    }
    public void setMeetingId(String meetingId){
        this.meetingId = meetingId;
    }

    public String getMemberId(){
        return  memberId;
    }
    public void setMemberId(String memberId){
        this.memberId = memberId;
    }

    public String getMemberName(){
        return  memberName;
    }

    public void setMemberName(String memberName){
        this.memberName = memberName;
    }

    public String getAttendance(){
        return  attendance;
    }

    public void setAttendance(String attendance){
        this.attendance = attendance;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
