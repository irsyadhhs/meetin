package irsyadhhs.cs.upi.edu.meetin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;


public class FragmentNotif extends Fragment{

    private static RecyclerView.Adapter adapter;
    @SuppressLint("StaticFieldLeak")
    private static RecyclerView recyclerView;

    SharedPreferences sp;
    String myIdS, myNameS, meetingIdS;

    ProgressBar progressBar;

    private DatabaseReference mFirebaseDatabase;

    ArrayList<ModelNotif> isinotif = new ArrayList<ModelNotif>();

    AdapterNotif datakamus;

    Context mContext;

    public FragmentNotif() {
        // Required empty public constructor
    }

    public static FragmentNotif newInstance() {
        FragmentNotif fragment = new FragmentNotif();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        sp = this.getActivity().getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fragment_add, container, false);

        mContext = getActivity().getApplicationContext();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.listViewNotif);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        progressBar = rootView.findViewById(R.id.progressBar);

        datakamus = new AdapterNotif(getActivity(), null);

        detailChangeListener("memberId");
        detailChangeListener("hostId");
        relationChangeListener();

        return rootView;
    }

    private void meetingChangeListener(final String meetingId, final String detailId, final String memberId, final String memberName, final int type) {
        mFirebaseDatabase.child("MeetingList").child(meetingId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelMeeting modelMeeting = dataSnapshot.getValue(ModelMeeting.class);
                modelMeeting.setMeetingId(meetingId);
                if (modelMeeting == null) {
                    Log.e(TAG, "ModelUser data is null!");
                    return;
                }
                isinotif.add(new ModelNotif(detailId, meetingId, memberId,
                        memberName, modelMeeting.meetingTitle,
                        type
                ));
                if(isAdded()){
                    datakamus = new AdapterNotif(getActivity().getApplicationContext(),isinotif);
                    recyclerView.setAdapter(datakamus);
                }
                Log.e(TAG, "meeting data is changed!" + modelMeeting.meetingTitle + " " + modelMeeting.date);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void detailChangeListener(final String equal) {
        progressBar.setVisibility(View.VISIBLE);
        mFirebaseDatabase.child("MeetingDetail").orderByChild(equal).equalTo(myIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                    String detailId = childSnapshot.getKey();
                    if (modelDetail == null) {
                        Log.e(TAG, "ModelUser data is null!");
                        return;
                    }
                    if(("0".equals(modelDetail.approve))&&("hostId".equals(equal))){
                        meetingChangeListener(modelDetail.meetingId, detailId, modelDetail.memberId,
                                modelDetail.memberName, ModelNotif.EVENT_TYPE);
                    }
                    if(("3".equals(modelDetail.approve))&&("memberId".equals(equal))){
                        meetingChangeListener(modelDetail.meetingId, detailId, modelDetail.memberId,
                                modelDetail.memberName, ModelNotif.INVITATION_TYPE);
                    }
                    Log.e(TAG, "meetingId is !" + modelDetail.meetingId);
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void relationChangeListener() {
        progressBar.setVisibility(View.VISIBLE);
        mFirebaseDatabase.child("Relation").child(myIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    ModelRelation modelRelation = childSnapshot.getValue(ModelRelation.class);
                    String relationId = childSnapshot.getKey();
                    if (modelRelation == null) {
                        Log.e(TAG, "ModelUser data is null!");
                        return;
                    }
                    if("0".equals(modelRelation.approve)){
                        isinotif.add(new ModelNotif(relationId, myIdS,
                                modelRelation.relationUserId,
                                modelRelation.relationName,
                                ModelNotif.RELATION_TYPE
                        ));
                    }
                }
                if (isAdded()){
                    datakamus = new AdapterNotif(getActivity().getApplicationContext(),isinotif);
                    recyclerView.setAdapter(datakamus);
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
        progressBar.setVisibility(View.INVISIBLE);
    }

}
