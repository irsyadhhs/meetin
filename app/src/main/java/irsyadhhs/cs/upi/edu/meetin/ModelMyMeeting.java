package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/24/2018.
 */

public class ModelMyMeeting {

    public String meetingTitle;
    public String hostId;
    public String hostName;
    public String about;
    public String description;
    public String location;
    public String latitude;
    public String longitude;
    public String date;
    public String timeStart;
    public String timeEnd;
    public String status;
    public String code;
    public String meetingId;
    public String detailId;
    public int dateDiff;
    public int sync;
    public long hourDiff;

    public ModelMyMeeting(){

    }

    public ModelMyMeeting(String meetingTitle, String hostId, String hostName, String about, String description,
                        String location, String latitude, String longitude, String date, String timeStart,
                        String timeEnd, String status, String code, int dateDiff, long hourDiff){
        this.meetingTitle = meetingTitle;
        this.hostId = hostId;
        this.hostName = hostName;
        this.about = about;
        this.description = description;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.status = status;
        this.code = code;
        this.dateDiff = dateDiff;
        this.hourDiff = hourDiff;
        this.sync = 0;
    }

    public void setMeetingId(String meetingId){
        this.meetingId = meetingId;
    }

    public String getMeetingId(){
        return this.meetingId;
    }

    public void setMeetingTitle(String title) {
        this.meetingTitle = title;
    }

    public String getTitle() { return meetingTitle; }

    public void setHostId(String hostId ) {
        this.hostId = hostId;
    }

    public String getHostId() {
        return hostId;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName ) {
        this.hostName = hostName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude ) {
        this.longitude = longitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode(){
        return code;
    }

    public void setCode(String code){
        this.code = code;
    }

    public void setDateDiff(int dateDiff){
        this.dateDiff = dateDiff;
    }

    public int getDateDiff(){
        return this.dateDiff;
    }

    public void setHourDiff(long hourDiff){
        this.hourDiff = hourDiff;
    }

    public long getHourDiff(){ return this.hourDiff; }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }
}
