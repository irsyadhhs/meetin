package irsyadhhs.cs.upi.edu.meetin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class FindEvent extends AppCompatActivity {
    TextView noEventTV;
    EditText kode_et;
    String mInputEvent, myIdS, myNameS, hostIdS, meetingIdS;
    SharedPreferences sp;

    private ProgressBar progressBar;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    private RecyclerView recyclerView;
    FirebaseRecyclerAdapter<ModelMeeting, EventHolder> mEventAdapter;

    final static String TAG = "FindEvent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_event);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FindEvent.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();

        sp = getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");

        noEventTV = findViewById(R.id.noEventInfo);
        kode_et = findViewById(R.id.kode_et);
        recyclerView = findViewById(R.id.listViewFindEvent);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    public void findEvent(View view){
        String orderBy = "";
        mInputEvent = kode_et.getText().toString();

        if(TextUtils.isDigitsOnly(mInputEvent)){
            orderBy = "code";
        }else{
            orderBy = "meetingTitle";
        }

        progressBar.setVisibility(View.VISIBLE);

        populateEventFound(mInputEvent, orderBy);
        mEventAdapter.startListening();
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != this.getCurrentFocus()) {
            imm.hideSoftInputFromWindow(this.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
        }
    }

    public void populateEventFound(String input, String orderBy){
        //2605660641

        DatabaseReference userRef = mFirebaseDatabase.child("MeetingList");
        Query query = userRef.orderByChild(orderBy).startAt(input).endAt(input + "\uf8ff");
        FirebaseRecyclerOptions personsOptions = new FirebaseRecyclerOptions.Builder<ModelMeeting>().setQuery(query, ModelMeeting.class).build();

        mEventAdapter = new FirebaseRecyclerAdapter<ModelMeeting, EventHolder>(personsOptions) {
            @Override
            protected void onBindViewHolder(final EventHolder holder, int position, final ModelMeeting model) {
                Log.d(TAG, "itemCount "+getItemCount());
                final String meetingId = getRef(position).getKey();
                final boolean isPublic;
                if (("1".equals(model.status))||("2".equals(model.status))) {
                    isPublic = false;
                }else{
                    isPublic = true;
                }
                Log.d(TAG, "input "+mInputEvent+" meetingID "+meetingId+" isPublic "+isPublic);
                mFirebaseDatabase.child("MeetingDetail").orderByChild("meetingId").equalTo(meetingId)
                        .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                            ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                            if (modelDetail == null) {
                                Log.e(TAG, "ModelUser data is null!");
                                return;
                            }
                            String approve;
                            if(modelDetail.memberId.equals(myIdS)){
                                approve = modelDetail.approve;
                            }else{
                                approve = "-1";
                            }
                            Log.d(TAG, "isPublic "+isPublic+" approve "+approve);
                            if(isPublic) {
                                holder.setEvent(model, approve, modelDetail.memberId, meetingId);
                            }else{
                                progressBar.setVisibility(View.INVISIBLE);
                                noEventTV.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.INVISIBLE);
                            }
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.e(TAG, "Failed to read user", error.toException());
                    }
                });
            }
            @Override
            public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_find_event, parent, false);

                return new EventHolder(view);
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if(getItemCount() == 0){
                    noEventTV.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                }else{
                    noEventTV.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        };
        recyclerView.setAdapter(mEventAdapter);
        progressBar.setVisibility(View.GONE);
    }

    public class EventHolder extends RecyclerView.ViewHolder{
        ImageView addEventImV;
        TextView eventNameTV, eventNoticeTV;
        public EventHolder(View itemView) {
            super(itemView);
            this.eventNameTV = itemView.findViewById(R.id.eventName);
            this.eventNoticeTV = itemView.findViewById(R.id.eventNotice);
            this.addEventImV = itemView.findViewById(R.id.addEvent);
        }

        public void setEvent(final ModelMeeting model, final String approve, final String memberId, final String meetingId){
            if("-1".equals(approve)){
                addEventImV.setImageResource(R.drawable.ic_add_black_24dp);
                addEventImV.setClickable(true);
                eventNoticeTV.setVisibility(View.INVISIBLE);
            }else if("0".equals(approve)){
                addEventImV.setImageResource(R.drawable.ic_check_black_24dp);
                eventNoticeTV.setVisibility(View.VISIBLE);
                eventNoticeTV.setText("Permintaan sedang dikirim");
                addEventImV.setClickable(false);
            }else if("1".equals(approve)){
                addEventImV.setImageResource(R.drawable.ic_check_black_24dp);
                eventNoticeTV.setVisibility(View.VISIBLE);
                eventNoticeTV.setText("Anda sudah tergabung");
                addEventImV.setClickable(false);
            }else if(model.hostId.equals(memberId)){
                addEventImV.setImageResource(R.drawable.ic_check_black_24dp);
                eventNoticeTV.setVisibility(View.VISIBLE);
                eventNoticeTV.setText("Jadwal dibuat oleh Anda");
                addEventImV.setClickable(false);
            }
            this.eventNameTV.setText(model.meetingTitle);

            this.addEventImV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String detailId = mFirebaseDatabase.push().getKey();
                    ModelDetail modelDetail = new ModelDetail(meetingId, model.hostId, myIdS, myNameS, "0", "0", "0", "0");
                    modelDetail.setSync(0);
                    mFirebaseDatabase.child("MeetingDetail").child(detailId).setValue(modelDetail);
                }
            });
        }
    }
}
