package irsyadhhs.cs.upi.edu.meetin;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.KEHADIRAN;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TIDAK_ADA_JADWAL;

/**
 * Created by HARVI on 2/19/2018.
 */

public class StopServiceReceiver extends BroadcastReceiver {
    DatabaseReference mFirebaseDatabase;
    Context mContext;
    NotificationManager mNotificationManager;
    SharedPreferences sp;
    @Override
    public void onReceive(Context context, Intent intent) {
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        mContext = context;

        Bundle extras = intent.getExtras();
        String action = intent.getAction();

        sp = context.getSharedPreferences(SP, Context.MODE_PRIVATE);

        int geoId = extras.getInt(AppConfig.EXTRA_GEO_ID);
        int notifId = geoId * -1;

        Intent notifIntent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, notifId, notifIntent, 0);

        String channelId = "";
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            channelId = createNotificationChannel();
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, String.valueOf(notifId))
                .setSmallIcon(R.drawable.ic_alarm_black_24dp)
                .setContentTitle("Pertemuan Selesai")
                .setContentText("ketuk disini jika kehadiran tidak diperbaharui")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setChannelId(channelId)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(notifId, mBuilder.build());

        final String detailId = extras.getString(AppConfig.EXTRA_DETAIL_ID);

        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
        Date systemDate = Calendar.getInstance().getTime();
        final String timeLeave = format.format(systemDate);

        Log.d("alarmstop", "stopping running");
        Log.d(TAG, "stop running detailId " + detailId);
        Log.d(TAG, "stop running timeLeave " + timeLeave);

        sp.edit().putString(KEHADIRAN, TIDAK_ADA_JADWAL).apply();

        mFirebaseDatabase.child("MeetingDetail").child(detailId).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelDetail model = dataSnapshot.getValue(ModelDetail.class);
                if (model == null) {
                    Log.e(TAG, "ModelUser data is null!");
                    return;
                }
                if((("1".equals(model.attendance))||("4".equals(model.attendance)))){
                    mFirebaseDatabase.child("MeetingDetail").child(detailId).child("timeLeave").setValue(timeLeave);
                }else if("3".equals(model.attendance)){
                    mFirebaseDatabase.child("MeetingDetail").child(detailId).child("attendance").setValue("1");
                    //mFirebaseDatabase.child("MeetingDetail").child(detailId).child("timeLeave").setValue(timeLeave);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Intent i = new Intent(context, ServiceAttendance.class);
        i.setAction(action);
        context.startService(i);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(){
        String channelId = "end_service";
        String channelName = "Schedule End Info";
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_LOW);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.createNotificationChannel(chan);
        return channelId;
    }

}
