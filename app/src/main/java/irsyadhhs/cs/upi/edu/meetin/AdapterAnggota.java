package irsyadhhs.cs.upi.edu.meetin;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;

/**
 * Created by HARVI on 2/6/2018.
 */

public class AdapterAnggota extends RecyclerView.Adapter<AdapterAnggota.ViewHolder>{
    private static ArrayList<ModelAnggota> searchArrayList;
    private LayoutInflater mInflater;
    private Context context;

    private static int position;
    private static String userId;
    private static String detailId;

    String myIdS, hostIdS;
    SharedPreferences sp;

    public static String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        AdapterAnggota.userId = userId;
    }

    public static String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        AdapterAnggota.detailId = detailId;
    }

    public static int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public AdapterAnggota(Context context, ArrayList<ModelAnggota> results, String hostId) {
        searchArrayList = results;
        this.context = context;
        this.sp = context.getSharedPreferences(SP, MODE_PRIVATE);
        this.myIdS = sp.getString(AppConfig.MY_ID, "0");
        this.hostIdS = hostId;
        mInflater = LayoutInflater.from(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{
        TextView member;
        ImageView iconMore;
        ImageView memberPicture;
        public ViewHolder(View itemView) {
            super(itemView);
            this.member = (TextView) itemView.findViewById(R.id.member);
            this.iconMore = itemView.findViewById(R.id.iconMore);
            this.memberPicture = itemView.findViewById(R.id.memberPicture);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if(hostIdS.equals(myIdS)){
                menu.add(Menu.NONE, R.id.menu_profile,
                        Menu.NONE, "Lihat Profil");
                menu.add(Menu.NONE, R.id.menu_admin,
                        Menu.NONE, "Jadikan Admin");
                menu.add(Menu.NONE, R.id.menu_delete,
                        Menu.NONE, "Hapus Anggota");
            }else{
                menu.add(Menu.NONE, R.id.menu_profile,
                        Menu.NONE, "Lihat Profil");
            }
        }
    }

    @Override
    public AdapterAnggota.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.member_list, parent, false);

        AdapterAnggota.ViewHolder myViewHolder = new AdapterAnggota.ViewHolder(view);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final AdapterAnggota.ViewHolder holder, final int position) {
        holder.member.setText(searchArrayList.get(position).getMemberName());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(holder.getAdapterPosition());
                setUserId(searchArrayList.get(holder.getAdapterPosition()).getMemberId());
                setDetailId(searchArrayList.get(holder.getAdapterPosition()).getDetailId());
                return false;
            }
            /*@Override
            public void onClick(View v) {

            }*/
        });
        Picasso.with(context)
                .load(searchArrayList.get(position).getPicture())
                .resize(96,96)
                .centerCrop()
                .error(R.drawable.ic_account_circle_black_48dp)
                .placeholder(R.drawable.ic_account_circle_black_48dp)
                .transform(new CircleTransform())
                .into(holder.memberPicture);
        if(myIdS.equals(searchArrayList.get(position).getMemberId())){
            holder.iconMore.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        holder.iconMore.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

}
