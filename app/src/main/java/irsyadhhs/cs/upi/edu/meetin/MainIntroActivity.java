package irsyadhhs.cs.upi.edu.meetin;

import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

public class MainIntroActivity extends IntroActivity{
    @Override protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // Add slides, edit configuration...
        setButtonBackVisible(false);
        addSlide(new SimpleSlide.Builder()
                .title("Tampilan Sederhana")
                .description("Mengatur pertemuan dengan mudah")
                .image(R.drawable.tutor1)
                .background(R.color.colorLightBlue)
                .backgroundDark(R.color.colorDarkBlue)
                .scrollable(false)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Jadwal Terorganisir")
                .description("Fokus pada jadwal mendatang, simpan riwayat yang lalu")
                .image(R.drawable.tutor2)
                .background(R.color.colorLightBlue)
                .backgroundDark(R.color.colorDarkBlue)
                .scrollable(false)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Rincian Lengkap")
                .description("Temukan informasi pertemuan, daftar kehadiran, dan catatan pertemuan")
                .image(R.drawable.tutor3)
                .background(R.color.colorLightBlue)
                .backgroundDark(R.color.colorDarkBlue)
                .scrollable(false)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Temukan Relasi")
                .description("Kenali lebih jauh sesama anggota pertemuan")
                .image(R.drawable.tutor4)
                .background(R.color.colorLightBlue)
                .backgroundDark(R.color.colorDarkBlue)
                .scrollable(false)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Catatan Pertemuan")
                .description("Catat hal yang penting, bagikan dengan ketua")
                .image(R.drawable.tutor5)
                .background(R.color.colorLightBlue)
                .backgroundDark(R.color.colorDarkBlue)
                .scrollable(false)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Draf Jadwal")
                .description("Simpan semua jadwal yang belum siap dengan aman")
                .image(R.drawable.tutor6)
                .background(R.color.colorLightBlue)
                .backgroundDark(R.color.colorDarkBlue)
                .scrollable(false)
                .build());
    }
}
