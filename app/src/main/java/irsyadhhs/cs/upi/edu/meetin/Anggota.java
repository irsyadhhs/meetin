package irsyadhhs.cs.upi.edu.meetin;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class Anggota extends AppCompatActivity {

    AdapterAnggota datamember;
    private static RecyclerView recyclerView;

    ArrayList<ModelAnggota> mAnggota = new ArrayList<ModelAnggota>();
    ArrayList<HashMap<String, String>> isianggota;

    String titles, meetingIdS, memberName, detailId, memberId, myIdS, hostIdS;

    SharedPreferences sp;

    TextView title;

    private DatabaseReference mFirebaseDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anggota);

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Anggota.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        //isianggota = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra(AppConfig.EXTRA_ARRAYLIST_ANGGOTA);

        title = (TextView) findViewById(R.id.title);
        recyclerView = findViewById(R.id.listView2);

        Bundle extras = getIntent().getExtras();

        meetingIdS = extras.getString(AppConfig.EXTRA_MEETING_ID);
        titles = extras.getString(AppConfig.EXTRA_MEETING_TITLE);
        hostIdS = extras.getString(AppConfig.EXTRA_MEETING_HOSTID);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        registerForContextMenu(recyclerView);

        sp = getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");

        title.setText(titles);
        loadMember();
    }

    public void loadMember() {
        mAnggota.clear();
        mFirebaseDatabase.child("MeetingDetail").orderByChild("meetingId").equalTo(meetingIdS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    String detailId = childSnapshot.getKey();
                    ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                    if (modelDetail == null) {
                        return;
                    }
                    if("1".equals(modelDetail.approve)) {
                        final ModelAnggota anggota = new ModelAnggota(detailId, meetingIdS, modelDetail.memberId, modelDetail.memberName);
                        mFirebaseDatabase.child("Users").child(modelDetail.memberId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                                if (modelUser == null) {
                                    return;
                                }
                                anggota.setPicture(modelUser.picture);
                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                Log.e(TAG, "Failed to read user", error.toException());
                            }
                        });
                        mAnggota.add(anggota);
                    }
                }
                datamember = new AdapterAnggota(Anggota.this, mAnggota, hostIdS);
                recyclerView.setAdapter(datamember);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        String memberId = "";
        final String detailId;
        try {
            memberId = AdapterAnggota.getUserId();
            detailId = AdapterAnggota.getDetailId();
        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage(), e);
            return super.onContextItemSelected(item);
        }

        switch (item.getItemId()) {
            case R.id.menu_profile:
                Intent intent = new Intent(Anggota.this, Profile.class);
                intent.putExtra(AppConfig.PROFILE_USER_ID, memberId);
                startActivity(intent);
                break;
            case R.id.menu_admin:
                mFirebaseDatabase.child("MeetingDetail").child(detailId).child("admin").setValue("1");
                break;
            case R.id.menu_delete:
                final AlertDialog.Builder ad = new AlertDialog.Builder(this);
                ad.setMessage("Hapus anggota?")
                        .setCancelable(false)
                        .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mFirebaseDatabase.child("MeetingDetail").child(detailId).removeValue();
                                loadMember();
                                datamember.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ad.create().dismiss();
                            }
                        });
                AlertDialog alert = ad.create();
                alert.show();
                break;
        }
        return super.onContextItemSelected(item);
    }
}
