package irsyadhhs.cs.upi.edu.meetin;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;

/**
 * Created by HARVI on 2/14/2018.
 */

public class AdapterAddAnggota extends RecyclerView.Adapter<AdapterAddAnggota.ViewHolder>{
    private static ArrayList<ModelAddAnggota> searchArrayList;

    private LayoutInflater mInflater;
    private Context context;
    SharedPreferences sp;
    String myIdS, myNameS, titleS;
    private DatabaseReference mFirebaseDatabase;

    private static AdapterCallback mAdapterCallback;

    public AdapterAddAnggota(Context context) {
        try {
            this.mAdapterCallback = ((AdapterCallback) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    public AdapterAddAnggota(Context context, ArrayList<ModelAddAnggota> results, String title) {
        searchArrayList = results;
        this.context = context;
        mInflater = LayoutInflater.from(context);
        sp = context.getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        this.titleS = title;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        MyClickListener listener;
        TextView member;
        TextView memberAdded;
        ImageView addMember;
        ImageView cancelMember;
        ImageView picture;

        public ViewHolder(View itemView, MyClickListener listener) {
            super(itemView);
            this.member = (TextView) itemView.findViewById(R.id.memberAddAnggota);
            this.memberAdded = (TextView) itemView.findViewById(R.id.memberAdded);
            this.addMember = itemView.findViewById(R.id.addMember);
            this.cancelMember = itemView.findViewById(R.id.cancelMember);
            this.picture = itemView.findViewById(R.id.memberPicture);
            this.listener = listener;

            addMember.setOnClickListener(this);
            cancelMember.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.addMember:
                    listener.onAdd(this.getAdapterPosition());
                    try {
                        mAdapterCallback.onMethodCallback();
                    } catch (ClassCastException exception) {
                        // do something
                    }
                    break;
                case R.id.cancelMember:
                    listener.onDelete(this.getAdapterPosition());
                    try {
                        mAdapterCallback.onMethodCallback();
                    } catch (ClassCastException exception) {
                        // do something
                    }
                    break;
                default:
                    break;
            }
        }

        public interface MyClickListener {
            void onAdd(int p);
            void onDelete(int p);
        }
    }

    public interface AdapterCallback {
        void onMethodCallback();
    }

    @Override
    public AdapterAddAnggota.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_anggota_list, parent, false);

        final AdapterAddAnggota.ViewHolder myViewHolder = new AdapterAddAnggota.ViewHolder(view, new ViewHolder.MyClickListener() {
            @Override
            public void onAdd(int p) {
                String detailId = mFirebaseDatabase.push().getKey();
                ModelDetail modelDetail = new ModelDetail(searchArrayList.get(p).meetingId, searchArrayList.get(p).hostId,
                        searchArrayList.get(p).memberId, searchArrayList.get(p).memberName, "3", "0", "0", "0");
                modelDetail.setSync(0);
                mFirebaseDatabase.child("MeetingDetail").child(detailId).setValue(modelDetail);
                mFirebaseDatabase.child("MeetingList").child(searchArrayList.get(p).meetingId).child("sync").setValue(0);
                String body = "Undangan di " + titleS;
                mFirebaseDatabase.child("messages").push().setValue(new ModelMessage(body, myIdS, searchArrayList.get(p).memberId));
            }

            @Override
            public void onDelete(int p) {
                mFirebaseDatabase.child("MeetingDetail").child(searchArrayList.get(p).detailId).removeValue();
            }
        });

        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final AdapterAddAnggota.ViewHolder holder, int position) {
        holder.member.setText(searchArrayList.get(position).memberName);
        Picasso.with(context)
                .load(searchArrayList.get(position).getPicture())
                .resize(96,96)
                .centerCrop()
                .error(R.drawable.ic_account_circle_black_48dp)
                .placeholder(R.drawable.ic_account_circle_black_48dp)
                .transform(new CircleTransform())
                .into(holder.picture);
        Log.d("detailId di adapter", "detail id " + searchArrayList.get(position).detailId);
        if(!"-1".equals(searchArrayList.get(position).approve)){
            holder.memberAdded.setVisibility(View.VISIBLE);
            holder.memberAdded.setText("anggota sudah ditambahkan");
            holder.cancelMember.setVisibility(View.VISIBLE);
            holder.cancelMember.setClickable(true);
            holder.addMember.setVisibility(View.GONE);
            holder.addMember.setClickable(false);
        }
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }

}
