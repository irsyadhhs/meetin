package irsyadhhs.cs.upi.edu.meetin;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class CreateMeeting extends AppCompatActivity {

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener dateDialog;
    TimePickerDialog.OnTimeSetListener timeStartDialog;
    TimePickerDialog.OnTimeSetListener timeEndDialog;
    TextView titlePageTV;
    EditText date_et, topik_et, deskripsi_et, lokasi_et, title_et, picker_et;
    String myNameS,myIdS, meetingIdS, modeS, hostIdS, dateS, timeStart, duration, timeEndS;
    String topikS, deskripsiS, lokasiS, titleS, placeNameS, codeS, mDraftId;
    boolean isDraft = false;
    Button btnCreate;
    TextView waktuStartPicker;
    EditText waktuEndPicker;
    double latl=0, longl=0;
    SharedPreferences sp;

    private int PLACE_PICKER_REQUEST = 1;

    private DatabaseReference mFirebaseDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_meeting);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateMeeting.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        myCalendar = Calendar.getInstance();

        date_et= findViewById(R.id.date_et);
        topik_et = findViewById(R.id.topik_et);
        deskripsi_et = findViewById(R.id.deskripsi_et);
        lokasi_et = findViewById(R.id.lokasi_et);
        title_et = findViewById(R.id.title_et);
        picker_et = findViewById(R.id.picker_et);
        btnCreate = findViewById(R.id.btnSumbit);
        titlePageTV = findViewById(R.id.titleCreateTV);
        waktuStartPicker = findViewById(R.id.waktuStartPicker);
        waktuEndPicker = findViewById(R.id.waktuEndPicker);

        sp = getSharedPreferences(SP, MODE_PRIVATE);
        myNameS = sp.getString(AppConfig.MY_NAME, "");
        myIdS = sp.getString(AppConfig.MY_ID, "");

        Bundle extras = getIntent().getExtras();
        hostIdS = extras.getString(AppConfig.EXTRA_MEETING_HOSTID);
        modeS = extras.getString(AppConfig.MODE);

        if("draft".equals(modeS)){
            isDraft = true;
            this.mDraftId = extras.getString(AppConfig.EXTRA_MEETING_ID);
            title_et.setText(extras.getString(AppConfig.EXTRA_MEETING_TITLE));
            topik_et.setText(extras.getString(AppConfig.EXTRA_MEETING_ABOUT));
            deskripsi_et.setText(extras.getString(AppConfig.EXTRA_MEETING_DESCRIPTION));
            date_et.setText(extras.getString(AppConfig.EXTRA_MEETING_DATE));
            waktuStartPicker.setText(extras.getString(AppConfig.EXTRA_MEETING_TIMESTART));

            String duration = getDuration(extras.getString(AppConfig.EXTRA_MEETING_TIMESTART),extras.getString(AppConfig.EXTRA_MEETING_TIMEEND));

            waktuEndPicker.setText(duration);
        }else if("edit".equals(modeS)){
            this.latl = Double.valueOf(extras.getString(AppConfig.EXTRA_MEETING_LATITUDE));
            this.longl = Double.valueOf(extras.getString(AppConfig.EXTRA_MEETING_LONGITUDE));
            this.meetingIdS = extras.getString(AppConfig.EXTRA_MEETING_ID);

            title_et.setText(extras.getString(AppConfig.EXTRA_MEETING_TITLE));
            topik_et.setText(extras.getString(AppConfig.EXTRA_MEETING_ABOUT));
            deskripsi_et.setText(extras.getString(AppConfig.EXTRA_MEETING_DESCRIPTION));
            lokasi_et.setText(extras.getString(AppConfig.EXTRA_MEETING_LOCATION));
            date_et.setText(extras.getString(AppConfig.EXTRA_MEETING_DATE));
            waktuStartPicker.setText(extras.getString(AppConfig.EXTRA_MEETING_TIMESTART));

            String duration = getDuration(extras.getString(AppConfig.EXTRA_MEETING_TIMESTART),extras.getString(AppConfig.EXTRA_MEETING_TIMEEND));

            waktuEndPicker.setText(duration);

            titlePageTV.setText("Update Pertemuan");
            btnCreate.setText("UPDATE");
        }else{
            btnCreate.setText("SUBMIT");
        }

        dateDialog = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                date_et.setText(sdf.format(myCalendar.getTime()));
            }

        };

        date_et.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(CreateMeeting.this, dateDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dpd.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
                dpd.show();
            }
        });

        timeStartDialog = new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                myCalendar.set(Calendar.MINUTE, minute);

                String myHour = "HH:mm";
                SimpleDateFormat sdf = new SimpleDateFormat(myHour, Locale.US);
                waktuStartPicker.setText(sdf.format(myCalendar.getTime()));
            }

        };

        waktuStartPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
                int minute = myCalendar.get(Calendar.MINUTE);
                new TimePickerDialog(CreateMeeting.this, timeStartDialog, hour, minute, true).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.draft_menu, menu);
        MenuItem saveToDraft = menu.findItem(R.id.menu_save);
        if("edit".equals(modeS)){
            saveToDraft.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save:
                if(!isDraft) {
                    saveToDraft();
                    Toast.makeText(this, "Jadwal disimpan di draft", Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    updateDraftValue();
                    finish();
                }
                return true;
            case R.id.menu_delete:
                final AlertDialog.Builder ad = new AlertDialog.Builder(this);
                ad.setMessage("Hapus jadwal?")
                        .setCancelable(false)
                        .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if("add".equals(modeS)){
                                    finish();
                                }else if("edit".equals(modeS)){
                                    mFirebaseDatabase.child("MeetingList").child(meetingIdS).removeValue();
                                    finish();
                                }else if("draft".equals(modeS)){
                                    mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).removeValue();
                                    finish();
                                }
                            }
                        })
                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ad.create().dismiss();
                            }
                        });
                AlertDialog alert = ad.create();
                alert.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* ModelUser data change listener */
    private void getUser(String UID) {
        // ModelUser data change listener
        mFirebaseDatabase.child("Users").child(UID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                // Check for null
                if (modelUser == null) {
                    return;
                }
                sp.edit().putString(AppConfig.MY_NAME, modelUser.name).apply();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void getText(){
        titleS = title_et.getText().toString();
        topikS = topik_et.getText().toString();
        deskripsiS = deskripsi_et.getText().toString();
        lokasiS = lokasi_et.getText().toString();
        dateS = date_et.getText().toString();
        timeStart = waktuStartPicker.getText().toString();
        duration = waktuEndPicker.getText().toString();
    }

    public boolean validate(){
        boolean valid = true;
        getText();
        if (topikS.isEmpty()) {
            valid = false;
        }else if (titleS.isEmpty()) {
            valid = false;
        }else if (lokasiS.isEmpty()) {
            valid = false;
        }else if(latl==0 && longl==0){
            valid = false;
        }else if (dateS.isEmpty()){
            valid = false;
        }else if (timeStart.isEmpty()){
            valid = false;
        }

        if (duration.isEmpty()){
            valid = false;
        }else{
            timeEndS = getTimeEnd(timeStart, duration);
        }
        return valid;
    }

    private String getTimeEnd(String timeStart, String duration) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
        long timeStarttoLong = 0;
        try {
            timeStarttoLong = format.parse(timeStart).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return format.format(timeStarttoLong + (Integer.parseInt(duration) * 60 * 1000));
    }

    public String getDuration(String timeStart, String timeEnd){
        String duration="";
        try {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date date1 = format.parse(timeStart);
            Date date2 = format.parse(timeEnd);

            long millse = date1.getTime() - date2.getTime();
            long mills = Math.abs(millse);

            int mins = (int) mills / (1000 * 60);

            duration = String.valueOf(mins);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return duration;
    }

    public void prosesCreate(View view){
        if("add".equals(modeS)  || "draft".equals(modeS)){
            if(validate()) {
                if(getDate(dateS) == 0 && getTime(timeStart) == -1){
                    Toast.makeText(this, "Waktu mulai tidak valid!", Toast.LENGTH_SHORT).show();
                }else {
                    Random r = new Random();
                    int kode1 = r.nextInt(999999) + 100000;
                    String kode2 = dateS.substring(0, 2);
                    String kode3 = dateS.substring(3, 5);

                    codeS = kode2 + kode3 + kode1;

                    getUser(hostIdS);
                    String meetingId = mFirebaseDatabase.push().getKey();
                    String detailId = mFirebaseDatabase.push().getKey();

                    ModelMeeting modelMeeting = new ModelMeeting(titleS, hostIdS, myNameS, topikS, deskripsiS,
                            lokasiS, String.valueOf(latl), String.valueOf(longl), dateS, timeStart, timeEndS, "0", codeS);
                    modelMeeting.setSync(1);
                    ModelDetail modelDetail = new ModelDetail(meetingId, hostIdS, hostIdS, myNameS, "1", "0", "0", "0");
                    modelDetail.setSync(2);
                    mFirebaseDatabase.child("MeetingList").child(meetingId).setValue(modelMeeting);
                    mFirebaseDatabase.child("MeetingDetail").child(detailId).setValue(modelDetail);

                    if ("draft".equals(modeS)) {
                        mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).removeValue();
                    }

                    Intent intent = new Intent("create-meeting");
                    intent.putExtra(AppConfig.EXTRA_MEETING_ID, meetingIdS);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, titleS);
                    intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, hostIdS);
                    intent.putExtra(AppConfig.EXTRA_MEETING_CODE, codeS);
                    intent.putExtra(AppConfig.EXTRA_MEMBER_USER1, myNameS);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }else{
                final AlertDialog.Builder ad = new AlertDialog.Builder(this);
                ad.setMessage("Data jadwal belum lengkap")
                        .setCancelable(false)
                        .setPositiveButton("Simpan ke draft", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if(!isDraft) {
                                    saveToDraft();
                                    Toast.makeText(getApplicationContext(), "Jadwal disimpan di draft", Toast.LENGTH_SHORT).show();
                                    finish();
                                }else{
                                    updateDraftValue();
                                    finish();
                                }
                            }
                        })
                        .setNegativeButton("Kembali", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ad.create().dismiss();
                            }
                        });
                AlertDialog alert = ad.create();
                alert.show();
            }
        }else if("edit".equals(modeS) && validate()){
            updateMeetingValue();

            mFirebaseDatabase.child("MeetingDetail").orderByChild("meetingId").equalTo(meetingIdS)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                        String detailId = childSnapshot.getKey();
                        if (modelDetail == null) {
                            Log.e(TAG, "ModelUser data is null!");
                            return;
                        }
                        if("1".equals(modelDetail.approve)){
                            mFirebaseDatabase.child("MeetingDetail").child(detailId).child("sync").setValue(2);
                            String body = titleS;
                            if(!modelDetail.memberId.equals(myIdS)) {
                                mFirebaseDatabase.child("messages").push().setValue(new ModelMessage(body, myIdS, modelDetail.memberId));
                            }
                        }
                    }
                }
                @Override
                public void onCancelled(DatabaseError error) {
                    Log.e(TAG, "Failed to read user", error.toException());
                }
            });
            Intent intent = new Intent("create-meeting");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            Intent intent2 = new Intent("delete-meeting");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
            setResult(RESULT_OK);
            finish();
        }
    }

    private void updateMeetingValue() {
        if (!titleS.isEmpty()) {
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("meetingTitle").setValue(titleS);
        }
        if (!topikS.isEmpty()) {
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("about").setValue(topikS);
        }
        if (!deskripsiS.isEmpty()) {
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("description").setValue(deskripsiS);
        }
        if (!lokasiS.isEmpty()) {
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("location").setValue(lokasiS);
        }
        if(latl!=0 && longl!=0){
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("latitude").setValue(String.valueOf(latl));
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("longitude").setValue(String.valueOf(longl));
        }
        if (!dateS.isEmpty()){
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("date").setValue(dateS);
        }
        if (!timeStart.isEmpty()){
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("timeStart").setValue(timeStart);
        }
        if (!duration.isEmpty()){
            timeEndS = getTimeEnd(timeStart, duration);
            Log.d(TAG, "new timeend "+ timeEndS);
            mFirebaseDatabase.child("MeetingList").child(meetingIdS).child("timeEnd").setValue(timeEndS);
        }
    }

    private void updateDraftValue() {
        getText();
        if (!titleS.isEmpty()) {
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("meetingTitle").setValue(titleS);
        }
        if (!topikS.isEmpty()) {
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("about").setValue(topikS);
        }
        if (!deskripsiS.isEmpty()) {
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("description").setValue(deskripsiS);
        }
        if (!lokasiS.isEmpty()) {
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("location").setValue(lokasiS);
        }
        if(latl!=0 && longl!=0){
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("latitude").setValue(String.valueOf(latl));
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("longitude").setValue(String.valueOf(longl));
        }
        if (!dateS.isEmpty()){
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("date").setValue(dateS);
        }
        if (!timeStart.isEmpty()){
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("timeStart").setValue(timeStart);
        }
        if (!duration.isEmpty()){
            timeEndS = getTimeEnd(timeStart, duration);
            Log.d(TAG, "new timeend "+ timeEndS);
            mFirebaseDatabase.child("Draft").child(myIdS).child(mDraftId).child("timeEnd").setValue(timeEndS);
        }
    }

    public int getDate(String dateS) {
        int diff;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String strDate = mdformat.format(calendar.getTime());

        try {
            Date date1 = mdformat.parse(dateS);
            Date date2 = mdformat.parse(strDate);

            diff = ((int) ((date1.getTime() / (24 * 60 * 60 * 1000)) - (int) (date2.getTime() / (24 * 60 * 60 * 1000))));

            return diff;
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public long getTime(String timeStart) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);

            Date systemDate = Calendar.getInstance().getTime();
            String myDate = format.format(systemDate);
            Date date1 = format.parse(timeStart);
            Date date2 = format.parse(myDate);


            long millse = date1.getTime() - date2.getTime();

            if (millse > 0) {
                return millse;
            } else {
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    private boolean saveToDraft() {
        getText();
        boolean allEmpty = false;
        if(titleS.isEmpty() && topikS.isEmpty() && deskripsiS.isEmpty() && lokasiS.isEmpty() && dateS.isEmpty() && duration.isEmpty()){
            allEmpty = true;
        }
        if (titleS.isEmpty()) {
            titleS="";
        }
        if (topikS.isEmpty()) {
            topikS="";
        }
        if (deskripsiS.isEmpty()) {
            deskripsiS="";
        }
        if (lokasiS.isEmpty()) {
            lokasiS="";
        }
        if (dateS.isEmpty()){
            dateS="";
        }
        if (timeStart.isEmpty()){
            timeStart="";
        }
        if (duration.isEmpty()){
            duration="";
        }
        if(allEmpty){
            return true;
        }else{
            String draftId = mFirebaseDatabase.push().getKey();
            Map<String, String> draft = new HashMap<>();
            draft.put("meetingTitle", titleS);
            draft.put("about", topikS);
            draft.put("description", deskripsiS);
            draft.put("location", lokasiS);
            draft.put("latitude", String.valueOf(latl));
            draft.put("longitude", String.valueOf(longl));
            draft.put("date", dateS);
            draft.put("timeStart", timeStart);
            draft.put("duration", duration);
            mFirebaseDatabase.child("Draft").child(myIdS).child(draftId).setValue(draft);

            return  false;
        }
    }

    public void placePicker(View view){
        // membuat Intent untuk Place Picker
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            //menjalankan place picker
            startActivityForResult(builder.build(CreateMeeting.this), PLACE_PICKER_REQUEST);

            // check apabila <a title="Solusi Tidak Bisa Download Google Play Services di Android" href="http://www.twoh.co/2014/11/solusi-tidak-bisa-download-google-play-services-di-android/" target="_blank">Google Play Services tidak terinstall</a> di HP
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // menangkap hasil balikan dari Place Picker, dan menampilkannya pada TextView
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                latl = place.getLatLng().latitude;
                longl = place.getLatLng().longitude;
                placeNameS = place.getName().toString();
                picker_et.setText(placeNameS);
                picker_et.setTextSize(15);

            }
        }
    }

    @Override
    public void onBackPressed() {
        if(!isDraft) {
            boolean isEmpty = saveToDraft();
            if (!isEmpty) {
                Toast.makeText(this, "Jadwal disimpan di draft", Toast.LENGTH_SHORT).show();
            }
        }else{
            updateDraftValue();
        }
        super.onBackPressed();
    }
}
