package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/24/2018.
 */

public class ModelMeeting {

    public String meetingTitle;
    public String hostId;
    public String hostName;
    public String about;
    public String description;
    public String location;
    public String latitude;
    public String longitude;
    public String date;
    public String timeStart;
    public String timeEnd;
    public String status;
    public String code;
    public String meetingId;
    public int dateDiff;
    public long hourDiff;
    public String admin;
    public String attendance;
    public String detailId;
    public int sync;

    public ModelMeeting(){

    }

    public ModelMeeting(String meetingTitle, String hostId, String hostName, String about, String description,
                         String location, String latitude, String longitude, String date, String timeStart,
                         String timeEnd, String status, String code){
        this.meetingTitle = meetingTitle;
        this.hostId = hostId;
        this.hostName = hostName;
        this.about = about;
        this.description = description;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.status = status;
        this.code = code;
        this.sync = 1;
    }

    public void setMeetingId(String meetingId){
        this.meetingId = meetingId;
    }

    public String getMeetingId(){
        return this.meetingId;
    }

    public void setDateDiff(int dateDiff){
        this.dateDiff = dateDiff;
    }
    public int getDateDiff(){
        return this.dateDiff;
    }

    public void setHourDiff(long hourDiff){
        this.hourDiff = hourDiff;
    }

    public long getHourDiff(){
        return this.hourDiff;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }

    public void setAdmin(String admin){
        this.admin = admin;
    }

    public String getAdmin(){
        return this.admin;
    }

    public void setAttendance(String attendance){
        this.attendance = attendance;
    }

    public String getAttendance(){
        return this.attendance;
    }

    public void setSync(int sync){
        this.sync = sync;
    }

    public int getSync(){
        return this.sync;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

}
