package irsyadhhs.cs.upi.edu.meetin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class Register extends AppCompatActivity {
    EditText nameET, emailET, phoneET, passwordET, repasswordET;
    TextView linkloginTV;
    Button registerBtn;
    String nameS, emailS, phoneS, passwordS, repasswordS;

    SharedPreferences sp;
    SharedPreferences.Editor ed;

    ProgressBar progressBar;
    FirebaseAuth auth;
    FirebaseUser user;
    private FirebaseAuth.AuthStateListener authListener;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        /*Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);*/

        nameET = (EditText) findViewById(R.id.name_et);
        emailET = (EditText) findViewById(R.id.email_et);
        passwordET = (EditText) findViewById(R.id.password_et);
        repasswordET = (EditText) findViewById(R.id.re_password_et);
        linkloginTV = (TextView) findViewById(R.id.link_login);
        registerBtn = (Button) findViewById(R.id.btnRegister);
        progressBar = findViewById(R.id.progressBar);

        auth = FirebaseAuth.getInstance();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("Users");

        sp = getSharedPreferences(SP, MODE_PRIVATE);

    }

    public void toLogin(View view) {
        Intent intent = new Intent(Register.this, Login.class);
        startActivity(intent);
        finish();
    }

    public void registerDone(View view){
        progressBar.setVisibility(View.VISIBLE);

        nameS = nameET.getText().toString().toLowerCase();
        emailS = emailET.getText().toString().toLowerCase();
        passwordS = passwordET.getText().toString();
        repasswordS = repasswordET.getText().toString();

        signup();
    }

    public void signup(){
        if (validate()) {
            registerAuth(emailS, passwordS, nameS);
        }else{
            onSignupFailed();
        }
    }

    public boolean validate(){
        boolean valid = true;

        if (nameS.isEmpty() || nameS.length() < 3) {
            nameET.setError("at least 3 characters");
            valid = false;
        } else {
            nameET.setError(null);
        }

        if (emailS.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailS).matches()) {
            emailET.setError("enter a valid email address");
            valid = false;
        } else {
            emailET.setError(null);
        }


        if (passwordS.isEmpty() || passwordS.length() < 4 || passwordS.length() > 16) {
            passwordET.setError("between 4 and 16 alphanumeric characters");
            valid = false;
        } else {
            passwordET.setError(null);
        }

        if (repasswordS.isEmpty() || repasswordS.length() < 4 || repasswordS.length() > 16 || !(repasswordS.equals(passwordS))) {
            repasswordET.setError("Password Do not match");
            valid = false;
        } else {
            repasswordET.setError(null);
        }
        return valid;
    }

    public void onSignupFailed() {
        Toast.makeText(Register.this, "Sign up failed", Toast.LENGTH_LONG).show();
        registerBtn.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    private void createUser(String name, String email, String UID) {
        ModelUser modelUser = new ModelUser(name, email);
        modelUser.setPicture("https://firebasestorage.googleapis.com/v0/b/quixotic-treat-194714.appspot.com/o/defaultpic.png?alt=media&token=d4f871bb-1676-4b41-8f80-11bf12f3f22f");
        mFirebaseDatabase.child(UID).setValue(modelUser);

        addUserChangeListener(UID);
        auth.signOut();
        if (auth.getCurrentUser() == null) {
            Intent intent = new Intent(Register.this, Login.class);
            startActivity(intent);
            finish();
        }
    }

    /* ModelUser data change listener */
    private void addUserChangeListener(String UID) {
        // ModelUser data change listener
        mFirebaseDatabase.child(UID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                // Check for null
                if (modelUser == null) {
                    Log.e(TAG, "ModelUser data is null!");
                    return;
                }
                sp.edit().putString(AppConfig.MY_NAME, modelUser.name).apply();
                sp.edit().putInt(AppConfig.ALARM_ID, 1).apply();
                sp.edit().putString(AppConfig.INFO_TAG_PREF, "yes").apply();
                Log.e(TAG, "ModelUser data is changed!" + modelUser.name + ", " + modelUser.email);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void registerAuth(final String email, final String password, final String name){
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Toast.makeText(Register.this, "Akun berhasil dibuat", Toast.LENGTH_SHORT).show();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        progressBar.setVisibility(View.GONE);
                        if (!task.isSuccessful()) {
                            Toast.makeText(Register.this, "Email sudah pernah digunakan",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
                            String UID = currentFirebaseUser.getUid();

                            ed = sp.edit();
                            ed.putString(AppConfig.APP_AUTH, "yes").apply();
                            ed.putString(AppConfig.MY_ID, UID).apply();

                            createUser(name, email, UID);
                        }
                    }
                });
    }

    /*public void checkEmail(final String emailCheck){
        StringRequest postRequest = new StringRequest(Request.Method.POST, SELECT_ALL_USER,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // menampilkan respone
                        Log.d("Response POST", response);

                        try {
                            JSONObject jsonobject = new JSONObject(response);
                            JSONArray jsonArray = jsonobject.getJSONArray("users");

                            int i;
                            i = 0;
                            while (i < jsonArray.length()) {
                                JSONObject user = jsonArray.getJSONObject(i);
                                Toast toast = Toast.makeText(Register.this, "ModelUser already exists", Toast.LENGTH_SHORT);
                                toast.show();
                                i++;
                            }
                        } catch (JSONException e) {
                            sp = getSharedPreferences(SP, MODE_PRIVATE);
                            ed = sp.edit();
                            ed.putString(AppConfig.APP_AUTH,"new");
                            ed.apply();
                            insert();
                            Toast.makeText(Register.this, "Register successful", Toast.LENGTH_SHORT).show();
                            onSignupSuccess();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e(TAG, "onErrorResponse: Error= " + error);
                        Log.e(TAG, "onErrorResponse: Error= " + error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                // Menambahkan parameters post
                Map<String, String>  params = new HashMap<String, String>();
                params.put("email", emailCheck);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(postRequest);
    }

    void insert() {
        StringRequest postRequest = new StringRequest(com.android.volley.Request.Method.POST, INSERT_USER,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // menampilkan respone
                        Log.i("Response", response);
                        Log.d("Response POST", response);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        error.printStackTrace();
                        Log.e(TAG, "onErrorResponse: Error= " + error);
                        Log.e(TAG, "onErrorResponse: Error= " + error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                // Menambahkan parameters post
                Map<String, String> params = new HashMap<String, String>();
                params.put("userId", "");
                params.put("name", nameS);
                params.put("email", emailS);
                params.put("phone", phoneS);
                params.put("password", passwordS);

                return params;
            }
        };
        postRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(postRequest);
    }*/

}
