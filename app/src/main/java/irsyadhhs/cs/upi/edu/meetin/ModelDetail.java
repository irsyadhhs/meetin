package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/24/2018.
 */

public class ModelDetail {
    public String meetingId;
    public String hostId;
    public String memberId;
    public String memberName;
    public String approve;
    public String attendance;
    public String timeArrive;
    public String timeLeave;
    public String detailId;
    public String admin;
    public int sync;

    public ModelDetail(){

    }

    public ModelDetail(String meetingId, String hostId, String memberId, String memberName, String approve,
                        String attendance, String timeArrive, String timeLeave){
        this.meetingId = meetingId;
        this.hostId = hostId;
        this.memberId = memberId;
        this.memberName = memberName;
        this.approve = approve;
        this.attendance = attendance;
        this.timeArrive = timeArrive;
        this.timeLeave = timeLeave;
    }

    public void setDetailId(String detailId){
        this.detailId = detailId;
    }
    public String getDetailId(){
        return this.detailId;
    }

    public void setAdmin(String admin){
        this.admin = admin;
    }
    public String getAdmin(){
        if(this.admin == null){
            return "0";
        }else{
            return this.admin;
        }
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }
}
