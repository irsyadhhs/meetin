package irsyadhhs.cs.upi.edu.meetin;

/**
 * Created by HARVI on 2/13/2018.
 */

public class ModelNotif {

    public static final int EVENT_TYPE=0;
    public static final int RELATION_TYPE=1;
    public static final int INVITATION_TYPE=2;
    public int type;

    private String detailId;
    private String userId2;
    private String meetingId;
    private String memberId;
    private String memberName;
    private String name2;
    private String meetingTitle;
    private String userId1;
    private String relationId;

    public ModelNotif(){

    }

    public ModelNotif(String detailId, String meetingId, String memberId, String memberName, String meetingTitle, int type){
        this.detailId = detailId;
        this.meetingId = meetingId;
        this. memberId = memberId;
        this.memberName = memberName;
        this.meetingTitle = meetingTitle;
        this.type = type;
    }

    public ModelNotif(String relationId, String userId1, String userId2, String name2, int type){
        this.userId1 = userId1;
        this.userId2 = userId2;
        this.name2 = name2;
        this.type = type;
        this.relationId = relationId;
    }

    public String getDetailId(){
        return  detailId;
    }
    public void setDetailId(String detailId){
        this.detailId = detailId;
    }

    public String getMeetingId(){
        return  meetingId;
    }
    public void setMeetingId(String meetingId){
        this.meetingId = meetingId;
    }

    public String getMemberId(){
        return  memberId;
    }
    public void setMemberId(String memberId){
        this.memberId = memberId;
    }

    public String getMemberName(){
        return  memberName;
    }

    public void setMemberName(String memberName){
        this.memberName = memberName;
    }

    public void setMeetingTitle(String meetingTitle){
        this.meetingTitle = meetingTitle;
    }

    public String getMeetingTitle(){
        return meetingTitle;
    }

    public String getName2(){
        return name2;
    }

    public String getUserId1(){
        return userId1;
    }
    public String getUserId2(){
        return userId2;
    }
    public String getRelationId(){return this.relationId;}
}
