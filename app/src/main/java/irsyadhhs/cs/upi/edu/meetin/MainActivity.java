package irsyadhhs.cs.upi.edu.meetin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class MainActivity extends AppCompatActivity implements AdapterNotif.EventCallback, AdapterNotif.RelationCallback{

    SharedPreferences sp;
    String myIdS;
    BottomNavigationViewEx navigation;
    Badge badge, relationBadge, eventBadge, createBadge;
    int badgeNumber;
    final static int BNV_NOTIF_POSITION = 3;
    final static int BADGE_NUMBER = 1;
    final static int BNV_EVENT_POSITION = 0;
    final static int BNV_RELATION_POSITION = 2;
    private AdapterNotif mMyAdapter;
    final static String TAG = "MainActivity";
    DatabaseReference mFirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mMyAdapter = new AdapterNotif(this);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        badgeNumber = 0;

        sp = getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");

        navigation = (BottomNavigationViewEx) findViewById(R.id.navigation);

        navigation.setTextVisibility(false);
        navigation.setIconSize(30, 30);
        navigation.enableShiftingMode(false);
        navigation.enableItemShiftingMode(false);
        navigation.enableAnimation(false);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("create-meeting"));

        Log.d(TAG, "1 "+badgeNumber);

        detailChangeListener("memberId");
        detailChangeListener("hostId");
        relationChangeListener();

        Log.d(TAG, "2 "+badgeNumber);

        badge = addBadgeAt(BNV_NOTIF_POSITION, badgeNumber);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId()) {
                    case R.id.NavFragmentFeed:
                        selectedFragment = FragmentHomeContainer.newInstance();
                        removeBadge(BNV_EVENT_POSITION, navigation, eventBadge);
                        removeBadge(BNV_EVENT_POSITION, navigation, createBadge);
                        break;
                    case R.id.NavFragmentNeigh:
                        selectedFragment = FragmentMyMeeting.newInstance();
                        break;
                    case R.id.NavFragmentAround:
                        selectedFragment = FragmentRelation.newInstance();
                        removeBadge(BNV_RELATION_POSITION, navigation, relationBadge);
                        break;
                    case R.id.NavFragmentNotif:
                        selectedFragment = FragmentNotif.newInstance();
                        badgeNumber = 0;
                        removeBadge(BNV_NOTIF_POSITION, navigation, badge);
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
                return true;
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, FragmentHomeContainer.newInstance());
        transaction.commit();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(createBadge == null){
                createBadge = addBadgeAt(BNV_EVENT_POSITION, 1);
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_seting:
                Intent intent = new Intent(MainActivity.this, Seting.class);
                startActivity(intent);
                return true;
            case R.id.menu_profile:
                Intent intent2 = new Intent(MainActivity.this, Profile.class);
                intent2.putExtra(AppConfig.PROFILE_USER_ID, myIdS);
                startActivity(intent2);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Badge addBadgeAt(int position, int number) {
        return new QBadgeView(this)
                .setBadgeNumber(number)
                .setGravityOffset(12, 2, true)
                .bindTarget(navigation.getBottomNavigationItemView(position));
    }



    public void removeBadge(int position, BottomNavigationViewEx bottomNavigationViewEx, Badge badgeView) {
        if (badgeView!=null){
            badgeView.bindTarget(bottomNavigationViewEx.getBottomNavigationItemView(position))
                    .hide(true);
        }
    }

    private void detailChangeListener(final String equal) {
        mFirebaseDatabase.child("MeetingDetail").orderByChild(equal).equalTo(myIdS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    ModelDetail modelDetail = childSnapshot.getValue(ModelDetail.class);
                    if (modelDetail == null) {
                        return;
                    }
                    if(("0".equals(modelDetail.approve))&&("hostId".equals(equal))&&(modelDetail.getSync()==0)){
                        badgeNumber++;
                        Log.d(TAG, "3.1 "+badgeNumber);
                        mFirebaseDatabase.child("MeetingDetail").child(childSnapshot.getKey()).child("sync").setValue(1);
                    }
                    if(("3".equals(modelDetail.approve))&&("memberId".equals(equal))&&(modelDetail.getSync()==0)){
                        badgeNumber++;
                        Log.d(TAG, "3.2 "+badgeNumber);
                        mFirebaseDatabase.child("MeetingDetail").child(childSnapshot.getKey()).child("sync").setValue(1);
                    }
                }
                badge.setBadgeNumber(badgeNumber);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void relationChangeListener() {
        mFirebaseDatabase.child("Relation").child(myIdS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    ModelRelation modelRelation = childSnapshot.getValue(ModelRelation.class);
                    if (modelRelation == null) {
                        return;
                    }
                    if(("0".equals(modelRelation.approve)) && (modelRelation.getSync() == 0)){
                        badgeNumber++;
                        mFirebaseDatabase.child("Relation").child(myIdS).child(childSnapshot.getKey()).child("sync").setValue(1);
                    }
                }
                Log.d(TAG, "4 "+badgeNumber);
                badge.setBadgeNumber(badgeNumber);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    @Override
    public void onRelationCallback() {
        if(relationBadge==null){
            relationBadge = addBadgeAt(BNV_RELATION_POSITION, 1);
        }
    }

    @Override
    public void onEventCallback() {
        if(eventBadge == null){
            eventBadge = addBadgeAt(BNV_EVENT_POSITION, 1);
        }
    }

    @Override
    protected void onDestroy() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }
}
