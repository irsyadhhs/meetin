package irsyadhhs.cs.upi.edu.meetin;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class DetailMeeting extends AppCompatActivity implements FragmentMeetingDetail.OnDataPass{
    FirebaseAuth auth;

    private static final String TAG = "DetailMeeting";
    private SectionPageAdapter mSectionPageAdapter;
    private ViewPager mViewPager;

    Bundle bundleNotulensi;
    Bundle bundleAttandance;
    FragmentNotulensi notulensi;
    FragmentAttendance attendance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_meeting);

        bundleNotulensi = new Bundle();
        bundleAttandance = new Bundle();

        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() == null) {
            Intent intent = new Intent(DetailMeeting.this, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailMeeting.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });


        mSectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        setUpViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    public void setUpViewPager(ViewPager viewPager){
        FragmentMeetingDetail detail = new FragmentMeetingDetail();
        notulensi = new FragmentNotulensi();
        attendance = new FragmentAttendance();

        mSectionPageAdapter.addFragment(detail, "Detail");
        viewPager.setAdapter(mSectionPageAdapter);
    }

    @Override
    public void onDataPass(Bundle bundle) {
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_ID, bundle.getString(AppConfig.EXTRA_MEETING_ID));
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_TITLE, bundle.getString(AppConfig.EXTRA_MEETING_TITLE));
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_TIMEEND, bundle.getString(AppConfig.EXTRA_MEETING_TIMEEND));
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_TIMESTART, bundle.getString(AppConfig.EXTRA_MEETING_TIMESTART));
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_DATE, bundle.getString(AppConfig.EXTRA_MEETING_DATE));
        bundleNotulensi.putString(AppConfig.EXTRA_DETAIL_ID, bundle.getString(AppConfig.EXTRA_DETAIL_ID));
        bundleNotulensi.putString(AppConfig.EXTRA_MEETING_HOSTID, bundle.getString(AppConfig.EXTRA_MEETING_HOSTID));

        notulensi.setArguments(bundleNotulensi);
        attendance.setArguments(bundleNotulensi);
        if(mSectionPageAdapter.getCount() != 3) {
            mSectionPageAdapter.addFragment(attendance, "Kehadiran");
            mSectionPageAdapter.addFragment(notulensi, "Catatan");
            mSectionPageAdapter.notifyDataSetChanged();
        }
    }
}
