package irsyadhhs.cs.upi.edu.meetin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MEETING_FINISHED;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.MEETING_ONGOING;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

/**
 * Created by HARVI on 2/8/2018.
 */

public class AdapterMyMeeting extends RecyclerView.Adapter<AdapterMyMeeting.ViewHolder> {
    private static ArrayList<ModelMyMeeting> searchArrayList;

    private LayoutInflater mInflater;

    Context mContext;

    private DatabaseReference mFirebaseDatabase;

    private static final String TAG = "AdapterMyMeeting";

    public AdapterMyMeeting(Context context, ArrayList<ModelMyMeeting> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView title, location, date, time;
        ImageView infoIcon;
        MyClickListener listener;

        public ViewHolder(View itemView, MyClickListener listener) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.outtitle);
            this.location = (TextView) itemView.findViewById(R.id.outlocation);
            this.date = (TextView) itemView.findViewById(R.id.outdate);
            this.time = (TextView) itemView.findViewById(R.id.outtime);
            this.infoIcon = itemView.findViewById(R.id.infoIcon);
            this.listener = listener;

            infoIcon.setOnClickListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    Intent intent = new Intent(v.getContext(), DetailMeeting.class);
                    intent.putExtra(AppConfig.EXTRA_MEETING_ID, searchArrayList.get(position).getMeetingId());
                    intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, searchArrayList.get(position).hostId);
                    intent.putExtra(AppConfig.EXTRA_MEETING_STATUS, searchArrayList.get(position).status);
                    intent.putExtra(AppConfig.EXTRA_MEETING_DATE, searchArrayList.get(position).date);
                    intent.putExtra(AppConfig.EXTRA_DETAIL_ID, searchArrayList.get(position).getDetailId());

                    Log.d(TAG, "DETAIL ID " + searchArrayList.get(position).getDetailId());

                    intent.putExtra(AppConfig.EXTRA_MEETING_HOSTNAME, searchArrayList.get(position).hostName);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, searchArrayList.get(position).meetingTitle);
                    intent.putExtra(AppConfig.EXTRA_MEETING_ABOUT, searchArrayList.get(position).about);
                    intent.putExtra(AppConfig.EXTRA_MEETING_DESCRIPTION, searchArrayList.get(position).description);
                    intent.putExtra(AppConfig.EXTRA_MEETING_LOCATION, searchArrayList.get(position).location);

                    intent.putExtra(AppConfig.EXTRA_MEETING_TIMESTART, searchArrayList.get(position).timeStart);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TIMEEND, searchArrayList.get(position).timeEnd);
                    intent.putExtra(AppConfig.EXTRA_MEETING_LATITUDE, String.valueOf(searchArrayList.get(position).latitude));
                    intent.putExtra(AppConfig.EXTRA_MEETING_LONGITUDE, String.valueOf(searchArrayList.get(position).longitude));

                    intent.putExtra(AppConfig.EXTRA_MEETING_CODE, searchArrayList.get(position).code);

                    intent.putExtra(AppConfig.EXTRA_DETAIL_MODE, "mymeeting");

                    v.getContext().startActivity(intent);

                }
            });
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.infoIcon:
                    listener.onInfo(this.getAdapterPosition());
                    break;
                default:
                    break;
            }
        }

        public interface MyClickListener {
            void onInfo(int p);
        }
    }

    @Override
    public AdapterMyMeeting.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        AdapterMyMeeting.ViewHolder myViewHolder = new AdapterMyMeeting.ViewHolder(view, new ViewHolder.MyClickListener() {
            @Override
            public void onInfo(final int p) {
                final AlertDialog.Builder ad = new AlertDialog.Builder(mContext);
                ad.setMessage("tambahkan anggota ke "+searchArrayList.get(p).getTitle()+" ?")
                        .setTitle("Anggota Belum Ditambahkan")
                        .setCancelable(false)
                        .setPositiveButton("Tambahkan", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(mContext, AddAnggota.class);
                                intent.putExtra(AppConfig.EXTRA_MEETING_ID, searchArrayList.get(p).getMeetingId());
                                intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, searchArrayList.get(p).getTitle());
                                intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, searchArrayList.get(p).getHostId());
                                intent.putExtra(AppConfig.EXTRA_MEETING_CODE, searchArrayList.get(p).getCode());
                                intent.putExtra(AppConfig.EXTRA_MEMBER_USER1, searchArrayList.get(p).getHostName());
                                mContext.startActivity(intent);
                            }
                        })
                        .setNegativeButton("Abaikan", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent("delete-meeting");
                                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                                mFirebaseDatabase.child("MeetingList").child(searchArrayList.get(p).meetingId).child("sync").setValue(0);
                                ad.create().dismiss();
                            }
                        });

                AlertDialog alert = ad.create();
                alert.show();
            }
        });

        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final AdapterMyMeeting.ViewHolder holder, int position) {
        if(MEETING_FINISHED.equals(String.valueOf(searchArrayList.get(position).status))){
            holder.title.setTextColor(Color.parseColor("#757575"));
        }else if(searchArrayList.get(position).getDateDiff() < 0){
            if(searchArrayList.get(position).getSync() == 1 || MEETING_ONGOING.equals(searchArrayList.get(position).status)) {
                mFirebaseDatabase.child("MeetingList").child(searchArrayList.get(position).meetingId).child("status").setValue("1");
                mFirebaseDatabase.child("MeetingList").child(searchArrayList.get(position).meetingId).child("sync").setValue(0);
            }
            holder.title.setTextColor(Color.parseColor("#757575"));
        }else if((searchArrayList.get(position).getDateDiff() == 0) && (searchArrayList.get(position).getHourDiff() == -1)){
            if(searchArrayList.get(position).getSync() == 1 || MEETING_ONGOING.equals(searchArrayList.get(position).status)) {
                mFirebaseDatabase.child("MeetingList").child(searchArrayList.get(position).meetingId).child("status").setValue("1");
                mFirebaseDatabase.child("MeetingList").child(searchArrayList.get(position).meetingId).child("sync").setValue(0);
            }
            holder.infoIcon.setVisibility(View.GONE);
            holder.title.setTextColor(Color.parseColor("#757575"));
        }
        Log.d(TAG, "Diff "+searchArrayList.get(position).getHourDiff()+" "+searchArrayList.get(position).getDateDiff()+" "+searchArrayList.get(position).meetingTitle);
        holder.title.setText(searchArrayList.get(position).meetingTitle);
        holder.location.setText(searchArrayList.get(position).location);
        holder.date.setText(searchArrayList.get(position).date);
        holder.time.setText(searchArrayList.get(position).timeStart);
        if(searchArrayList.get(position).getSync() == 1 && MEETING_ONGOING.equals(searchArrayList.get(position).status)){
            holder.infoIcon.setVisibility(View.VISIBLE);
        }else{
            holder.infoIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }
}

