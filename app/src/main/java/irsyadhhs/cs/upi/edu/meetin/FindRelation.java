package irsyadhhs.cs.upi.edu.meetin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class FindRelation extends AppCompatActivity {
    TextView noPersonTV;
    EditText emailFind_et;
    String myIdS, myNameS, inputRelationS;
    SharedPreferences sp;

    private ProgressBar progressBar;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    private RecyclerView recyclerView;
    FirebaseRecyclerAdapter<ModelUser, RelationHolder> mRelationAdapter;

    final static String TAG = "FindRelation";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_relation);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FindRelation.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference();

        sp = getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");
        myNameS = sp.getString(AppConfig.MY_NAME, "0");

        emailFind_et = findViewById(R.id.emailFind_et);
        noPersonTV = findViewById(R.id.noPersonInfo);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        recyclerView = findViewById(R.id.listViewFindRelation);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void findPerson(View view){
        String orderBy = "";
        progressBar.setVisibility(View.VISIBLE);
        inputRelationS = emailFind_et.getText().toString().toLowerCase();

        if(android.util.Patterns.EMAIL_ADDRESS.matcher(inputRelationS).matches()){
            orderBy = "email";
        }else{
            orderBy = "name";
        }

        populateRelationFound(inputRelationS, orderBy);
        mRelationAdapter.startListening();

        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != this.getCurrentFocus()) {
            imm.hideSoftInputFromWindow(this.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
        }
    }

    public void populateRelationFound(String input, String orderBy){
        DatabaseReference userRef = mFirebaseDatabase.child("Users");
        Query query = userRef.orderByChild(orderBy).startAt(input).endAt(input + "\uf8ff");
        FirebaseRecyclerOptions personsOptions = new FirebaseRecyclerOptions.Builder<ModelUser>().setQuery(query, ModelUser.class).build();

        mRelationAdapter = new FirebaseRecyclerAdapter<ModelUser, RelationHolder>(personsOptions) {
            @Override
            protected void onBindViewHolder(final RelationHolder holder, int position, final ModelUser model) {
                final String userId = getRef(position).getKey();
                Log.d(TAG, "itemCount "+getItemCount());

                mFirebaseDatabase.child("Relation").child(myIdS).child(userId)
                        .addValueEventListener(new ValueEventListener() {
                           @Override
                           public void onDataChange(DataSnapshot dataSnapshot) {
                               String approve="";
                               final ModelRelation modelRelation = dataSnapshot.getValue(ModelRelation.class);
                               if(dataSnapshot.getChildrenCount()==0){
                                   approve = "-1";
                               }else {
                                   if (modelRelation == null) {
                                       return;
                                   }
                                   if (modelRelation.relationUserId.equals(userId)) {
                                       approve = modelRelation.approve;
                                   }
                               }
                               holder.setRelation(userId, model,approve);
                           }
                           @Override
                           public void onCancelled(DatabaseError error) {
                               Log.e(TAG, "Failed to read user", error.toException());
                           }
                       });
            }
            @Override
            public RelationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_find_relation, parent, false);

                return new RelationHolder(view);
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if(getItemCount() == 0){
                    noPersonTV.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                }else{
                    noPersonTV.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        };
        recyclerView.setAdapter(mRelationAdapter);
        progressBar.setVisibility(View.GONE);
    }

    public class RelationHolder extends RecyclerView.ViewHolder{
        TextView personNameTV, personMailTV;
        ImageView addPersonImV, memberToAdd;
        public RelationHolder(View itemView) {
            super(itemView);
            this.personNameTV = itemView.findViewById(R.id.personName);
            this.personMailTV = itemView.findViewById(R.id.personMail);
            this.addPersonImV = itemView.findViewById(R.id.addPerson);
            this.memberToAdd = itemView.findViewById(R.id.memberToAdd);
        }

        public void setRelation(final String userId, final ModelUser model, final String approve){
            if(("-1".equals(approve))||("0".equals(approve))){
                this.addPersonImV.setImageResource(R.drawable.ic_add_black_24dp);
                this.addPersonImV.setClickable(true);
            }else{
                this.addPersonImV.setImageResource(R.drawable.ic_check_black_24dp);
                this.addPersonImV.setClickable(false);
            }
            this.personNameTV.setText(model.name);
            this.personMailTV.setText(model.email);
            Picasso.with(FindRelation.this)
                    .load(model.picture)
                    .resize(96,96)
                    .centerCrop()
                    .error(R.drawable.ic_account_circle_black_48dp)
                    .placeholder(R.drawable.ic_account_circle_black_48dp)
                    .transform(new CircleTransform())
                    .into(memberToAdd);
            this.addPersonImV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mFirebaseDatabase.child("Relation").child(myIdS).child(userId)
                            .setValue(new ModelRelation(userId, model.name, "1"));
                    Intent intent = new Intent("find-relation");
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                    mFirebaseDatabase.child("Relation").child(userId).child(myIdS)
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.getChildrenCount() == 0){
                                        ModelRelation modelRelation2 = new ModelRelation(myIdS, myNameS, "0");
                                        modelRelation2.setSync(0);
                                        mFirebaseDatabase.child("Relation").child(userId).child(myIdS).setValue(modelRelation2);
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError error) {
                                    Log.e(TAG, "Failed to read user", error.toException());
                                }
                            });
                }
            });
        }
    }
}
