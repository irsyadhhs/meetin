package irsyadhhs.cs.upi.edu.meetin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Description extends AppCompatActivity {

    TextView aboutTV, descriptionTV, titleAboutTV;
    String aboutS, descriptionS, titleS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Description.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        titleAboutTV = findViewById(R.id.titleAbout);
        aboutTV = findViewById(R.id.about);
        descriptionTV = findViewById(R.id.description);

        Bundle extras = getIntent().getExtras();

        titleS = extras.getString(AppConfig.EXTRA_MEETING_TITLE);
        aboutS = extras.getString(AppConfig.EXTRA_MEETING_ABOUT);
        descriptionS = extras.getString(AppConfig.EXTRA_MEETING_DESCRIPTION);

        titleAboutTV.setText(titleS);
        aboutTV.setText(aboutS);
        descriptionTV.setText(descriptionS);

    }
}
