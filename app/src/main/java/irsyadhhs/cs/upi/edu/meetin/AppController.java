package irsyadhhs.cs.upi.edu.meetin;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by HARVI on 11/18/2017.
 */

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class AppController extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseInstance.setPersistenceEnabled(true);

    }

}
