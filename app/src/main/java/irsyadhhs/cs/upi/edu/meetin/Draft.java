package irsyadhhs.cs.upi.edu.meetin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import static irsyadhhs.cs.upi.edu.meetin.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.meetin.AppConfig.TAG;

public class Draft extends AppCompatActivity {

    private static RecyclerView recyclerView;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseRecyclerAdapter<ModelDraft, DraftHolder> draftAdapter;
    String myIdS, myNameS;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarNextdoor);
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.mipmap.ic_action_logo);
        View view = myToolbar.getChildAt(1);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Draft.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        sp = getSharedPreferences(SP, MODE_PRIVATE);
        myIdS = sp.getString(AppConfig.MY_ID, "0");

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();

        recyclerView = findViewById(R.id.listViewMyDraft);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        populateDraft();
        draftAdapter.startListening();
    }

    public void populateDraft(){
        Query query = mFirebaseDatabase.child("Draft").child(myIdS);
        FirebaseRecyclerOptions draftOptions = new FirebaseRecyclerOptions.Builder<ModelDraft>().setQuery(query, ModelDraft.class).build();

        draftAdapter = new FirebaseRecyclerAdapter<ModelDraft, DraftHolder>(draftOptions) {
            @Override
            protected void onBindViewHolder(final DraftHolder holder, final int position, final ModelDraft model) {
                Log.d(TAG, "count "+getItemCount());
                Log.d(TAG, "title "+model.meetingTitle);

                holder.setDraft(model, getRef(position).getKey());
            }
            @Override
            public DraftHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_draft, parent, false);

                return new DraftHolder(view);
            }

            /*@Override
            public void onDataChanged() {
                super.onDataChanged();
                if(getItemCount() == 0){

                }
            }*/
        };
        recyclerView.setAdapter(draftAdapter);
    }

    public class DraftHolder extends RecyclerView.ViewHolder {
        TextView myDraftTitle;
        public DraftHolder(View itemView) {
            super(itemView);
            this.myDraftTitle = (TextView) itemView.findViewById(R.id.myDraftTitle);
        }

        public void setDraft(final ModelDraft model, final String draftId) {
            this.myDraftTitle.setText(model.getTitle());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), CreateMeeting.class);
                    intent.putExtra(AppConfig.EXTRA_MEETING_HOSTID, myIdS);
                    intent.putExtra(AppConfig.MODE, "draft");
                    intent.putExtra(AppConfig.EXTRA_MEETING_ID, draftId);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TITLE, model.meetingTitle);
                    intent.putExtra(AppConfig.EXTRA_MEETING_ABOUT, model.about);
                    intent.putExtra(AppConfig.EXTRA_MEETING_DESCRIPTION, model.description);
                    intent.putExtra(AppConfig.EXTRA_MEETING_DATE, model.date);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TIMESTART, model.timeStart);
                    intent.putExtra(AppConfig.EXTRA_MEETING_TIMEEND, model.duration);
                    startActivity(intent);
                }
            });
        }
    }
}
